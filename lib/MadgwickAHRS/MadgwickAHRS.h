/**
 * @file MadgwickAHRS.h
 * Implementation of Madgwick's IMU and AHRS algorithms.
 *
 * @author SOH Madgwick
 * @version 2.0 
 * @date 14/03/2017
 *  
 *=====================================================================================================
 * MadgwickAHRS.h
 *=====================================================================================================
 *
 * Implementation of Madgwick's IMU and AHRS algorithms.
 * From: http: http://www.x-io.co.uk/node/8#open_source_ahrs_and_imu_algorithms
 *
 * Date			Author          	Notes
 * 29/09/2011	SOH Madgwick    	Initial release
 * 02/10/2011	SOH Madgwick		Optimised for reduced CPU load
 * 
 * Modification:
 * 14/03/2017	Bruno Laurencich	Just some formal arrangement in order to adapt it to C++ OOP
 *
 * At the moment of downloading this file no licence was attached to the source,
 * but there was a notice on the website:
 *
 * "Open-source resources available on this website are provided under the 
 * [GNU General Public Licence](https://www.gnu.org/licenses/gpl.html) 
 * unless an alternative licence is provided in source."
 *
 * Following the link, the text of the GNU-GPLv3 license was displayed, so according to 
 * the directives from the Free software foudation, I'm including a license disclamer 
 * on this file:
 *
 * This file is part of the Implementation of Madgwick's IMU and AHRS algorithms.
 *	
 *    The Implementation of Madgwick's IMU and AHRS algorithms is free software: 
 *    you can redistribute it and/or modify it under the terms of the 
 *    GNU General Public License as published by the Free Software Foundation, 
 *    either version 3 of the License, or (at your option) any later version.
 *
 *    The Implementation of Madgwick's IMU and AHRS algorithms is distributed 
 *    in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 *    without even the implied warranty of MERCHANTABILITY or FITNESS 
 *    FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with The Implementation of Madgwick's IMU and AHRS algorithms.
 *    If not, see <https://www.gnu.org/licenses/>. * 
 */

//=====================================================================================================
#ifndef MadgwickAHRS_h
#define MadgwickAHRS_h

#include <Chordata_def.h>
#include <Chordata_utils.h>
//----------------------------------------------------------------------------------------------------
// Variable declaration

// extern volatile float beta;				// algorithm gain
// extern volatile float q0, q1, q2, q3;	// quaternion of sensor frame relative to auxiliary frame

class sensorFusion {
public:
	sensorFusion(float hz = _CHORDATA_ODR, float _beta = _MADGWICK_BETA):
	sampleFreq(hz),
	beta(_beta),
	q0(1.0f), q1(0.0f), q2(0.0f), q3(0.0f)
	{};

	void setBeta(float b){ beta = b; };

protected:
	Quaternion MadgwickAHRSupdate(float gx, float gy, float gz, 
							float ax, float ay, float az, 
							float mx, float my, float mz) volatile;

	Quaternion MadgwickAHRSupdateIMU(float gx, float gy, float gz, 
								float ax, float ay, float az) volatile;

private:
	float sampleFreq;
	float beta;
	volatile float q0, q1, q2, q3;
};

#endif
//=====================================================================================================
// End of file
//=====================================================================================================
