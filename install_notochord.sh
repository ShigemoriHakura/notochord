#!/bin/bash

set -e

#change the hostname
echo notochord | sudo tee /etc/hostname

#set the supplicant to connect to a WIFI network
echo -e "\n\
network={ \n\
\tssid=\"Chordata-net\"\n\
\tpsk=\"chordata\"\n\
}" | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf
wpa_cli -i wlan0 reconfigure

#Enable i2c and set it to 330kHz
echo "dtparam=i2c_arm=on" | sudo tee -a /boot/config.txt
echo "dtparam=i2c_arm_baudrate=550000" | sudo tee -a /boot/config.txt

#Install deps
sudo apt-get update
sudo apt-get install -y i2c-tools libboost-dev libi2c-dev libxml2-utils scons git vim

#Clone the Notochord repo, and go to the [develop] branch (the one active during Beta-testing period)
git clone https://gitlab.com/chordata/notochord.git
cd notochord
git checkout develop

#Compile a debug build, using 3 cores from your raspberry (attemping to use 4 might result in an out-of-memory error)
scons -j3 debug=1

echo "cat $(pwd)/banner" >> ${HOME}/.bashrc

echo "====================================="
echo ">> Set hostname to: notochord.local"
echo ">>"
echo ">> Your raspberry will automatically connect to the WIFI network:"
echo ">> SSID: Chordata-net"
echo ">> PSW:  chordata"
echo ">>"
echo ">> $(bin/notochord -V) sucesfully compiled"
echo "===================================="
echo ""
echo "The defauld password is 'raspberry',"
echo "it is recomended to create a new password."
read -p "Would you like to set a new password now? [Y/n] " -n1 CREATE_PSSW 

set +e
echo ""

if [[ "$CREATE_PSSW" == "n" ]]; then
	echo "Password unchanged."
else
	sudo passwd pi
	echo "New password created, use it to access the system the next time."
fi

read -p "Press a key to continue." -n1  
