# NOTOCHORD
_Hub software of the Chordata motion capture system_

Current version `0.1.2b`.

This software is still on **BETA stage** and under heavy development. Refer to the `develop` branch for the lastest version.

### Hardware

Tested under a Raspberry Pi3 with Raspbian "Stretch" Lite 

with one Chordata Hub R1.2 as a multiplexer

and three Chordata K-Ceptor R2.2 as sensors

### INSTALLATION

- **First of all you have to activate the i2c adaptor on your rPi** 

```
sudo raspi-config
```

And activate the option under [5 Interfacing options] > [P5 I2C]

- **Then you have to fetch the dependencies, and compile**

The script `install_notochord.sh` will take care of cloning this repository, installing dependencies and do a minimal network configuration on your raspbian.

It can be executed with this one-liner:

```bash
bash <(curl -sL https://gitlab.com/chordata/notochord/raw/develop/install_notochord.sh)
```


- **You can now run the notochord**

```
cd bin
./notochord <YOUR COMPUTER IP ADDRESS>
```

### WIRING

To wire the Rasperry and Chordata Hub see [this section from our wiki](https://wiki.chordata.cc/wiki/User_Manual/2._Setting_up_the_system#Wiring_the_Hub_and_raspberry) 



### CALIBRATION

Before using each K-Ceptor it has to be calibrated. See [this section from our wiki](https://wiki.chordata.cc/wiki/User_Manual/4._Sensor_calibration) for more info.

The proccesing of the raw data is performed by a matlab script created by _Ozan AKTAÃž_: `calib/getMagCalib.m`

### LICENSE

This program is free software and is distributed under the GNU General Public License, version 3.
You are free to run, study, share and modify this software.
Derivative work can only be distributed under the same license terms, and replicating this program copyright notice.

More info about the terms of the license [here](https://www.gnu.org/licenses/gpl-faq.en.html). 