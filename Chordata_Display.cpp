
/*********************************************************************
SSD1306 I2C Library for Raspberry Pi.
Based on Adafruit SSD1306 Arduino library. Some functions came from Adafruit GFX lib
Modified by Ilia Penev
Tested on Raspberry Pi 2 with 0.96 Yellow/Blue OLED
*********************************************************************/

/*********************************************************************
This is a library for our Monochrome OLEDs based on SSD1306 drivers
  Pick one up today in the adafruit shop!
  ------> http://www.adafruit.com/category/63_98
These displays use SPI to communicate, 4 or 5 pins are required to
interface
Adafruit invests time and resources providing this open source code,
please support Adafruit and open-source hardware by purchasing
products from Adafruit!
Written by Limor Fried/Ladyada  for Adafruit Industries.
BSD license, check license.txt for more information
All text above, and the splash screen below must be included in any redistribution
*********************************************************************/

#include <stdio.h>
#include <string.h>

#include "Chordata_Display.h"
#include "i2c_manager.h"
#include "Chordata_parser.h"
#include "Chordata_utils.h"

#include "oled_fonts.h"

#define true 1
#define false 0

#define rotation 0

#define pgm_read_byte(addr) (*(const unsigned char *)(addr))

int cursor_y = 0;
int cursor_x = 0;

unsigned int buffer[Diaplay_LCDWIDTH * Diaplay_LCDHEIGHT / 8] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00};

int _vccstate;
int i2cd;

I2C_io *mi2c;

#define Diaplay_swap(a, b) \
  {                        \
    int t = a;             \
    a = b;                 \
    b = t;                 \
  }

// the most basic function, set a single pixel
void Diaplay_drawPixel(int x, int y, unsigned int color)
{
  if ((x < 0) || (x >= WIDTH) || (y < 0) || (y >= HEIGHT))
    return;

  // check rotation, move pixel around if necessary
  switch (rotation)
  {
  case 1:
    Diaplay_swap(x, y);
    x = WIDTH - x - 1;
    break;
  case 2:
    x = WIDTH - x - 1;
    y = HEIGHT - y - 1;
    break;
  case 3:
    Diaplay_swap(x, y);
    y = HEIGHT - y - 1;
    break;
  }

  // x is which column
  switch (color)
  {
  case WHITE:
    buffer[x + (y / 8) * Diaplay_LCDWIDTH] |= (1 << (y & 7));
    break;
  case BLACK:
    buffer[x + (y / 8) * Diaplay_LCDWIDTH] &= ~(1 << (y & 7));
    break;
  case INVERSE:
    buffer[x + (y / 8) * Diaplay_LCDWIDTH] ^= (1 << (y & 7));
    break;
  }
}

// Init SSD1306
void Diaplay_begin(unsigned int vccstate, I2C_io *io)
{
  // I2C Init
  mi2c = io;
  _vccstate = vccstate;
  //printf("Initing Display\n");

  mi2c->writeByte(0x73, 0, (1 << (7 - 1)));

  // Init sequence
  Diaplay_command(Diaplay_DISPLAYOFF);         // 0xAE
  Diaplay_command(Diaplay_SETDISPLAYCLOCKDIV); // 0xD5
  Diaplay_command(0x80);                       // the suggested ratio 0x80

  Diaplay_command(Diaplay_SETMULTIPLEX); // 0xA8
  Diaplay_command(Diaplay_LCDHEIGHT - 1);

  Diaplay_command(Diaplay_SETDISPLAYOFFSET);   // 0xD3
  Diaplay_command(0x0);                        // no offset
  Diaplay_command(Diaplay_SETSTARTLINE | 0x0); // line #0
  Diaplay_command(Diaplay_CHARGEPUMP);         // 0x8D
  if (vccstate == Diaplay_EXTERNALVCC)
  {
    Diaplay_command(0x10);
  }
  else
  {
    Diaplay_command(0x14);
  }
  Diaplay_command(Diaplay_MEMORYMODE); // 0x20
  Diaplay_command(0x00);               // 0x0 act like ks0108
  Diaplay_command(Diaplay_SEGREMAP | 0x1);
  Diaplay_command(Diaplay_COMSCANDEC);

  Diaplay_command(Diaplay_SETCOMPINS); // 0xDA
  Diaplay_command(0x12);
  Diaplay_command(Diaplay_SETCONTRAST); // 0x81
  if (vccstate == Diaplay_EXTERNALVCC)
  {
    Diaplay_command(0x9F);
  }
  else
  {
    Diaplay_command(0xCF);
  }

  Diaplay_command(Diaplay_SETPRECHARGE); // 0xd9
  if (vccstate == Diaplay_EXTERNALVCC)
  {
    Diaplay_command(0x22);
  }
  else
  {
    Diaplay_command(0xF1);
  }
  Diaplay_command(Diaplay_SETVCOMDETECT); // 0xDB
  Diaplay_command(0x40);
  Diaplay_command(Diaplay_DISPLAYALLON_RESUME); // 0xA4
  Diaplay_command(Diaplay_NORMALDISPLAY);       // 0xA6

  Diaplay_command(Diaplay_DEACTIVATE_SCROLL);

  Diaplay_command(Diaplay_DISPLAYON); // --turn on oled panel

  //Diaplay_display(); //Adafruit logo is visible
  //sleep(1);

  //Diaplay_clearDisplay();
  char *text = "Loading";
  Diaplay_drawString(text);
  Diaplay_display();
}

void Diaplay_invertDisplay(unsigned int i)
{
  if (i)
  {
    Diaplay_command(Diaplay_INVERTDISPLAY);
  }
  else
  {
    Diaplay_command(Diaplay_NORMALDISPLAY);
  }
}

void Diaplay_command(unsigned int c)
{
  // I2C
  unsigned int control = 0x00; // Co = 0, D/C = 0
  mi2c->writeByte(Diaplay_I2C_ADDRESS, control, c);
}

void Diaplay_display(void)
{
  auto val = mi2c->readByte(0x73, 0);
  mi2c->writeByte(0x73, 0, (1 << (7 - 1)));

  Diaplay_command(Diaplay_COLUMNADDR);
  Diaplay_command(0);                    // Column start address (0 = reset)
  Diaplay_command(Diaplay_LCDWIDTH - 1); // Column end address (127
  // = reset)

  Diaplay_command(Diaplay_PAGEADDR);
  Diaplay_command(0);

  Diaplay_command(7);

  // I2C
  int i;
  for (i = 0; i < (Diaplay_LCDWIDTH * Diaplay_LCDHEIGHT / 8); i++)
  {
    mi2c->writeByte(Diaplay_I2C_ADDRESS, 0x40, buffer[i]);
    //This sends byte by byte.
    //Better to send all buffer without 0x40 first
    //Should be optimized
  }

  mi2c->writeByte(0x73, 0, val);
}

// startscrollright
// Activate a right handed scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// Diaplay_scrollright(0x00, 0x0F)
void Diaplay_startscrollright(unsigned int start, unsigned int stop)
{
  Diaplay_command(Diaplay_RIGHT_HORIZONTAL_SCROLL);
  Diaplay_command(0X00);
  Diaplay_command(start);
  Diaplay_command(0X00);
  Diaplay_command(stop);
  Diaplay_command(0X00);
  Diaplay_command(0XFF);
  Diaplay_command(Diaplay_ACTIVATE_SCROLL);
}

// startscrollleft
// Activate a right handed scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// Diaplay_scrollright(0x00, 0x0F)
void Diaplay_startscrollleft(unsigned int start, unsigned int stop)
{
  Diaplay_command(Diaplay_LEFT_HORIZONTAL_SCROLL);
  Diaplay_command(0X00);
  Diaplay_command(start);
  Diaplay_command(0X00);
  Diaplay_command(stop);
  Diaplay_command(0X00);
  Diaplay_command(0XFF);
  Diaplay_command(Diaplay_ACTIVATE_SCROLL);
}

// startscrolldiagright
// Activate a diagonal scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// Diaplay_scrollright(0x00, 0x0F)
void Diaplay_startscrolldiagright(unsigned int start, unsigned int stop)
{
  Diaplay_command(Diaplay_SET_VERTICAL_SCROLL_AREA);
  Diaplay_command(0X00);
  Diaplay_command(Diaplay_LCDHEIGHT);
  Diaplay_command(Diaplay_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL);
  Diaplay_command(0X00);
  Diaplay_command(start);
  Diaplay_command(0X00);
  Diaplay_command(stop);
  Diaplay_command(0X01);
  Diaplay_command(Diaplay_ACTIVATE_SCROLL);
}

// startscrolldiagleft
// Activate a diagonal scroll for rows start through stop
// Hint, the display is 16 rows tall. To scroll the whole display, run:
// Diaplay_scrollright(0x00, 0x0F)
void Diaplay_startscrolldiagleft(unsigned int start, unsigned int stop)
{
  Diaplay_command(Diaplay_SET_VERTICAL_SCROLL_AREA);
  Diaplay_command(0X00);
  Diaplay_command(Diaplay_LCDHEIGHT);
  Diaplay_command(Diaplay_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL);
  Diaplay_command(0X00);
  Diaplay_command(start);
  Diaplay_command(0X00);
  Diaplay_command(stop);
  Diaplay_command(0X01);
  Diaplay_command(Diaplay_ACTIVATE_SCROLL);
}

void Diaplay_stopscroll(void)
{
  Diaplay_command(Diaplay_DEACTIVATE_SCROLL);
}

// Dim the display
// dim = true: display is dimmed
// dim = false: display is normal
void Diaplay_dim(unsigned int dim)
{
  unsigned int contrast;

  if (dim)
  {
    contrast = 0; // Dimmed display
  }
  else
  {
    if (_vccstate == Diaplay_EXTERNALVCC)
    {
      contrast = 0x9F;
    }
    else
    {
      contrast = 0xCF;
    }
  }
  // the range of contrast to too small to be really useful
  // it is useful to dim the display
  Diaplay_command(Diaplay_SETCONTRAST);
  Diaplay_command(contrast);
}

// clear everything
void Diaplay_clearDisplay(void)
{
  memset(buffer, 0,
         (Diaplay_LCDWIDTH * Diaplay_LCDHEIGHT / 8) * sizeof(int));
  cursor_y = 0;
  cursor_x = 0;
}

void Diaplay_drawFastHLineInternal(int x, int y, int w, unsigned int color)
{
  // Do bounds/limit checks
  if (y < 0 || y >= HEIGHT)
  {
    return;
  }
  // make sure we don't try to draw below 0
  if (x < 0)
  {
    w += x;
    x = 0;
  }
  // make sure we don't go off the edge of the display
  if ((x + w) > WIDTH)
  {
    w = (WIDTH - x);
  }
  // if our width is now negative, punt
  if (w <= 0)
  {
    return;
  }
  // set up the pointer for movement through the buffer
  unsigned int *pBuf = buffer;
  // adjust the buffer pointer for the current row
  pBuf += ((y / 8) * Diaplay_LCDWIDTH);
  // and offset x columns in
  pBuf += x;

  unsigned int mask = 1 << (y & 7);

  switch (color)
  {
  case WHITE:
    while (w--)
    {
      *pBuf++ |= mask;
    };
    break;
  case BLACK:
    mask = ~mask;
    while (w--)
    {
      *pBuf++ &= mask;
    };
    break;
  case INVERSE:
    while (w--)
    {
      *pBuf++ ^= mask;
    };
    break;
  }
}

void Diaplay_drawFastVLineInternal(int x, int __y, int __h, unsigned int color)
{

  // do nothing if we're off the left or right side of the screen
  if (x < 0 || x >= WIDTH)
  {
    return;
  }
  // make sure we don't try to draw below 0
  if (__y < 0)
  {
    // __y is negative, this will subtract enough from __h to account
    // for __y being 0
    __h += __y;
    __y = 0;
  }
  // make sure we don't go past the height of the display
  if ((__y + __h) > HEIGHT)
  {
    __h = (HEIGHT - __y);
  }
  // if our height is now negative, punt
  if (__h <= 0)
  {
    return;
  }
  // this display doesn't need ints for coordinates, use local byte
  // registers for faster juggling
  unsigned int y = __y;
  unsigned int h = __h;

  // set up the pointer for fast movement through the buffer
  unsigned int *pBuf = buffer;
  // adjust the buffer pointer for the current row
  pBuf += ((y / 8) * Diaplay_LCDWIDTH);
  // and offset x columns in
  pBuf += x;

  // do the first partial byte, if necessary - this requires some
  // masking
  unsigned int mod = (y & 7);
  if (mod)
  {
    // mask off the high n bits we want to set
    mod = 8 - mod;

    // note - lookup table results in a nearly 10% performance
    // improvement in fill* functions
    // register unsigned int mask = ~(0xFF >> (mod));
    static unsigned int premask[8] =
        {0x00, 0x80, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC, 0xFE};
    unsigned int mask = premask[mod];

    // adjust the mask if we're not going to reach the end of this
    // byte
    if (h < mod)
    {
      mask &= (0XFF >> (mod - h));
    }

    switch (color)
    {
    case WHITE:
      *pBuf |= mask;
      break;
    case BLACK:
      *pBuf &= ~mask;
      break;
    case INVERSE:
      *pBuf ^= mask;
      break;
    }

    // fast exit if we're done here!
    if (h < mod)
    {
      return;
    }

    h -= mod;

    pBuf += Diaplay_LCDWIDTH;
  }
  // write solid bytes while we can - effectively doing 8 rows at a time
  if (h >= 8)
  {
    if (color == INVERSE)
    { // separate copy of the code so we don't
      // impact performance of the black/white
      // write version with an extra comparison
      // per loop
      do
      {
        *pBuf = ~(*pBuf);

        // adjust the buffer forward 8 rows worth of data
        pBuf += Diaplay_LCDWIDTH;

        // adjust h & y (there's got to be a faster way for me to
        // do this, but this should still help a fair bit for now)
        h -= 8;
      } while (h >= 8);
    }
    else
    {
      // store a local value to work with
      register unsigned int val = (color == WHITE) ? 255 : 0;

      do
      {
        // write our value in
        *pBuf = val;

        // adjust the buffer forward 8 rows worth of data
        pBuf += Diaplay_LCDWIDTH;

        // adjust h & y (there's got to be a faster way for me to
        // do this, but this should still help a fair bit for now)
        h -= 8;
      } while (h >= 8);
    }
  }
  // now do the final partial byte, if necessary
  if (h)
  {
    mod = h & 7;
    // this time we want to mask the low bits of the byte, vs the high
    // bits we did above
    // register unsigned int mask = (1 << mod) - 1;
    // note - lookup table results in a nearly 10% performance
    // improvement in fill* functions
    static unsigned int postmask[8] =
        {0x00, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F};
    unsigned int mask = postmask[mod];
    switch (color)
    {
    case WHITE:
      *pBuf |= mask;
      break;
    case BLACK:
      *pBuf &= ~mask;
      break;
    case INVERSE:
      *pBuf ^= mask;
      break;
    }
  }
}

void Diaplay_drawFastHLine(int x, int y, int w, unsigned int color)
{
  unsigned int bSwap = false;
  switch (rotation)
  {
  case 0:
    // 0 degree rotation, do nothing
    break;
  case 1:
    // 90 degree rotation, swap x & y for rotation, then invert x
    bSwap = true;
    Diaplay_swap(x, y);
    x = WIDTH - x - 1;
    break;
  case 2:
    // 180 degree rotation, invert x and y - then shift y around for
    // height.
    x = WIDTH - x - 1;
    y = HEIGHT - y - 1;
    x -= (w - 1);
    break;
  case 3:
    // 270 degree rotation, swap x & y for rotation, then invert y and
    // adjust y for w (not to become h)
    bSwap = true;
    Diaplay_swap(x, y);
    y = HEIGHT - y - 1;
    y -= (w - 1);
    break;
  }

  if (bSwap)
  {
    Diaplay_drawFastVLineInternal(x, y, w, color);
  }
  else
  {
    Diaplay_drawFastHLineInternal(x, y, w, color);
  }
}

void Diaplay_drawFastVLine(int x, int y, int h, unsigned int color)
{
  unsigned int bSwap = false;
  switch (rotation)
  {
  case 0:
    break;
  case 1:
    // 90 degree rotation, swap x & y for rotation, then invert x and
    // adjust x for h (now to become w)
    bSwap = true;
    Diaplay_swap(x, y);
    x = WIDTH - x - 1;
    x -= (h - 1);
    break;
  case 2:
    // 180 degree rotation, invert x and y - then shift y around for
    // height.
    x = WIDTH - x - 1;
    y = HEIGHT - y - 1;
    y -= (h - 1);
    break;
  case 3:
    // 270 degree rotation, swap x & y for rotation, then invert y
    bSwap = true;
    Diaplay_swap(x, y);
    y = HEIGHT - y - 1;
    break;
  }

  if (bSwap)
  {
    Diaplay_drawFastHLineInternal(x, y, h, color);
  }
  else
  {
    Diaplay_drawFastVLineInternal(x, y, h, color);
  }
}

void Diaplay_fillRect(int x, int y, int w, int h, int fillcolor)
{

  // Bounds check
  if ((x >= WIDTH) || (y >= HEIGHT))
    return;

  // Y bounds check
  if (y + h > HEIGHT)
  {
    h = HEIGHT - y - 1;
  }
  // X bounds check
  if (x + w > WIDTH)
  {
    w = WIDTH - x - 1;
  }

  switch (rotation)
  {
  case 1:
    //swap_values(x, y);
    x = WIDTH - x - 1;
    break;
  case 2:
    x = WIDTH - x - 1;
    y = HEIGHT - y - 1;
    break;
  case 3:
    //swap_values(x, y);
    y = HEIGHT - y - 1;
    break;
  }
  int i;
  for (i = 0; i < h; i++)
    Diaplay_drawFastHLine(x, y + i, w, fillcolor);
}

int textsize = 1;
int wrap = 1;

void Diaplay_setTextSize(int s)
{
  textsize = (s > 0) ? s : 1;
}

void Diaplay_write(int c)
{

  if (c == '\n')
  {
    cursor_y += textsize * 8;
    cursor_x = 0;
  }
  else if (c == '\r')
  {
    // skip em
  }
  else
  {
    Diaplay_drawChar(cursor_x, cursor_y, c, WHITE, textsize);
    cursor_x += textsize * 6;
    if (wrap && (cursor_x > (WIDTH - textsize * 6)))
    {
      cursor_y += textsize * 8;
      cursor_x = 0;
    }
  }
}

void Diaplay_drawString(char *str)
{
  int i, end;
  end = strlen(str);
  for (i = 0; i < end; i++)
    Diaplay_write(str[i]);
}

void Diaplay_drawString(const char *str)
{
  int i, end;
  end = strlen(str);
  for (i = 0; i < end; i++)
    Diaplay_write(str[i]);
}

void Diaplay_show(const char *str)
{
  if (mi2c)
  {
    Diaplay_clearDisplay();
    Diaplay_drawString(str);
    Diaplay_display();
  }
}

void Diaplay_show(char *str)
{
  if (mi2c)
  {
    Diaplay_clearDisplay();
    Diaplay_drawString(str);
    Diaplay_display();
  }
}

// Draw a character
void Diaplay_drawChar(int x, int y, unsigned char c, int color, int size)
{

  if ((x >= WIDTH) ||             // Clip right
      (y >= HEIGHT) ||            // Clip bottom
      ((x + 6 * size - 1) < 0) || // Clip left
      ((y + 8 * size - 1) < 0))   // Clip top
    return;
  int i;
  int j;
  for (i = 0; i < 6; i++)
  {
    int line;
    if (i == 5)
      line = 0x0;
    else
      line = pgm_read_byte(font + (c * 5) + i);
    for (j = 0; j < 8; j++)
    {
      if (line & 0x1)
      {
        if (size == 1) // default size
          Diaplay_drawPixel(x + i, y + j, color);
        else
        { // big size
          Diaplay_fillRect(x + (i * size),
                           y + (j * size), size,
                           size, color);
        }
      }
      line >>= 1;
    }
  }
}
