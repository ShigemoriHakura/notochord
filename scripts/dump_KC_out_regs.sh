#!/bin/bash
#
# This script will open all the gates from a Chordata HUB and
# dump acel OUT registers forever
# 
# It is adviced to plug just one KCeptor at the time to avoid confusion
# or address clashing  

set -e

AG_ADDR=0x6b

out_regs=( 0x28 0x29 0x2a 0x2b 0x2c 0x2d )

while true; do 
	READING=""
	for i in "${out_regs[@]}"
	do
		READING+="$(i2cset -y 1 0x73 0xff; i2cget -y 1 $AG_ADDR $i) "
	done
	sleep 0.2

	echo $READING
done

