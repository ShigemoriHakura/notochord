/**
 * @file Chordata_test_Notochord.cpp
 * Test the Notochord central class.
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2017/10/11
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 * -catch library: Copyright (c) 2012 Two Blue Cubes Ltd, under the Boost Software License, Version 1.0
 * -trompeloeil mocking framework: Copyright Björn Fahller 2014-2017 under the Boost Software License, Version 1.0
 *
 */
#include "catch.hpp"
#include "trompeloeil.hpp"
#include <Chordata_utils.h>
#include <Chordata_parser.h>
#include "Chordata_test.h"
#include "Chordata_test_xml_examples.h"

#include "../src/Notochord.h"

using Catch::Matchers::Contains;
using Catch::Matchers::Equals;

using trompeloeil::_;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

#define _XML_FILENAME "Chordata_test_xml_correct.xml"

#define BASE_FOR_NOTOCHORD \
	CHECK_NOTHROW(create_temp_file(_XML_FILENAME,xml_correct));\
	CHECK_NOTHROW(create_temp_file(schema_filename,xsd_v1_0));\
	auto d = Chordata::createDefaultConfig();\
	d.xml.filename = TEMP_FILES_PATH _XML_FILENAME;\
	d.xml.schema= schema_path.c_str();


namespace Chordata_test{
	//TO PLACE MOCKS
}

//////////////////////////////////
/// TODO 
//////////////////////////////////

TEST_CASE("TODO Notochord", "[.TODO]"){
	WARN("Hold and pass smart pointers on the Notochord");
	WARN("Supress warning about init_communicator on the catchs in main()");
	WARN("Wait for user confirmation before starting capture.");
	WARN("Return error codes from main");
	WARN("put Chordata::getConf() in another place (utils ?)");


}


TEST_CASE("TODO Timer", "[.TODO]"){
	WARN("Create dedicated file to test the timer");
	WARN("Check for correct interface in timer<T>::actuator");

}

int Chordata_test::Test_Helper::spawn_threads(Chordata::Notochord *n){
	return n->spawn_threads();
}

int Chordata_test::Test_Helper::join_threads(Chordata::Notochord * n){
	return n->join_threads();
}

int Chordata_test::Test_Helper::inform_thread(Chordata::Notochord *n , int num){
	return n->inform_thread(num);
}


//////////////////////////////////
/// CIRCULAR BUFFER 
//////////////////////////////////


TEST_CASE("TODO Circular_buffer", "[.TODO]"){
	WARN("Create a dedicated file for Circular_buffer testing");
	WARN("Test passing a class without bang()");

}

class Test_Node
{
public:
	int num;
	explicit Test_Node(int n): num(n){}
	
	void bang(){throw Chordata::Error(num);}
	
};

#include "Chordata_circular_buffer.h"

SCENARIO("The circular Buffer offers a thread safe FIFO where to queue the nodes to be banged","[c_buffer]"){
	GIVEN("A circular buffer with a capacity of 5"){
		Chordata::Circular_buffer<Test_Node *> b(5);
		Test_Node t(54);
		GIVEN("A test_node pushed"){
			b.put(&t);
			WHEN("A user eats the node, it should call the bang() member function"){
				try{
					b.eat();
				} catch (const Chordata::Error& e){
					REQUIRE(e.running_time ==  54);
				}
				THEN("The buffer should be empty"){
					REQUIRE(b.empty());
				}
			}
		}
		GIVEN("5 nodes pushed to the buffer"){
			for (int i = 0; i < 5; ++i)
			{
				b.put(&t);
			}
			WHEN("A user pushes another one, the buffer should throw"){
				REQUIRE_THROWS_AS(b.put(&t), Chordata::Error);
			}
		}
	}
}


//////////////////////////////////
/// NOTOCHORD 
//////////////////////////////////

SCENARIO("The namespace function Chordata::getConf, returns the global Configuration_Data", "[Notochord][config]"){
	WHEN("A user just calls it"){
		THEN("It should throw"){
			REQUIRE_THROWS_AS(Chordata::getConf(), Chordata::Logic_Error);
		}
	}
	WHEN("A user calls it, after an instance of Notochord has been created"){
		BASE_FOR_NOTOCHORD
		Chordata::Notochord n(std::move(d));
		THEN("It should return the same Configuration_Data than the Notochord has created"){
			REQUIRE(Chordata::getConf().xml.filename == n.getConf().xml.filename);
		}
	}

	WARN("Further test this");

}


SCENARIO("The Notochord manages the thread pool", "[.thread]"){
	BASE_FOR_NOTOCHORD

	Chordata::Notochord n(std::move(d));

	using h = Chordata_test::Test_Helper;
	auto ts= h::spawn_threads(&n);


	for (int i = 0; i <= ts; ++i)
	{
		std::cout << "informing " << 2+i << std::endl;
		h::inform_thread(&n, 2+i);
		std::this_thread::sleep_for(Chordata::millis(10));
	}

	h::join_threads(&n);

}


SCENARIO("The Notochord triggers the armature parsing","[Notochord][armature]"){
	BASE_FOR_NOTOCHORD

	Chordata::Notochord n(std::move(d));

	REQUIRE_NOTHROW(n.createArmature());
}

SCENARIO("The Notochord triggers the i2c_adapter initialization\n"
	"This test may fail in systems without an adapter exposed on `/dev/i2c-1`",
	"[Notochord][.i2c_adapter][!mayfail]"){

	BASE_FOR_NOTOCHORD

	Chordata::Notochord n(std::move(d));

	INFO("Trying to open: " << n.getConf().comm.adapter );  
	bool i2c_adapter_initialization;
	REQUIRE_NOTHROW((i2c_adapter_initialization = n.initI2C()));

	REQUIRE(i2c_adapter_initialization);

	CHECK(system("rm -rf " TEMP_FILES_PATH) != -1);

}

SCENARIO("The Notochord controls the root funzionalities of the program","[Notochord]"){
	CHECK_NOTHROW(create_temp_file(_XML_FILENAME,xml_correct));
	CHECK_NOTHROW(create_temp_file(schema_filename,xsd_v1_0));

	WHEN("It's initialized with the argc / argv values"){
		THEN("it should create config struct parsing the cmd_line" 
			"and then try to parse the xml config file"){

			const char *args[] = {"Fake_notochord", 
							"-c", TEMP_FILES_PATH _XML_FILENAME,
							"--schema", schema_path.c_str()	};
			
			Chordata::Notochord n(5,args);

			CHECK_THAT(n.getConf().comm.ip, Equals(_XML_E_IP));
			REQUIRE_THAT(n.getConf().xml.filename, Equals(TEMP_FILES_PATH _XML_FILENAME));
		

		}
	}
	
	WHEN("It's initialized with a Configuration_Data struct"){
		THEN("It should try to parse the xml config file"){
			auto d = Chordata_test::create_config_for_OSC();
			d.xml.filename = TEMP_FILES_PATH _XML_FILENAME;
			d.xml.schema= schema_path.c_str();
			REQUIRE(d.comm.transmit == (Chordata::OSC | Chordata::STDERR));
			CHECK_NOTHROW(Chordata::Notochord(std::move(d)));
		}
	}

	

	// CHECK(system("rm -rf " TEMP_FILES_PATH) != -1);
}
