/**
 * @file Chordata_test_node.cpp
 * Testing the node objects.
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2017/10/11
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 * -catch library: Copyright (c) 2012 Two Blue Cubes Ltd, under the Boost Software License, Version 1.0
 * -trompeloeil mocking framework: Copyright Björn Fahller 2014-2017 under the Boost Software License, Version 1.0
 *
 */
#include <limits>
#include <cstddef>

#include "catch.hpp"
#include "trompeloeil.hpp"

// #define __FORCE_LSM9DS0__

#include "Chordata_test.h"
#include "Chordata_test_xml_examples.h"
#include "Chordata_node.h"
#include "Chordata_parser.h"
#include "Chordata_timer.h"
#include "../lib/i2c_manager.h"


// #include "../lib/imu/LSM9DS0.h"

using Catch::Matchers::Contains;
using trompeloeil::_;
using h = Chordata_test::Test_Helper;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

TEST_CASE("TODO more on Nodes", "[.TODO]"){
	WARN("The bang test fails if communicator not initialized");

}


//////////////////////////////////
/// I2C_io MOCK
//////////////////////////////////
namespace Chordata_test{

	class LSM9DS0_MOCK: public _CHORDATA_IMU_TYPE_{
	public:
		LSM9DS0_MOCK(I2C_io * _i2c):
		_CHORDATA_IMU_TYPE_( _i2c , 23)
		{};

		// MAKE_MOCK7(param_begin, 
		// 			uint16_t(
		// 				gyro_scale, 
		// 				accel_scale, 
		// 				mag_scale,
		// 				gyro_odr, 
		// 				accel_odr, 
		// 				accel_abw, 
		// 				mag_odr),
		// 			override);

		MAKE_MOCK0(begin, uint16_t(),override);

		MAKE_MOCK0(readGyro, void(),override);
		MAKE_MOCK0(readAccel, void(),override);
		MAKE_MOCK0(readMag, void(),override);

	};
	
	class I2C_io_MOCK: public I2C_io {
	public:
		I2C_io_MOCK():
		I2C_io(new I2C_Device("", false))
		{};

		int validate(){
			return 32;
		}

		MAKE_MOCK2(I2CwriteByte, 
			void(uint8_t address, uint8_t data), 
			override);	
		
		MAKE_MOCK3(I2CwriteByte, 
			void(uint8_t address, uint8_t subAddress, uint8_t data),
			override);
		
		MAKE_MOCK2(I2CreadByte, 
			int32_t(uint8_t address, uint8_t subAddress),
			override);

		MAKE_MOCK4(I2CreadBytes, 
			void(uint8_t address, uint8_t subAddress, uint8_t * dest, uint8_t count),
			override);
		

	};
}


std::string filename = "Chordata_test_xml_correct.xml";
std::string path = std::string(TEMP_FILES_PATH) + filename;

TEST_CASE("TODO Nodes", "[.TODO]"){
	WARN("If multithreaded implemented, each KC will need to have it's own instance of IMU, "
		"or the way IMU stores lecture will have to change.");
	WARN("Generalize the test to use I_Imu and LSM9DS1");

}

void Chordata_test::Test_Helper::set_imu(Chordata::K_Ceptor* kc, Chordata::I_Imu<I2C_io>* imu){
	 	kc->imu = imu;
	 }

//////////////////////////////////
/// KCEPTOR 
//////////////////////////////////
SCENARIO("A KCEPTOR can be \"banged\" and it should comunicate with the IMU it represents", 
	"[nodes][i2c_mock][LSM9DS0_mock][!mayfail]"){
	CHECK_NOTHROW(create_temp_file(filename,xml_correct));
	CHECK_NOTHROW(create_temp_file(schema_filename,xsd_v1_0));

	auto conf = Chordata::createConfig_with_filenames(path, schema_path);
	conf.append_path();

	GIVEN("A freshly created KCeptor object"){
		Chordata_test::I2C_io_MOCK i2c_mock{};
		Chordata_test::LSM9DS0_MOCK imu_mock{ &i2c_mock };
		Chordata::Node_Scheluder ndsch{};
		Chordata::Armature_Parser axmlp(&ndsch, conf);
		axmlp.set_io(new I2C_io(new I2C_Device("", false)));
		auto xml = Chordata_test::basicConfigXML();
		auto label = "L_leg_sensor";
		auto kc_xml = Chordata_test::appendElement(xml, "K_Ceptor", label, "1","\n\t\n\t 0x6e \t\t\n\r");

		auto kc = std::unique_ptr<Chordata::KC>(axmlp.make<Chordata::KC>(nullptr, kc_xml));
		h::set_imu(kc.get(), &imu_mock);

		
		THEN("It should have the setup member == false"){
			REQUIRE(kc->setupDone() == false);
		}
		
		WHEN("A user bangs it for the first time"){
			THEN("It should perform the initial setup of the imu"){
				
				REQUIRE_CALL(imu_mock, begin())
					.RETURN(Chordata::KC::who_am_i);
				
				kc->bang();

				REQUIRE(kc->setupDone() == true);	

				WHEN("A user bangs it more times"){
					THEN("It should read the 3 sensors in the imu"){
						REQUIRE_CALL(imu_mock, readGyro())
							.LR_SIDE_EFFECT(imu_mock.gx = 0xaa)
							.LR_SIDE_EFFECT(imu_mock.gy = 0xbb)
							.LR_SIDE_EFFECT(imu_mock.gz = 0xcc)
							;

						REQUIRE_CALL(imu_mock, readAccel())
							.LR_SIDE_EFFECT(imu_mock.ax = 0xaa)
							.LR_SIDE_EFFECT(imu_mock.ay = 0xbb)
							.LR_SIDE_EFFECT(imu_mock.az = 0xcc)
							;

						REQUIRE_CALL(imu_mock, readMag())
							.LR_SIDE_EFFECT(imu_mock.mx = 0xaa)
							.LR_SIDE_EFFECT(imu_mock.my = 0xbb)
							.LR_SIDE_EFFECT(imu_mock.mz = 0xcc)
							;
						
						kc->bang();
						
					}

				}

			}
		}

		WHEN("A user bangs it for the first time, but there's an error"){
			THEN("It should not complete the initial setup of the imu"){

				REQUIRE_CALL(imu_mock, begin())
					.RETURN(Chordata::KC::who_am_i + 1);
					

				try{
					kc->bang();
					FAIL("The setup was indeed completed ");
				} catch (const Chordata::IO_Error e){
					INFO("The setup wasn't complete :)")
				} catch (...){
					FAIL("An unknown exception was raised");
				}

				REQUIRE(kc->setupDone() == false);	

			}
		}
	}
}


//////////////////////////////////
/// Branch 
//////////////////////////////////
SCENARIO("A Branch can be \"banged\" "
	"and it should write the mux channel to it's MUX address thought I2C",
	"[nodes][i2c_mock]"){
	GIVEN("A branch object, with a parent MUX's address = 0x72"){
		Chordata::Node_Scheluder ndsch{};
		Chordata_test::I2C_io_MOCK i2c_mock{};
		Chordata_test::LSM9DS0_MOCK imu_mock{ &i2c_mock };

		CHECK_NOTHROW(create_temp_file(filename,xml_correct));
		CHECK_NOTHROW(create_temp_file(schema_filename,xsd_v1_0));

		auto conf = Chordata::createConfig_with_filenames(path, schema_path);
		conf.append_path();

		Chordata::Armature_Parser axmlp(&ndsch, conf);
		axmlp.set_io(&i2c_mock);
		auto xml = Chordata_test::basicConfigXML();
		auto mux_xml = Chordata_test::appendElement(xml, "Mux", "The Mux", "1","0x6e");
		auto branch_xml = Chordata_test::appendElement(mux_xml, "Branch", "The branch", "1","CH_2");


		auto mux = std::unique_ptr<Chordata::Mux>(axmlp.make<Chordata::Mux>(nullptr, mux_xml));
		auto branch = axmlp.make<Chordata::Branch>(mux.get(), branch_xml);


		WHEN("A user bangs it"){
			THEN("It should perform the write and change the mux active channel"){
				{
					REQUIRE_CALL(i2c_mock, I2CwriteByte(branch->getAddress(), branch->getChannel()));
						// .RETURN(Chordata::SUCCESS);
					
					branch->bang();
				}
				REQUIRE(mux->getActiveChannel() == branch->getChannel());
			}
		}
	}
}



//////////////////////////////////
/// MultiLink 
//////////////////////////////////


SCENARIO("A MultiLink can have one parent and a limited number of children", "[nodes]"){
	GIVEN("An empty MultiLink, with a capacity of 8"){
		size_t max_possible_size = std::numeric_limits<std::size_t>::max();
		Chordata::MultiLink ml = Chordata::MultiLink(nullptr, 8);
		
		THEN("It shouldn't allow a user to ask for a child outside it's capacity"){
			CHECK_THROWS( ml.getChild(8) );
			
			CHECK_THROWS( ml.getChild( max_possible_size ));
			CHECK_THROWS( ml.getChild( max_possible_size+9 )); //This should overflow to just 8
		}

		THEN("It shouldn't have parent or children"){
			REQUIRE( ml.getParent() == nullptr );
			REQUIRE( ml.getChildrenN() == 0 );
			REQUIRE_THROWS( ml.getChild() );
			REQUIRE_THROWS( ml.getChild( max_possible_size+1 ) ); //This should overflow to 0

			WHEN("A user adds 3 children to the Multilink"){
					for (int i = 0; i < 3; ++i)	{
						REQUIRE_NOTHROW(ml.addChild( new Chordata::SimpleLink(&ml)));
					}

				THEN("A user should be able to get all the children"){
					REQUIRE( ml.getChildrenN() == 3 );
					REQUIRE_NOTHROW( ml.getChild(0) );
					CHECK_THROWS( ml.getChild( max_possible_size+8 )); //This should overflow to 7

				}		

				THEN("The children's parent should be == this Multilink"){
					for (int i = 0; i < 3; ++i)	{
						REQUIRE(ml.getChild(i)->getParent() == &ml);
						
					}					
				}

				WHEN("A user adds another 5 children (8 in total)"){
					THEN("It'should't complain"){
						for (int i = 0; i < 5; ++i)	{
							REQUIRE_NOTHROW(ml.addChild( new Chordata::SimpleLink(&ml)));
						}
						WHEN("A user adds another child (9 in total)"){
							THEN("It'should throw"){
								REQUIRE_THROWS(ml.addChild( new Chordata::SimpleLink(&ml)));
							}
						}
					}
				}	

			}
		}
	}
}


//////////////////////////////////
/// SimpleLink 
//////////////////////////////////



SCENARIO("The SimpleLink can have only one parent, and only one child", 
	"[nodes]"){
	GIVEN("An empty SimpleLink"){
		Chordata::SimpleLink sl = Chordata::SimpleLink();
		
		THEN("It shouldn't have parent or child"){
			REQUIRE( sl.getParent() == nullptr );
			REQUIRE( sl.getChildrenN() == 0 );
			REQUIRE( sl.getChild() == nullptr);
		}

		WHEN("The only possible Child is added"){
			REQUIRE_NOTHROW(sl.addChild( new Chordata::SimpleLink() ));

			THEN("It should have a child and no parent"){
				REQUIRE( sl.getParent() == nullptr );
				REQUIRE( sl.getChildrenN() == 1 );
				REQUIRE( sl.getChild() != nullptr);

				WHEN("Yet another child is added"){
					THEN("It should throw an exception"){
						REQUIRE_THROWS(sl.addChild( new Chordata::SimpleLink(&sl) ));
					}
				}
			}
		}

		THEN("It shouldn't allow a user to ask for a child diff than the #0"){
			REQUIRE_THROWS( sl.getChild( 1 ) );
			REQUIRE_THROWS( sl.getChild( -1 ) );
		}
	}

	GIVEN("A SimpleLink with a parent"){
		Chordata::SimpleLink sl = Chordata::SimpleLink( new Chordata::SimpleLink() );
		THEN("A user should be able to get it's parent"){
			Chordata::Link *slp;
			REQUIRE_NOTHROW(slp = sl.getParent());
			REQUIRE(slp != nullptr);
			REQUIRE(slp->getParent() == nullptr);
			REQUIRE(slp->getChild() == nullptr);
		}
	}
}

