/**
 * @file Chordata_test_parser.cpp
 * Testing the parsing classes.
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2017/10/29
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 * -catch library: Copyright (c) 2012 Two Blue Cubes Ltd, under the Boost Software License, Version 1.0
 * -trompeloeil mocking framework: Copyright Björn Fahller 2014-2017 under the Boost Software License, Version 1.0
 *
 */

#include <iostream>
#include <string>
#include <fstream>
#include <memory> 

#include "catch.hpp"
#include "trompeloeil.hpp"
#include "../lib/fmt/core.h"

#include "Chordata_test.h"
#include "Chordata_test_xml_examples.h"
#include "Chordata_parser.h"
#include "Chordata_node.h"
#include "Chordata_def.h"

using Catch::Matchers::Contains;
using Catch::Matchers::Equals;
using Chordata::createConfig_with_filenames;
using Chordata::createDefaultConfig;


using trompeloeil::_;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

namespace Chordata_test{
	//TO PLACE MOCKS
}

TEST_CASE("TODO Parser", "[.TODO]"){
	WARN("Indent Adress and Channel informatio properly on logging");
	WARN("Right now the cmd_line parsing is overwrited by the XML one");
	WARN("Parse the raw property in xml");

}


//////////////////////////////////
/// CONFIGURATION DATA 
//////////////////////////////////

SCENARIO("The Configuration_Data structure hold all the configuration parameters","[config]"){
	GIVEN("A void Configuration_Data "){
		Chordata::Configuration_Data d;
		THEN("It should hold only the executable path"){
			REQUIRE_THAT(d.exe_path, Contains("/"));

			WHEN("Somehow the filename holding parameters get filled with relative paths, and append_path() is called"){
				d.xml.filename = "../Chord.xml";
				d.xml.schema = "../Chord.xsd";
				d.comm.filename = "chor_log";
				d.append_path();
				THEN("These variables should hold an absolute path"){
					CHECK_THAT(d.xml.filename, Contains(d.exe_path));
					CHECK_THAT(d.xml.schema, Contains(d.exe_path));
					CHECK_THAT(d.comm.filename, Contains(d.exe_path));


				}
			}

			WHEN("Somehow the filename holding parameters get filled with absolute paths, and append_path() is called"){
				d.xml.filename = "/Chord.xml";
				d.xml.schema = "/home/pippo/megalomanic/Chord.xsd";
				d.comm.filename = "/tmp/chor_log";
				d.append_path();
				THEN("These variables should remain untouched"){
					CHECK_THAT(d.xml.filename, !Contains(d.exe_path));
					CHECK_THAT(d.xml.schema, !Contains(d.exe_path));
					CHECK_THAT(d.comm.filename, !Contains(d.exe_path));


				}
			}
		}
	}
}


//////////////////////////////////
/// CMD_LINE
//////////////////////////////////

TEST_CASE("I want to see the output of calling the program with --version","[.V]"){
	const char *args[] = {"./notochord", "--version"};
	
	try{
		Chordata::parse_cmd_line(2, args );
		
	} catch (Chordata::CMDLine_Parse_Error& e) {
		std::cout << e.what() << std::endl;
	}


}

TEST_CASE("I want to see the output of calling the program with -h","[.h]"){
	const char *args[] = {"./notochord", "-h"};
	
	try{
		Chordata::parse_cmd_line(2, args );
		
	} catch (Chordata::CMDLine_Parse_Error& e) {
		std::cout << e.what() << std::endl;
	}


}

TEST_CASE("I want to see the output of calling the program with -W (not allowed flag)","[.W]"){
	const char *args[] = {"./notochord", "-W"};
	
	try{
		Chordata::parse_cmd_line(2, args );
		
	} catch (Chordata::CMDLine_Parse_Error& e) {
		std::cout << e.what() << std::endl;
	}


}

SCENARIO("The Command_Line_Parser parses the arguments "
	"passed by the user when calling the excecution of the program ",
	"[parser][cmd_line]"){

	WHEN("A user calls it with some wrong argument"){
		const char *args[] = {"./notochord", "-W"};
		THEN("It should trow an \"CMDLine_Parse_Error\" with the original library exception inside")
		
		REQUIRE_THROWS_AS([&]{
			try{
				Chordata::parse_cmd_line(2, args );
			} catch (const Chordata::CMDLine_Parse_Error& e){
				throw e.get_original();
			}
			
		} (), const args::Error&);
	}

	WHEN("A user calls it with a correct argument, but without a value"){
		const char *args[] = {"./notochord", "-c"};
		THEN("It should trow an \"CMDLine_Parse_Error\" with the original library exception inside")
		
		REQUIRE_THROWS_AS([&]{
			try{
				Chordata::parse_cmd_line(2, args );
			} catch (const Chordata::CMDLine_Parse_Error& e){
				throw e.get_original();
			}
			
		} (), const args::Error&);
	}

	WHEN("A user calls the program with the two positional arguments"){
		const char *args[] = {"./notochord", "192.168.21.222", "6565"};
		const Chordata::Configuration_Data config =
					Chordata::parse_cmd_line(3, args );

		THEN("It should fill the configuration struct with the default, and the user data "){
			CHECK_THAT(config.info.description, Equals(_NOTOCHORD_DESCRIPTION));
			CHECK_THAT(config.info.notes, Equals(_NOTOCHORD_NOTES));
			CHECK(config.info.version == _CHORDATA_VERSION_STR );

			CHECK_THAT(config.xml.filename, Contains(config.exe_path) && Contains(_CHORDATA_CONF));
			CHECK_THAT(config.xml.schema, Contains(config.exe_path) && Contains(_CHORDATA_CONF_SCHEMA));

			CHECK_THAT(config.comm.ip, Equals("192.168.21.222"));
			CHECK( config.comm.port == 6565 );
			REQUIRE_THAT(config.comm.adapter, Equals(_CHORDATA_I2C_DEFAULT_ADAPTER_));

		}
	}

	WHEN("A user calls the program with no arguments"){
		const char *args[] = {"./notochord"};
		const Chordata::Configuration_Data config =
					Chordata::parse_cmd_line(1, args );


		THEN("It should fill the configuration struct with the default data"){
			REQUIRE_THAT(config.info.description, Equals(_NOTOCHORD_DESCRIPTION));
			REQUIRE_THAT(config.info.notes, Equals(_NOTOCHORD_NOTES));
			REQUIRE(config.info.version == _CHORDATA_VERSION_STR );

			REQUIRE_THAT(config.comm.ip, Equals(_CHORDATA_TRANSMIT_ADDR));
			REQUIRE(config.comm.port == _CHORDATA_TRANSMIT_PORT );
			REQUIRE_THAT(config.comm.adapter, Equals(_CHORDATA_I2C_DEFAULT_ADAPTER_));

		}
	}

	CHECK(system("rm -rf " TEMP_FILES_PATH) != -1);
	
}


SCENARIO("The command line parser should create a valid config file",
	"[parser][cmd_line][xml][config]"){
	WHEN("A user calls the program with both configuration files flags pointing at valid files"){
		std::string filename = "Chordata_test_xml_correct.xml";
		std::string path = std::string(TEMP_FILES_PATH) + filename;

		CHECK_NOTHROW(create_temp_file(filename,xml_correct));
		CHECK_NOTHROW(create_temp_file(schema_filename,xsd_v1_0));

		const char *args[] = {"./notochord", 
							"-c", path.c_str(),
							"--schema", schema_path.c_str()
							};

		Chordata::Configuration_Data config;
				
		CHECK_NOTHROW(config = Chordata::parse_cmd_line(5	, args ) );

		CHECK_THAT(config.xml.filename, Equals(path));
		CHECK_THAT(config.xml.schema, Equals(schema_path));



		THEN("An intance of XML_Parser should throw no error "
			"when initialized using the configuration data from the parsing"){
			REQUIRE_NOTHROW(Chordata::XML_Parser( config ));

		}
	}

	WHEN("A user calls the program with a configuration file flag pointing at a NOT valid file"){
		std::string filename = "Chordata_test_xml_FAKE.xml";
		std::string path = std::string(TEMP_FILES_PATH) + filename;
		
		const char *args[] = {"./notochord", 
							"-c", path.c_str(),
							"--schema", schema_path.c_str()
							};

		Chordata::Configuration_Data config;
				
		CHECK_NOTHROW(config = Chordata::parse_cmd_line(5	, args ) );


		CHECK_THAT(config.xml.filename, Equals(path));
		CHECK_THAT(config.xml.schema, Equals(schema_path));

		THEN("An intance of XML_Parser should throw an error "
			"when initialized using the configuration data from the parsing"){
			REQUIRE_THROWS(Chordata::XML_Parser( config ));

		}
	}

}

SCENARIO("The command line parser should observe some constrains "
	"on the values passed to the communication flags","[parser][cmd_line]"){
	
	Chordata::Output_Redirect result = Chordata::NONE;

	Chordata::Output_Redirect expected_result = Chordata::STDOUT;
	expected_result |= Chordata::OSC; //only the asignement OR operator is everloaded for Output_Redirect

	Chordata::Cmd_Line_Validator_OutputFlag validate;

	WHEN("value == `stdout`"){
		validate("boh", "stdout", result);
		THEN("The result should be == Chordata::STDOUT"){
			REQUIRE(result == Chordata::STDOUT);
		}

		WHEN("another value == `osc`"){
			validate("boh", "osc", result);
			THEN("The result should be == Chordata::STDOUT | Chordata::OSC "){
				REQUIRE(result == expected_result);
			}
		}
	}

	WHEN("value == `none`"){
		validate("boh", "none", result);
		THEN("The result should be == Chordata::NONE"){
			REQUIRE(result == Chordata::NONE);
		}
	}

	WHEN("the value is not allowed"){
		THEN("It should throw and in the what() should describe which flag wasn't right"){
			REQUIRE_THROWS_WITH(validate("the_flag", "boniato", result), Contains("the_flag"));
		}
	}

}


//////////////////////////////////
/// ARMATURE 
//////////////////////////////////



const std::string Chordata_test::Test_Helper::get_osc_addr(const Chordata::K_Ceptor * kc){
	return kc->osc_addr;
}

typedef Chordata::Configuration_Data C_data;

SCENARIO("The Armature_Parser manages the armature-specific xml parsing",  
	"[parser][xml][armature]"){
	Chordata::Node_Scheluder ndsch{};
	GIVEN("A correct xml file"){
		std::string filename = "Chordata_test_xml_correct.xml";
		std::string path = std::string(TEMP_FILES_PATH) + filename;

		CHECK_NOTHROW(create_temp_file(filename,xml_correct));
		CHECK_NOTHROW(create_temp_file(schema_filename,xsd_v1_0));

		C_data conf = createConfig_with_filenames(path, schema_path);
		conf.osc.base_address = "/Chordata_test";
		conf.append_path();


		Chordata::Armature_Parser axmlp( &ndsch, conf);
		axmlp.set_io(new I2C_io(new I2C_Device("", false)));

		WHEN("A user asks for the Armature node"){
			THEN("It should return it"){
				REQUIRE_THAT(axmlp.getArmatureNode()->Value(), Equals("Armature"));
			}
		}

		GIVEN("A fake xml node, representing a Branch"){
			auto xml = Chordata_test::basicConfigXML();
			auto mux = Chordata_test::appendElement(xml,"Mux","TheMux", "0", "0x6e");
			auto label = "L_leg";
			auto branch = Chordata_test::appendElement(mux, "Branch", label, "1","\n\t\t\tCH_1\t\t\n\r");


			CHECK_THAT(branch->Value(), Equals("Branch") );

			WHEN("I get's parsed, with a Mux Parent"){
				THEN("It should return the a Branch * casted to a Link * "){
					auto muxNode = axmlp.make<Chordata::Mux>(nullptr, mux);
					auto branch_link = axmlp.parseNode(branch, muxNode, 0 );
					auto branch_node = dynamic_cast<Chordata::Branch*>(branch_link);
					REQUIRE( branch_node != nullptr );
					REQUIRE(branch_node->getChannel() == Chordata::CH_1);	
					REQUIRE_THAT(branch_node->getLabel(), Equals(label));
					REQUIRE((int)branch_node->getAddress() == 0x6e);		

				}
			}
			WHEN("I get's parsed, with a non Mux Parent"){
				THEN("It should throw"){
					auto kc_xml = Chordata_test::appendElement(branch, "K_Ceptor", "kc", "1","0x6e");
					auto kcNode = axmlp.make<Chordata::KC>(nullptr, mux);
					REQUIRE_THROWS_AS (axmlp.parseNode(branch, kcNode, 0 ), Chordata::Hierarchy_Error);
				}
			}
			WHEN("I get's parsed, with a non Mux Parent"){
				THEN("It should throw"){
					auto link = Chordata::SimpleLink();
					REQUIRE_THROWS_AS (axmlp.parseNode(branch, &link, 0 ), Chordata::Hierarchy_Error);
				}
			}
		}

		GIVEN("A fake xml node, representing a correct K_Ceptor"){
			auto xml = Chordata_test::basicConfigXML();
			auto label = "L_leg_sensor";
			auto kc = Chordata_test::appendElement(xml, "K_Ceptor", label, "1","\n\t\n\t 0x6e \t\t\n\r");
			float c[] = {-23.45, 0 , 7.890};
			auto calibVal = fmt::format("{:.6f} {:.6f} {:.6f}", c[0], c[1], c[2]);
			Chordata_test::appendElement(kc,"calibration", "", "", calibVal.c_str() )->SetAttribute("type","offset");
			
			CHECK_THAT(kc->Value(), Equals("K_Ceptor") );

			WHEN("I get's parsed"){
				THEN("It should return the a K_Ceptor* casted to a Link* "){
					auto kc_link = axmlp.parseNode(kc, nullptr, 0 );
					auto kc_node = dynamic_cast<Chordata::KC*>(kc_link);
					REQUIRE( kc_node != nullptr );
					REQUIRE( kc_node->getMagOffset()[0] != 0);
					REQUIRE( kc_node->getMagOffset()[0] == c[0]);
					REQUIRE( kc_node->getMagOffset()[2] == c[2]);	
					REQUIRE_THAT(kc_node->getLabel(), Equals(label));
					REQUIRE((int)kc_node->getAddress() == 0x6e);
					REQUIRE_THAT(Chordata_test::Test_Helper::get_osc_addr(kc_node), 
							Contains(conf.osc.base_address) && 
							Contains(label));		

				}
			}
		}

		GIVEN("A fake xml representing a K_Ceptor, with errors:"){
			auto xml = Chordata_test::basicConfigXML();
			auto label = "L_leg_sensor";
			SECTION("Wrong address"){
				auto kc = Chordata_test::appendElement(xml, "K_Ceptor", label, "1","\n\t\n\t pippo \t\t\n\r");
				WHEN("I get's parsed, it should throw"){
					REQUIRE_THROWS_AS(axmlp.parseNode(kc, nullptr, 0 ), Chordata::Logic_Error);
				}
			}
			SECTION("Address out of range"){
				auto kc = Chordata_test::appendElement(xml, "K_Ceptor", label, "1","\n\t\n\t 129 \t\t\n\r");
				WHEN("I get's parsed, it should throw"){
					REQUIRE_THROWS_AS(axmlp.parseNode(kc, nullptr, 0 ), Chordata::Logic_Error);
				}
			}
			SECTION("A Branch as child"){
				auto kc = Chordata_test::appendElement(xml, "K_Ceptor", label, "1","\n\t\n\t 0x6e \t\t\n\r");
				WHEN("I get's parsed, it should throw"){
					label = "L_leg";
					auto branch = Chordata_test::appendElement(kc, "Branch", label, "1","\n\t\t\tCH_1\t\t\n\r");
					REQUIRE_THROWS_AS(axmlp.parseNode(kc, nullptr, 0 ), Chordata::Hierarchy_Error);
				}
			}
		}

		GIVEN("A fake node representing a Mux"){
			auto xml = Chordata_test::basicConfigXML();
			auto label = "The mux";
			auto mux = Chordata_test::appendElement(xml,"Mux", label, "0", "0x6e");

			WHEN("It get's parsed with a legal number of children"){
			Chordata_test::appendElement(mux, "Branch", "a branch", "1","CH_1");
			Chordata_test::appendElement(mux, "Branch", "a branch", "2","CH_2");
			Chordata_test::appendElement(mux, "Branch", "a branch", "3","CH_3");
				THEN("It should return the a Mux* casted to a Link* "){
					auto mux_link = axmlp.parseNode(mux, nullptr, 0 );
					auto mux_node = dynamic_cast<Chordata::Mux*>(mux_link);
					REQUIRE( mux_node != nullptr );
					REQUIRE_THAT(mux_node->getLabel(), Equals(label));

				}
			}

			WHEN("It get's parsed, but has too much children ( > _CHORDATA_MUX_MAX )"){
				for (int i = 0; i < _CHORDATA_MUX_MAX + 1; ++i)				{
					Chordata_test::appendElement(mux, "Branch", "a branch", "237","OFF");
				}
				THEN("It should throw "){
					REQUIRE_THROWS_AS(axmlp.parseNode(mux, nullptr, 0 ), Chordata::Hierarchy_Error);

				}
			}

			WHEN("It get's parsed, but has a K_Ceptor child"){
				Chordata_test::appendElement(mux, "K_Ceptor", "maldito!", "10","0x6e");

				THEN("It should throw "){
					REQUIRE_THROWS_AS(axmlp.parseNode(mux, nullptr, 0 ), Chordata::Hierarchy_Error);

				}
			}

			WHEN("It get's parsed, but has a Mux child"){
				Chordata_test::appendElement(mux, "Mux", "maldito mux!", "10","0x6e");

				THEN("It should throw "){
					REQUIRE_THROWS_AS(axmlp.parseNode(mux, nullptr, 0 ), Chordata::Hierarchy_Error);

				}
			}			
		}

		
		CHECK(system("rm -rf " TEMP_FILES_PATH) != -1);

	}
}

//////////////////////////////////
/// XML 
//////////////////////////////////

SCENARIO("The XML_Parser parses the Configuration_Data from the <config>.xml file", 
		"[parser][xml][config]"){
	GIVEN("An xml file with configuration data on it"){
		std::string filename = "Chordata_test_xml_correct.xml";
		std::string path = std::string(TEMP_FILES_PATH) + filename;

		CHECK_NOTHROW(create_temp_file(filename,xml_correct));
		CHECK_NOTHROW(create_temp_file(schema_filename,xsd_v1_0));

		C_data conf = createConfig_with_filenames(path, schema_path);
		conf.append_path();


		WHEN("The parser is instantiated"){
			Chordata::XML_Parser xmlp( conf );
			THEN("It should overwrite the passed Configuration_Data reference members"){
				REQUIRE_THAT(conf.comm.adapter, Equals(_XML_E_ADAPTER));
				REQUIRE_THAT(conf.osc.base_address, Equals(_XML_E_BASE_ADDRESS));
				REQUIRE_THAT(conf.comm.ip, Equals(_XML_E_IP));
				REQUIRE(conf.comm.log == (_XML_E_LOG_RESULT ));

			}
		}
	}
}


SCENARIO("The XML_PARSER can handle the basic operations of opening, validating and parsing xml files",
	"[parser][xml]"){
	
	GIVEN("A correct xml file"){
		std::string filename = "Chordata_test_xml_correct.xml";
		std::string path = std::string(TEMP_FILES_PATH) + filename;

		CHECK_NOTHROW(create_temp_file(filename,xml_correct));
		CHECK_NOTHROW(create_temp_file(schema_filename,xsd_v1_0));

		C_data conf = createConfig_with_filenames(path, schema_path);
		conf.append_path();

		Chordata::XML_Parser xmlp( conf );

		WHEN("A user asks for the basenode"){
			THEN("It should return it"){
				REQUIRE_NOTHROW(xmlp.getBaseNode());
			}
		}

		WHEN("A user asks for the version"){
			THEN("It should return it"){
				REQUIRE_THAT(xmlp.getVersion(), Equals("1.0"));
			}
		}
		
		CHECK(system("rm -rf " TEMP_FILES_PATH) != -1);

	}

	GIVEN("An xml file with no <Chordata> BaseNode"){
		std::string filename = "Chordata_test_xml_noBaseNode.xml";
		std::string path = std::string(TEMP_FILES_PATH) + filename;	
		
		CHECK_NOTHROW(create_temp_file(filename, xml_noBaseNode));
		CHECK_NOTHROW(create_temp_file(schema_filename,xsd_v1_0));

		C_data conf = createConfig_with_filenames(path, schema_path);
		conf.append_path();

		
		WHEN("A user tries to create an instance"){
			THEN("It should throw"){
				REQUIRE_THROWS( Chordata::XML_Parser( conf ));
			}
		}
		
		CHECK(system("rm -rf " TEMP_FILES_PATH) != -1);
	}

	GIVEN("An xml file with some formal error like a tag unclosed"){
		std::string filename = "Chordata_test_xml_noClosingTag.xml";
		std::string path = std::string(TEMP_FILES_PATH) + filename;

		CHECK_NOTHROW(create_temp_file(filename, xml_noClosingTag));
		CHECK_NOTHROW(create_temp_file(schema_filename,xsd_v1_0));

		C_data conf = createConfig_with_filenames(path, schema_path);
		conf.append_path();


		WHEN("The parser is initialized"){
			THEN("It should throw"){
				REQUIRE_THROWS( Chordata::XML_Parser( conf ));
				
			}
		}

		CHECK(system("rm -rf " TEMP_FILES_PATH) != -1);
	}

	GIVEN("A Chordata config without the version attribute on the initial tag"){
		std::string filename = "Chordata_test_xml_noVersion.xml";
		std::string path = std::string(TEMP_FILES_PATH) + filename;

		CHECK_NOTHROW(create_temp_file(filename, xml_noVersion));
		CHECK_NOTHROW(create_temp_file(schema_filename,xsd_v1_0));

		C_data conf = createConfig_with_filenames(path, schema_path);
		conf.append_path();


		WHEN("A user tries to create an instance"){
			THEN("It should throw"){
				REQUIRE_THROWS( Chordata::XML_Parser( conf ));
			}
		}

		CHECK(system("rm -rf " TEMP_FILES_PATH) != -1);
	}
}


