/**
 * @file Chordata_test.cpp
 * The main file for the testing procedure.
 * To be included in other test files
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2017/10/11
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 * -catch library: Copyright (c) 2012 Two Blue Cubes Ltd, under the Boost Software License, Version 1.0
 * -trompeloeil mocking framework: Copyright Björn Fahller 2014-2017 under the Boost Software License, Version 1.0
 *
 */

#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include "trompeloeil.hpp"


#include <string>
#include <cstddef>
#include <iostream>
#include "Chordata_test.h"
#include "Chordata_utils.h"
#include "Chordata_def.h"
#include "Chordata_test_xml_examples.h"


//Adaptation snipet for trompeloeil with catch
//https://github.com/rollbear/trompeloeil/blob/master/docs/CookBook.md/#use-trompeloeil-with-catch
namespace trompeloeil
{
	template <>
	void reporter<specialized>::send(
	  severity s,
	  const char* file,
	  unsigned long line,
	  const char* msg)
	{
	  std::ostringstream os;
	  if (line) os << file << ':' << line << '\n';
	  os << msg;
	  auto failure = os.str();
	  if (s == severity::fatal)
	  {
	    FAIL(failure);
	  }
	  else
	  {
	    CAPTURE(failure);
	    CHECK(failure.empty());
	  }
	}
}

#include <osc/OscReceivedElements.h>
#include <osc/OscPacketListener.h>

#include <csignal>
#include <unistd.h>

namespace Chordata_test{
	#include <iostream>
	#include <cstring>


	//////////////////////////////////
	/// TEST OSC SERVER
	/// Gets specific messages, and respondes on pipe 
	//////////////////////////////////
	

	class ExamplePacketListener : public osc::OscPacketListener {
	public:
		std::string recibed;

		ExamplePacketListener(int _pipe, const Chordata::Configuration_Data& d): 
		pipe(_pipe),
		config(d)
		{
			const char* init =  "ready\n";
			write(pipe, init, strlen(init));
		}
		
	protected:
		int pipe;
		const Chordata::Configuration_Data& config;

	    virtual void ProcessMessage( const osc::ReceivedMessage& m, 
					const IpEndpointName& remoteEndpoint )
	    {
	        (void) remoteEndpoint; // suppress unused parameter warning

	        try{
	            // example of parsing single messages. osc::OsckPacketListener
	            // handles the bundle traversal.
	            const char* message = "(DUMP OF OSC ADDR: ";
	           	write(pipe, message, strlen(message));
	            write(pipe, m.AddressPattern(), strlen(m.AddressPattern()));
	            message = ") ";
	           	write(pipe, message, strlen(message));

	            if( std::strcmp( m.AddressPattern(), (config.osc.base_address + config.osc.comm_address).c_str() ) == 0 ){


	            	osc::ReceivedMessage::const_iterator arg = m.ArgumentsBegin();

	            	recibed = (arg++)->AsString();

	            	if( arg != m.ArgumentsEnd() ){
	            		const char* error = "Too much arguments in the OSC message"; 
	            		write(pipe, error, strlen(error));
						close(pipe);
		            	};
	            	    
	            	write(pipe, recibed.c_str(), strlen(recibed.c_str()));
	            	write(pipe, "\n", 1);

	                
	            } else if( std::strcmp( m.AddressPattern(), config.osc.base_address.c_str() ) == 0 ){
	            	osc::ReceivedMessage::const_iterator arg = m.ArgumentsBegin();

	            	recibed = (arg++)->AsString();

	            	write(pipe, recibed.c_str(), strlen(recibed.c_str()));

	            	auto fpipe = fdopen(pipe, "w");

	            	fmt::MemoryWriter w;
	            	for (int i = 0; i < 4; ++i)
	            	{
	            		w << ',' << (arg++)->AsFloat() ;
	            		
	            	}
	            	write(pipe, w.c_str(), strlen(w.c_str()));

	            	if( arg != m.ArgumentsEnd() ){
	            		const char* error = "Too much arguments in the OSC message"; 
	            		write(pipe, error, strlen(error));
						close(pipe);
		            	};

	            	write(pipe, "\n", 1);

	                // std::cout << "OSC RECIBED, unknown address" << std::endl;
	            }


	        }catch( osc::Exception& e ){
	            // any parsing errors such as unexpected argument types, or 
	            // missing arguments get thrown as exceptions.
	            fmt::MemoryWriter w;
	            w << "error while parsing message: "
	                << m.AddressPattern() << ": " << e.what() << "\n";

	            write(pipe, w.c_str(), strlen(w.c_str()));
            	close(pipe);

	        }
	    }
	};

	class RAII_Kill{
	int pid;
	public:
		RAII_Kill(int p): pid(p){};
		~RAII_Kill(){
	    	//Give the child some time to get its breath back 
	    	//(don't know why this is necessary, but without it doesn't works)
			thread_sleep(Chordata::millis(100));
			//Unmercyfully kill the child
			kill(pid, SIGKILL);
		}
	};

	int pipefd[2];


}

const Chordata::Configuration_Data& Chordata_test::create_config_for_OSC(){
	static Chordata::Configuration_Data d;
	d.comm.filename = _CHORDATA_FILEOUT_NAME;
	d.comm.log_level = 1;
	d.comm.ip = _XML_E_IP;
	d.comm.port = std::stoi(_XML_E_PORT);
	d.append_path();

	d.osc.base_address = _XML_E_BASE_ADDRESS;
	d.osc.comm_address = "/info_subaddr";
	d.osc.error_address = "/error_subaddr";

	d.comm.log = Chordata::STDERR;
	d.comm.log |= Chordata::FILE;

	d.comm.error = Chordata::FILE;

	d.comm.transmit = Chordata::OSC;
	d.comm.transmit |= Chordata::STDERR;

	return d;
}


int main( int argc, char* argv[] ) {
  	int result;
  	// Setup the OSC test server 
  	auto d = Chordata_test::create_config_for_OSC();
	pipe(Chordata_test::pipefd);

	try{
		Chordata_test::ExamplePacketListener listener(Chordata_test::pipefd[1], d);
		UdpListeningReceiveSocket s(
		        IpEndpointName(IpEndpointName::ANY_ADDRESS, d.comm.port),
		        &listener );

		auto pid = fork();

		//set a timeout, in case the OSC server doesn't responde
		std::thread t([pipefd = Chordata_test::pipefd]{
			thread_sleep(Chordata::millis(3000));
			const char* timeout =  "Timeout on waiting for OSC server to responde\n";
			write(pipefd[1], timeout, strlen(timeout));
		});
		t.detach();

		if (pid == 0){ //Child's part
			close(Chordata_test::pipefd[0]);
			
			//Detach the child from the terminal, in order to don't see it's agony ;)
			freopen( "/dev/null", "r", stdin);
		    freopen( "/dev/null", "w", stdout);
		    freopen( "/dev/null", "w", stderr);

			s.Run();

		} else { //Parent's part
			Chordata_test::RAII_Kill kill_child(pid);

			close(Chordata_test::pipefd[1]);
			
  			result = Catch::Session().run( argc, argv );

        	close(Chordata_test::pipefd[0]);
        }

	} catch ( const std::runtime_error& e){
		std::cout << "Couldn't start the test, a runtime error was raised while forking the ";
		std::cout << "process to run the OSC test server.\n >>> ";
		std::cout << e.what() << std::endl;

		if (std::strcmp(e.what(), "unable to bind udp socket\n") == 0){
			std::cout << "Perhaps there's another running process using the same port? (";
			std::cout << Chordata_test::create_config_for_OSC().comm.port << ")\n";
			std::cout << "Check `netstat -u --all --program`" << std::endl;
		}


	}





  // global clean-up...

  return result;
}

