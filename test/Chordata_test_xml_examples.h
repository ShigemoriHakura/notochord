/**
 * @file Chordata_xml_examples.h
 * Embeding xml documents to be used by the tests
 *
 * @author Bruno Laurencich
 * @version 0.1.0 
 * @date 2017/10/11
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 * -catch library: Copyright (c) 2012 Two Blue Cubes Ltd, under the Boost Software License, Version 1.0
 * -trompeloeil mocking framework: Copyright Björn Fahller 2014-2017 under the Boost Software License, Version 1.0
 *
 */

#ifndef _CHORDATA_XML_EXAMPLES_
#define _CHORDATA_XML_EXAMPLES_
#include <string>
#include "tinyxml2.h"

extern std::string xsd_v1_0;

extern std::string xml_correct;

extern std::string xml_noBaseNode;

extern std::string xml_noClosingTag;

extern std::string xml_noVersion;


namespace Chordata_test{
	extern tinyxml2::XMLDocument xmlDoc;

	tinyxml2::XMLElement *basicConfigXML();

	tinyxml2::XMLError saveXML(tinyxml2::XMLDocument &xmlDoc, const char* filepath);

	tinyxml2::XMLElement *appendElement(tinyxml2::XMLElement *,  const char* type,const char* name, const char* id, const char* value = "");
}


void create_temp_file(const std::string, const std::string);

#define _XML_E_ADAPTER  "/dev/i2c-1"

#define _XML_E_BASE_ADDRESS  "/FakeBase"

#define _XML_E_IP "127.0.0.1"

#define _XML_E_PORT "7123"

#define _XML_E_LOG "stderr, file"

#define _XML_E_LOG_RESULT Chordata::STDERR | Chordata::FILE

#define TEMP_FILES_PATH "/tmp/xml_test_files/"

const std::string schema_filename = "Chordata_test.xsd";
const std::string schema_path = std::string(TEMP_FILES_PATH) + schema_filename;


#endif