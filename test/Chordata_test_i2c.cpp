/**
 * @file Chordata_test_i2c.cpp
 * Test the i2c functionality.
 *
 * @author Bruno Laurencich
 * @version 1.0 
 * @date 2017/10/11
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: Copyright 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 * -catch library: Copyright (c) 2012 Two Blue Cubes Ltd, under the Boost Software License, Version 1.0
 * -trompeloeil mocking framework: Copyright Björn Fahller 2014-2017 under the Boost Software License, Version 1.0
 *
 */

#include <cstdint>
#include <array>

#include "catch.hpp"
#include "trompeloeil.hpp"

#include "Chordata_def.h"
#include "Chordata_error.h"
#include "../lib/i2c_manager.h"
#include "../lib/fmt/core.h"
#include "Chordata_imu.h"


using Catch::Matchers::Contains;
using trompeloeil::_;

extern template struct trompeloeil::reporter<trompeloeil::specialized>;

namespace Chordata_test{
	//TO PLACE MOCKS
}

#define _KC_XOR 0x0F

#define WHO_AM_I WHO_AM_I_XG

#define _GX_ADDR LSM9DS1_AG_ADDR(1)
#define _M_ADDR LSM9DS1_M_ADDR(1)

#define _GX_ID WHO_AM_I_AG_RSP
#define _M_ID WHO_AM_I_M_RSP

#define __SAMPLES__ 20

TEST_CASE("TODO I2C", "[.TODO]"){
	WARN("on read/write error inform about which node caused the problem");

}

SCENARIO("The I2C_Device represents a hardware device handled from user space through an adapter.\n"
	"The I2C_io is responsable for read/write operation throught this adapter.\n"
	"The LSM9DS1 handles the specific IMU setup and read/write"
	"[!] --> This test may fail in systems without an adapter exposed on `/dev/i2c-1`",
	"[.i2c_adapter][.i2c_device][!mayfail]"){
	GIVEN("An I2C_Device instance"){
		I2C_Device dev = I2C_Device(_CHORDATA_I2C_DEFAULT_ADAPTER_, false);
		
		WHEN("The init() member is called, It shoudl open the device if available on the system"){
			REQUIRE(dev.init() == Chordata::SUCCESS);
			INFO("Adaptor " << _CHORDATA_I2C_DEFAULT_ADAPTER_ 
					<< " open sucesfully.");

			GIVEN( fmt::format("[!] --> A Physical K_Ceptor device plugged to the i2c port of this system, "
				"with an XOR translation value = 0x{:2X} \n(otherwise test will fail)", _KC_XOR) ){
				I2C_io io(&dev);

				WHEN("A user tries to read the gyro/acel WHO_AM_I register"){
					THEN(fmt::format("Response should be {:8b}", _GX_ID)){
						REQUIRE(_GX_ID == io.I2CreadByte( (_GX_ADDR ^ _KC_XOR) , WHO_AM_I ) );
					}
				}
				WHEN("A user tries to read the mag WHO_AM_I register"){
					THEN(fmt::format("Response should be {:8b}", _M_ID)){
						REQUIRE(_M_ID == io.I2CreadByte( (_M_ADDR ^ _KC_XOR) , WHO_AM_I ) );
	
					}
				}

				GIVEN("An instance of the LSM9DS1 "){
					auto imu = make_LSM9DS1(&io, _KC_XOR);
					WHEN("A user calls begin(), it should perform the setup on the sensor, and return the WHO_AM_I values"){
						REQUIRE(imu->begin() == ((_GX_ID << 8) | _M_ID));

						WHEN("Some readings of the accelerometer are made"){
							THEN("They should return values != 0, and different between them"){
								using read = std::array<int16_t, 3>;

								std::array<read, __SAMPLES__> list;
								
								int differents = 0; 

								for (int i = 0; i < __SAMPLES__; ++i)
								{
									imu->readAccel();
									list[i] = {imu->ax, imu->ay, imu->az};
									REQUIRE(imu->ax != 0);
									REQUIRE(imu->ay != 0);
									REQUIRE(imu->az != 0);

									for (int j = i-1; j >= 0; --j)
									{	
										if (j < 0) continue;
										if(list[i] != list[j]) differents++;
										break;
									}

								}
								if ( differents / (__SAMPLES__ -1 ) < 0.5)
									WARN("All the readed values from the sensor where equal, try moving the sensor while performing the test");
								else 
									WARN("Readed values decently different");
							}	
						}
					}
				} //end "An instance of the LSM9DS1 "
			} //end A Physical K_Ceptor device...
		}
	} // end An I2C_Device instance..
}

