#ifndef CHORDATA_DISPLAY_H
#define CHORDATA_DISPLAY_H

#include "i2c_manager.h"

#define BLACK 0
#define WHITE 1
#define INVERSE 2

#define Diaplay_I2C_ADDRESS 0x3C

#define WIDTH 128
#define HEIGHT 64

#define Diaplay_LCDWIDTH 128
#define Diaplay_LCDHEIGHT 64

#define Diaplay_SETCONTRAST 0x81
#define Diaplay_DISPLAYALLON_RESUME 0xA4
#define Diaplay_DISPLAYALLON 0xA5
#define Diaplay_NORMALDISPLAY 0xA6
#define Diaplay_INVERTDISPLAY 0xA7
#define Diaplay_DISPLAYOFF 0xAE
#define Diaplay_DISPLAYON 0xAF

#define Diaplay_SETDISPLAYOFFSET 0xD3
#define Diaplay_SETCOMPINS 0xDA

#define Diaplay_SETVCOMDETECT 0xDB

#define Diaplay_SETDISPLAYCLOCKDIV 0xD5
#define Diaplay_SETPRECHARGE 0xD9

#define Diaplay_SETMULTIPLEX 0xA8

#define Diaplay_SETLOWCOLUMN 0x00
#define Diaplay_SETHIGHCOLUMN 0x10

#define Diaplay_SETSTARTLINE 0x40

#define Diaplay_MEMORYMODE 0x20
#define Diaplay_COLUMNADDR 0x21
#define Diaplay_PAGEADDR 0x22

#define Diaplay_COMSCANINC 0xC0
#define Diaplay_COMSCANDEC 0xC8

#define Diaplay_SEGREMAP 0xA0

#define Diaplay_CHARGEPUMP 0x8D

#define Diaplay_EXTERNALVCC 0x1
#define Diaplay_SWITCHCAPVCC 0x2

// Scrolling #defines
#define Diaplay_ACTIVATE_SCROLL 0x2F
#define Diaplay_DEACTIVATE_SCROLL 0x2E
#define Diaplay_SET_VERTICAL_SCROLL_AREA 0xA3
#define Diaplay_RIGHT_HORIZONTAL_SCROLL 0x26
#define Diaplay_LEFT_HORIZONTAL_SCROLL 0x27
#define Diaplay_VERTICAL_AND_RIGHT_HORIZONTAL_SCROLL 0x29
#define Diaplay_VERTICAL_AND_LEFT_HORIZONTAL_SCROLL 0x2A

void Diaplay_begin(unsigned int switchvcc, I2C_io *io); //switchvcc should be Diaplay_SWITCHCAPVCC
void Diaplay_command(unsigned int c);

void Diaplay_clearDisplay(void);
void Diaplay_invertDisplay(unsigned int i);
void Diaplay_display();

void Diaplay_startscrollright(unsigned int start, unsigned int stop);
void Diaplay_startscrollleft(unsigned int start, unsigned int stop);

void Diaplay_startscrolldiagright(unsigned int start, unsigned int stop);
void Diaplay_startscrolldiagleft(unsigned int start, unsigned int stop);
void Diaplay_stopscroll(void);

void Diaplay_dim(unsigned int dim);

void Diaplay_drawPixel(int x, int y, unsigned int color);

void Diaplay_drawFastVLine(int x, int y, int h, unsigned int color);
void Diaplay_drawFastHLine(int x, int y, int w, unsigned int color);

void Diaplay_fillRect(int x, int y, int w, int h, int fillcolor);

void Diaplay_setTextSize(int s);
void Diaplay_drawString(char *str);
void Diaplay_drawString(const char *str);
void Diaplay_drawChar(int x, int y, unsigned char c, int color, int size);

void Diaplay_show(char *str);
void Diaplay_show(const char *str);

#endif /* _Diaplay_I2C_H_ */
