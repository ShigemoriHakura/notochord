/**
 * @file Chordata_messages.h
 * Definition of messages structures to be sent through OSC
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef __CHORDAT_SYMBOLS_
#define __CHORDAT_SYMBOLS_

#include <vector>
#include <string>
#include <cstdint>
#include "Chordata_utils.h"
// #include "Chordata_node.h"



namespace Chordata {
	struct Q_msg{
		std::string addr;
		Quaternion q;

		template <typename Node>
		Q_msg(const Node *, Quaternion&&);
		// Q_msg(const char *s, Quaternion&& q);
	};

	struct RAW_msg{
		std::string addr;
		int16_t gx, gy, gz;
		int16_t ax, ay, az;
		int16_t mx, my, mz;

		template <typename Node>
		RAW_msg(const Node *, 	int16_t, int16_t, int16_t, 
								int16_t, int16_t, int16_t, 
								int16_t, int16_t, int16_t);

	};

	struct POINT_CLOUD_msg{
		std::string addr;
		int16_t mx, my, mz;

		template <typename Node>
		POINT_CLOUD_msg(const Node *, int16_t, int16_t, int16_t);
	};


	class COPP_Bundle_Manager{
	public:
		std::vector<Q_msg> q_list;
		std::vector<RAW_msg> raw_list;
		std::vector<POINT_CLOUD_msg> pointc_list;

		bool q_data, raw_data, pointc_data;

		COPP_Bundle_Manager():
		q_data(false), raw_data(false) 
		{}

		template <typename Node>
		void append(const Node *, Quaternion&&);
		
		template <typename Node>
		void append(const Node *, 	int16_t, int16_t, int16_t, 
									int16_t, int16_t, int16_t, 
									int16_t, int16_t, int16_t );

		template <typename Node>
		void append(const Node *, 	int16_t, int16_t, int16_t);

		
	};

}



template <typename Node>
void Chordata::COPP_Bundle_Manager::append(const Node *n, Quaternion&& q){
	q_list.emplace_back(n, std::move(q));
	q_data = true;
}

template <typename Node>
void Chordata::COPP_Bundle_Manager::append(const Node *n, int16_t gx, int16_t gy, int16_t gz,
		        					int16_t ax, int16_t ay, int16_t az,
		        					int16_t mx, int16_t my, int16_t mz){
	raw_list.emplace_back(n, gx, gy, gz, ax, ay, az, mx, my, mz);
	raw_data = true;
}

template <typename Node>
void Chordata::COPP_Bundle_Manager::append(const Node *n, int16_t mx, int16_t my, int16_t mz){
	pointc_list.emplace_back(n, mx, my, mz);
	pointc_data = true;
}

template <typename Node>
Chordata::Q_msg::Q_msg(const Node *n, Quaternion&& _q):
	addr(n->osc_addr),
	q(_q)
	{}

template <typename Node>
Chordata::RAW_msg::RAW_msg(const Node *n, int16_t _gx, int16_t _gy, int16_t _gz,
		        					int16_t _ax, int16_t _ay, int16_t _az,
		        					int16_t _mx, int16_t _my, int16_t _mz):
	addr(n->raw_osc_addr),
	gx(_gx), gy(_gy), gz(_gz),
	ax(_ax), ay(_ay), az(_az),
	mx(_mx), my(_my), mz(_mz)
	{}


template <typename Node>
Chordata::POINT_CLOUD_msg::POINT_CLOUD_msg(const Node *n, int16_t _mx, int16_t _my, int16_t _mz):
	addr(n->pointc_osc_addr),
	mx(_mx), my(_my), mz(_mz)
	{}
// Chordata::Q_msg::Q_msg(const char *s, Quaternion&& q)



#endif