/**
 * @file Chordata_fusion.h
 * TODO description. 
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef __CHORDATA_FUSION__
#define __CHORDATA_FUSION__ 


#include "Chordata_def.h"
#include "Chordata_communicator.h"


namespace FSsf{
	extern "C" {
	  	#include "FS_sensor_fusion/build.h"
		#include "FS_sensor_fusion/types.h"
		#include "FS_sensor_fusion/fusion.h"
		#include "FS_sensor_fusion/magnetic.h"
		#include "FS_sensor_fusion/orientation.h"

	}



	namespace CoordSystem{
		const FSsf::int16 _NED = NED;
		const FSsf::int16 _ANDROID = ANDROID;
		const FSsf::int16 _WIN8 = WIN8;
	};
}

namespace Chordata{

	struct SensorResolution{
		float gRes; //in DPS/tick 
		float aRes; //in G/tick
		float mRes;	//in uT/tick
		
		void convert_GAUSS_to_uT(){
			mRes = 100*mRes;
		}
	};



	class Kalman_filter {
		FSsf::SV_9DOF_GBY_KALMAN thisSV;
		FSsf::AccelSensor thisAccel;
		FSsf::GyroSensor thisGyro;
		FSsf::MagneticBuffer thisMagBuffer;

		uint32_t loopcounter;

		FSsf::int16 skip_mag_calibration;
		FSsf::fquaternion result;

	public:

		//These should be private
		FSsf::int8 iCounter;
		FSsf::MagSensor thisMag;
		FSsf::MagCalibration thisMagCal;
		static const FSsf::fquaternion rotate_match_coord_sys;
		//end should be private

		Kalman_filter(FSsf::int16);

		void set_gyro(FSsf::int16, FSsf::int16, FSsf::int16);
		void set_acel(FSsf::int16, FSsf::int16, FSsf::int16);
		void set_mag(FSsf::int16, FSsf::int16, FSsf::int16);
		void set_sensitivity(float, float, float);
		void get_mag_offset_for_EEPROM(std::array<int16_t, 3>&, std::array<std::array<float, 3>, 3>&);
		void set_mag_cal_from_EEPROM(uint32_t, const float*, const std::array<std::array<float, 3>, 3>&);
		void set_mag_cal_from_EEPROM_v010( const float*, const std::array<std::array<float, 3>, 3>&);

		void process_readings();
		bool process_and_run();
		bool run_mag_calib();
		void run_filter();

		const FSsf::fquaternion& get_quat(){
			
			FSsf::qAeqBxC(&result, &rotate_match_coord_sys, &thisSV.fqPl );
			
			return result;
		}


		float get_a_covariance() const {
			return thisAccel.fQvGQa;
		};

		float get_m_covariance() const {
			return thisMag.fQvBQd;
		}


		template <typename Node>
		void transmit_mag_pointcloud(const Node *n){
			int cc = 0;
			static int j, k;

			for (j = 0; j < MAGBUFFSIZEX; j++){
				for (k = 0; k < MAGBUFFSIZEY; k++){
					if (thisMagBuffer.index[j][k] != -1){
						cc++;
						Chordata::Communicator::transmit( n ,
							thisMagBuffer.iBs[0][j][k],
							thisMagBuffer.iBs[1][j][k],
							thisMagBuffer.iBs[2][j][k]);
						

					}
				}
			}
			Chordata::Communicator::debug("Magnetic buffer length: {} ", cc);

			Chordata::Communicator::transmit_pointc(); 
		}


	};
}



#endif