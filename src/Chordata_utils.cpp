/**
 * @file Chordata_utils.cpp
 * Utilities. 
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "Chordata_utils.h"
#include "Chordata_def.h"
#include "Chordata_error.h"

#include <iostream>
#include <unistd.h>//system()
#include <libgen.h>//dirname()
#include <cstdint>
#include <string>
#include <algorithm>
#include <cstdlib>
#include <math.h>
#include <iterator>

#include <thread>
#include <functional>
#include <fstream>
#include <memory>
#include <regex>


using std::cout;		using std::endl;
using std::cerr;
using std::string;		using std::copy;
using std::ostream;		using std::vector;
using std::ostream_iterator;
using std::ios;



Chordata::Configuration_Data::Configuration_Data(): //from Chordata_def.h
		exe_path(Chordata::get_exe_path()),
		hardware_concurrency(std::thread::hardware_concurrency()),
		wait(true),
		raw(false),
		calib(false),
		scan(false),
		kc_ver(-1),
		odr(_CHORDATA_ODR),
		runner_mode(false),
		state({.beta_timer_init = 0, .imu_setup_complete = false})
		{

		info.description = _NOTOCHORD_DESCRIPTION;
		info.notes = _NOTOCHORD_NOTES;
		info.version = _CHORDATA_VERSION;

		xml.filename = _CHORDATA_CONF;
		xml.schema = _CHORDATA_CONF_SCHEMA;

		comm.ip = _CHORDATA_TRANSMIT_ADDR;
		comm.port = _CHORDATA_TRANSMIT_PORT;
		comm.adapter = _CHORDATA_I2C_DEFAULT_ADAPTER_;
		comm.filename = _CHORDATA_FILEOUT_NAME;
		comm.log_level = _CHORDATA_VERBOSITY;
		comm.send_rate = _CHORDATA_SEND_RATE;
		comm.use_bundles = true;

		comm.log = _CHORDATA_DEF_LOG_REDIR;
		comm.error = _CHORDATA_DEF_ERR_REDIR;
		comm.transmit = _CHORDATA_DEF_TRA_REDIR;

		osc.base_address = _CHORDATA_DEF_OSC_ADDR;
		osc.error_address = _CHORDATA_DEF_ERROR_SADDR;
		osc.comm_address = _CHORDATA_DEF_COM_SADDR;

		fusion.beta_s = fusion.beta_f = _MADGWICK_BETA;
		fusion.time = _MADGWICK_BETA_MANGLING_TIME;
		fusion.madgwick = false;

		append_path();

		};

void Chordata::Configuration_Data::append_path(){
	static std::regex regex("(/\\w.*)+");
	auto doit = [this](std::string& member, const char * subdir = nullptr ){
		if (!std::regex_match(member, regex ) || member.empty())
			member = fmt::format("{}{}{}/{}", 
				this->exe_path,
				(subdir == nullptr)? "": "/", 
				(subdir == nullptr)? "": subdir, 
				member);
	};

	if (xml.filename == _CHORDATA_CONF)
		doit(xml.filename);

	if (xml.schema == _CHORDATA_CONF_SCHEMA)
		doit(xml.schema);
	
	doit(comm.filename, _CHORDATA_FILEOUT_DIR);



}
//////////////////////////////////
/// create a directory 
//////////////////////////////////

int Chordata::create_dir(const char* path){

	 return system(fmt::format("mkdir -p {}", path).c_str());
		
	}

std::string Chordata::get_exe_path(){
	const size_t bufsize = 256;
	char buf[bufsize];
	uint16_t bytes = readlink("/proc/self/exe", buf, bufsize);
	if (-1 != bytes ){
		buf[bytes] = '\0';
		return dirname(buf);
	} else {
		throw Chordata::System_Error(errno, "Can't get the executable path");
	}
}



//////////////////////////////////
/// SPLIT A STRING on whitespaces, and discard the excess 
//////////////////////////////////

string* splitMax(const string& s, string* buf, size_t max){

	string* pos = buf;
	typedef string::const_iterator iter;
	iter i = s.begin();
	// invariant: we have processed characters [original value of i, i)
	// and we haven't use the last slot of the buffer
	while (i != s.end() && pos - buf != max) {
		// ignore leading blanks
		// invariant: characters in range [original i, current i) are all spaces
		i = find_if(i, s.end(), not_space);

		// find end of next word
		iter j = find_if(i, s.end(), space);
		// if we found some nonwhitespace characters
		if (i != s.end()) {
			// copy from s starting at i and taking j - i chars
			// cout << s.substr(i, j - i) << endl; 
			*pos = string(i, j);
			i = j;
			++pos;
		}
	}
	return pos;
}

// handlers for splitMax
bool space(char c)
{
return isspace(c);
}
bool not_space(char c)
{
return !isspace(c);
}

/////////////////////////////////
///HELPERS
/////////////////////////////////

bool verifyIP(const string& s){
	typedef string::const_iterator iter;
	iter i;
	iter start = s.begin();
	char count = 0;
	//while the result of finding a '.' is diferent to the end() 
	while (( i = find(start , s.end(), '.') ) != s.end()){
		string part(start, i);

		if ( !(atoi(part.c_str()) >= 0 && atoi(part.c_str()) < 256)) return false;
		
		start = i + 1;

		if (++count == 4) return false;
	}
	string part(start, i);
	if ( !(atoi(part.c_str()) >= 0 && atoi(part.c_str()) < 256)) return false;

	return true;
}


template<class T>
ostream& operator<<(ostream& os, const vector<T>& v)
{
    copy(v.begin(), v.end(), ostream_iterator<T>(os, " ")); 
    return os;
}

//////////////////////////////////
/// FOR TESTING INDIVIDUAL UTILS 
//////////////////////////////////


#ifdef __UTILS_TEST__

using namespace Chordata;
using namespace std;

int main(int argc, char const *argv[])
{
	Chordata::initCommunicator();
	Chordata::output("lala");

	// Timekeeper t;

	// for (int i = 0; i < 10000000; ++i)
	// {
	// 	/* code */
	// }

	// uint64_t e1 = t.ellapsedMicros();

	// uint64_t e2 = t.ellapsedMillis();

	// cout << "MICROS " << e1 << endl;

	// cout << "MILLIS " << e2 << endl;

	// for (int i = 0; i < 100000000; ++i)
	// {
	// 	/* code */
	// }

	// e1 = t.ellapsedMicros();

	// e2 = t.ellapsedMillis();

	// cout << "MICROS " << e1 << endl;

	// cout << "MILLIS " << e2 << endl;

	return 0;
}

#endif