/**
 * @file Chordata_error.h
 * Definitions of error objects
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef __CHORDATA_ERROR_
#define __CHORDATA_ERROR_

#include "Chordata_utils.h"
#include "tinyxml2.h"
#include "args.hxx"

#include <iostream>
#include <exception>
#include <cstdint>
#include <string>

using std::string;

namespace Chordata{

	typedef void (*comfn)(std::string);
	// comfn out = &Chordata::Comunicator::log; 
	// comfn err = &Chordata::Comunicator::error;
	// comfn debug = &Chordata::Comunicator::debug;

	struct Error: public std::exception {
		const string msg;
		uint64_t running_time;

		Error(uint64_t time = 0):
			msg("Unknown"),
			running_time(time)
			{};

		Error(string _msg = "Unknown", uint64_t time = 0): 
			msg(_msg), 
			running_time(time)
			{};

		virtual const char* what() const noexcept override{
			return msg.c_str();
		}

	};

	struct IO_Error: public Error{
		static long int count;
		
		IO_Error(string _msg = "Unknown", uint64_t time = 0): 
		Error(_msg, time) {
			++ count;
		};
	};

	struct Hierarchy_Error: public Error{
		
		Hierarchy_Error(string _msg = "Unknown", uint64_t time = 0): 
		Error(_msg, time) 
		{};
	};

	struct Idiot_Error: public Error{
		
		Idiot_Error(string _msg = "", uint64_t time = 0): 
		Error("Idiot, look carefully at your code!\n" +_msg, time) 
		{};
	};

	struct Logic_Error: public Error/*, public std::logic_error*/{
		Logic_Error(string _msg = "Unknown", uint64_t time = 0):
		Error(_msg, time)
		// std::logic_error(_msg) 
		{};

		// virtual const char* what() const noexcept override{
		// 	return Error::what();
		// }
	};
	

	struct System_Error: public Error/*, public std::system_error*/{
		int c;
		System_Error(int ec, string _msg = "Unknown", uint64_t time = 0):
		Error(_msg, time),
		c(ec)
		// std::system_error(std::error_code(ec, std::system_category())) 
		{};

		int code() const noexcept{
			return c;
		};

		// virtual const char* what() const noexcept override{
		// 	return Error::what();
		// }
	};



}
/////////////////////////////////
///PARSING ERRORS
/////////////////////////////////
#define ARMATURE_XML_ERROR(msg, XML_E) XML_Parse_Error(msg, ARMATURE_PARSING, XML_E)

	//TODO: include filename (pass the parser?)
namespace Chordata{

	 
	struct Parsing_Error: public Error{
	public:	
		const std::string errorCause;
		
		Parsing_Error() = delete; //DEFAULT CONSTRUCTOR NOT ALLOWED

		Parsing_Error(std:: string _cause, 
					Parsing_stage _stage, uint64_t time = 0):
		 errorCause( fmt::format("Error parsing Chordata's config files \n[Stage: {}]\n"
		 						"Cause: {}", msgs::stages[_stage], _cause).c_str()),
		 stage(_stage),
		 Error(time) 
		 {};


		virtual const char* what() const throw() {
			return cause();
		}

		virtual const char* cause() const throw() {
			return errorCause.c_str();
		}


	private:
		std::string filename;
		Parsing_stage stage;
	};

	struct CMDLine_Parse_Error: public Parsing_Error{
		public:
		CMDLine_Parse_Error(std::string _cause, 
				args::Error& aerror,
				uint64_t time = 0 
				):
				Parsing_Error(_cause, CMD_LINE_PARSING, time),
				args_error(aerror),
				output(
					fmt::format("{}\n{}", (typeid(aerror) == typeid(args::Help)) ? "": aerror.what(), _cause).c_str()					 
					)
				{};

		virtual const char* what() const throw() {
			return output.c_str();
		}

		const auto& get_original() const{
			return args_error;
		}
		// static bool checkHelp(args::Error error){
		// 	return dynamic_cast<args::Help>(error) != nullptr;
		// }

		private:
		std::string output; 
		args::Error args_error;
	};


	struct XML_Parse_Error: public Parsing_Error{
		public:
		XML_Parse_Error(std::string _cause, Parsing_stage _stage,
				tinyxml2::XMLError xerror = tinyxml2::XML_SUCCESS,
				uint64_t time = 0 ):
				Parsing_Error(_cause, _stage, time),
				xml_error(xerror)
				 {};

		virtual const char* what() const throw() {
			return cause();
		}

	private:
		tinyxml2::XMLError xml_error;
	};

}

#endif
