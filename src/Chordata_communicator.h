/**
 * @file Chordata_communicator.h
 * Declarations for the communicator namespace, and definitions of the communicator basic functions
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef _CHORDATA_COMMUNICATOR_
#define _CHORDATA_COMMUNICATOR_

#include <functional>
#include <string>
#include <cstdint>
#include <vector>
#include <mutex>

#include "spdlog/spdlog.h"
#include "spdlog/sinks/base_sink.h"
#include "osc/OscOutboundPacketStream.h"
#include "ip/UdpSocket.h"
#include "osc/OscTypes.h"
#include "ip/NetworkingUtils.h"

// #include "Notochord.h"
#include "Chordata_utils.h"
#include "Chordata_def.h"
#include "Chordata_error.h"
#include "Chordata_messages.h"

// #include <iostream>

namespace Chordata
{
	extern config_fn getConf;

	namespace Communicator
	{

		inline Chordata::COPP_Bundle_Manager &get_bundle_manager()
		{
			static Chordata::COPP_Bundle_Manager man;
			return man;
		}

		template <typename Node>
		void bundle_append(const Node *n, Quaternion &&q)
		{
			get_bundle_manager().append(n, std::move(q));
		}

		template <typename Node>
		void bundle_append(const Node *n, int16_t gx, int16_t gy, int16_t gz,
											 int16_t ax, int16_t ay, int16_t az,
											 int16_t mx, int16_t my, int16_t mz)
		{
			get_bundle_manager().append(n, gx, gy, gz, ax, ay, az, mx, my, mz);
		}

		template <typename Node>
		void bundle_append(const Node *n, int16_t mx, int16_t my, int16_t mz)
		{
			get_bundle_manager().append(n, mx, my, mz);
		}

		void bundle_clear();

		extern long log_attemps;

		typedef std::function<void(const std::string &, const Quaternion &)> transmit_fn_t;
		typedef std::function<void(const std::string &)> comm_fn_t;

		extern Chordata::Timekeeper timekeeper;

		struct Comm_Substrate
		{
			//A temporary structure that fills the vectors with spdlog::sinks,
			//which are then moved to the communication functions
			using redirects = std::vector<spdlog::sink_ptr>;
			redirects transmit;
			redirects error;
			redirects log;

			spdlog::level::level_enum log_level, err_level;

			Chordata::Configuration_Data config;

			explicit Comm_Substrate(const Chordata::Configuration_Data &);
		};

		class OscOut : public spdlog::sinks::base_sink<std::mutex>
		{

		public:
			explicit OscOut(const Chordata::Configuration_Data &conf) : ip(GetHostByName(conf.comm.ip.c_str())),
																																	port(conf.comm.port),
																																	base_address(conf.osc.base_address),
																																	comm_address(conf.osc.base_address + conf.osc.comm_address),
																																	error_address(conf.osc.base_address + conf.osc.error_address),
																																	transmitSocket(),
																																	buffer(),
																																	p(buffer, _CHORDATA_TRANSMIT_BUFFER_SIZE)
			{
				transmitSocket.SetEnableBroadcast(true);
				transmitSocket.Connect(IpEndpointName(ip, port));
			};

			void sink_it_(const spdlog::details::log_msg &msg) override;

			void transmit_q_bundle(int);

			void transmit_raw_bundle(int);

			void transmit_pointc_bundle();

			void transmit(const std::string &, const Quaternion &);

			void transmit(const std::string &, int16_t gx, int16_t gy, int16_t gz,
										int16_t ax, int16_t ay, int16_t az,
										int16_t mx, int16_t my, int16_t mz);

			void transmit(const std::string &s, float a, float b);

			void flush_() override;

		private:
			unsigned long ip;
			uint16_t port;
			std::string base_address, comm_address, error_address;

			UdpSocket transmitSocket;
			char buffer[_CHORDATA_TRANSMIT_BUFFER_SIZE];
			osc::OutboundPacketStream p;
		};

		extern transmit_fn_t transmit_handler;

		extern comm_fn_t output_handler;

		extern comm_fn_t trace_handler;

		extern comm_fn_t debug_handler;

		extern comm_fn_t info_handler;

		extern comm_fn_t info_undisplay_handler;

		extern comm_fn_t warn_handler;

		extern comm_fn_t err_handler;

		inline void debug(const std::string &s)
		{
			debug_handler(s);
		}

		template <typename... Args>
		inline void debug(const char *fmt, const Args &...args)
		{
			debug_handler(fmt::format(fmt, args...));
		}

		inline void trace(const std::string &s)
		{
			trace_handler(s);
		}

		template <typename... Args>
		inline void trace(const char *fmt, const Args &...args)
		{
			trace_handler(fmt::format(fmt, args...));
		}

		template <typename... Args>
		inline void try_debug(const char *fmt, const Args &...args)
		{
			try
			{
				debug_handler(fmt::format(fmt, args...));
			}
			catch (const std::bad_function_call &)
			{
				Chordata::Communicator::log_attemps++;
			}
		}

		template <typename FROM>
		inline void info_once(FROM f, const std::string &s)
		{
			static bool doit = true;
			if (!doit)
				return;
			doit = true;
			info_handler(s);
		}

		template <typename... Args, typename FROM>
		inline void info_once(FROM f, const char *fmt, const Args &...args)
		{
			static bool doit = true;
			if (!doit)
				return;
			doit = true;

			info_handler(fmt::format(fmt, args...));
		}

		inline void info(const std::string &s)
		{
			info_handler(s);
		}

		template <typename... Args>
		inline void info(const char *fmt, const Args &...args)
		{
			info_handler(fmt::format(fmt, args...));
		}

		inline void info_un(const std::string &s)
		{
			info_undisplay_handler(s);
		}

		template <typename... Args>
		inline void info_un(const char *fmt, const Args &...args)
		{
			info_undisplay_handler(fmt::format(fmt, args...));
		}

		template <typename... Args>
		inline void try_info(const char *fmt, const Args &...args)
		{
			try
			{
				info_handler(fmt::format(fmt, args...));
			}
			catch (const std::bad_function_call &)
			{
				Chordata::Communicator::log_attemps++;
			}
		}

		template <typename... Args>
		inline void try_info_un(const char *fmt, const Args &...args)
		{
			try
			{
				info_undisplay_handler(fmt::format(fmt, args...));
			}
			catch (const std::bad_function_call &)
			{
				Chordata::Communicator::log_attemps++;
			}
		}

		inline void warn(const std::string &s)
		{
			warn_handler(s);
		}

		template <typename... Args>
		inline void warn(const char *fmt, const Args &...args)
		{
			warn_handler(fmt::format(fmt, args...));
		}

		template <typename... Args>
		inline void try_warn(const char *fmt, const Args &...args)
		{
			try
			{
				warn_handler(fmt::format(fmt, args...));
			}
			catch (const std::bad_function_call &)
			{
				Chordata::Communicator::log_attemps++;
			}
		}

		inline void err(const std::string &s)
		{
			err_handler(s);
		}

		template <typename... Args>
		inline void err(const char *fmt, const Args &...args)
		{
			err_handler(fmt::format(fmt, args...));
		}

		template <typename... Args>
		inline void try_err(const char *fmt, const Args &...args)
		{
			try
			{
				err_handler(fmt::format(fmt, args...));
			}
			catch (const std::bad_function_call &)
			{
				Chordata::Communicator::log_attemps++;
			}
		}

		// #include <iostream>
		inline void check_transmit(const std::string &addr, const Quaternion &q)
		{
			static uint32_t micros_to_wait = 1000000 / Chordata::getConf().comm.send_rate;
			static Chordata::microDuration period(micros_to_wait);
			auto delta = timekeeper.get_transmision_delta(addr);

			if (delta >= period)
			{
				transmit_handler(addr, q);
				timekeeper.set_last_transmision(addr);
				// std::cout << addr << " SENT " <<  delta.count() <<  std::endl;
			}
			else
			{
				// std::cout << addr << " ***DROPPED*** " << delta.count() <<  std::endl;
			}
		}

		template <typename Node>
		inline void transmit(const Node *n, Quaternion &&q)
		{
			if (Chordata::getConf().comm.use_bundles)
			{
				bundle_append(n, std::move(q));
				return;
			}

			check_transmit(n->osc_addr, q);
		}

		// inline void transmit(const char *s, Quaternion&& q){
		// 	check_transmit(s, q);
		// 	// transmit_handler(s,q);
		// }

		// inline void transmit(const std::string& s, const Quaternion& q){
		// 	check_transmit(s,q);
		// }

		void transmit_bundles(int);

		void transmit_pointc();

		inline void transmit(const std::string &s,
												 int16_t gx)
		{
			output_handler(s);
		}

		inline void transmit(const std::string &s)
		{
			output_handler(s);
		}

		void transmit(const std::string &, int16_t gx, int16_t gy, int16_t gz,
									int16_t ax, int16_t ay, int16_t az,
									int16_t mx, int16_t my, int16_t mz);

		template <typename Node>
		inline void transmit(const Node *n, int16_t gx, int16_t gy, int16_t gz,
												 int16_t ax, int16_t ay, int16_t az,
												 int16_t mx, int16_t my, int16_t mz)
		{

			if (Chordata::getConf().comm.use_bundles)
			{
				bundle_append(n, gx, gy, gz, ax, ay, az, mx, my, mz);
				return;
			}

			Chordata::Communicator::transmit(n->raw_osc_addr, gx, gy, gz, ax, ay, az, mx, my, mz);
		}

		template <typename Node>
		inline void transmit(const Node *n, int16_t mx, int16_t my, int16_t mz)
		{

			if (Chordata::getConf().comm.use_bundles)
			{
				bundle_append(n, mx, my, mz);
				return;
			}

			// Chordata::Communicator::transmit(n->raw_osc_addr, gx, gy, gz, ax, ay, az, mx, my, mz);
		}

		void transmit(const std::string &, float a, float b);

		void init_communicator(Comm_Substrate &&);

		void init_communicator(const Chordata::Configuration_Data &);

		class initializers
		{
			using spdlevel = spdlog::level::level_enum;

			friend void init_communicator(Comm_Substrate &&);

			static auto transmit_fn(spdlevel, Comm_Substrate::redirects &&, spdlevel);

			static comm_fn_t log_fn(spdlevel, Comm_Substrate::redirects &&, spdlevel, bool display);

			static comm_fn_t error_fn(spdlevel, Comm_Substrate::redirects &&, spdlevel);
		};

	} //namespace Communicator

} //namespace Chordata

#endif
