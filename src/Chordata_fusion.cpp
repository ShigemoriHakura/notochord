/**
 * @file Chordata_fusion.h
 * TODO description. 
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>. *
 * This program uses code from various sources, the default license is GNU GPLv3
 * for all code, the dependencies where originally distributed as follows:
 * -LSM9DS# libraries: CopyrithisSV, ght 2014,2015 Jim Lindblom @ SparkFun Electronics, as beerware.
 * -fmt library: Copyright (c) 2012 - present, Victor Zverovich, Under BSD 2-clause License.
 * -spdlog library: Copyright(c) 2015 Gabi Melman, under MIT license.
 * -args library: Copyright (c) 2016 Taylor C. Richberger, under MIT license.
 * -bcm2835 library: Copyright 2011-2013 Mike McCaule, Under GNU-GPLv2 License.
 * -oscpack library: Copyright (c) 2004-2013 Ross Bencina, under MIT license.
 * -Sensor fusion algorithms: Copyright 2011 SOH Madgwick, Under GNU-GPLv3 License.
 * -tinyxml2 library: Copyright <unknowk year> Lee Thomason, under zlib License.
 * -pstreams library: Copyright (C) 2001 - 2017 Jonathan Wakely, under Boost Software License
 *
 */


#include "Chordata_fusion.h"
#include "Chordata_error.h"
#include "Chordata_communicator.h"



using FSsf::int32; 
using FSsf::int16;
using FSsf::int8;

//This rotation maps the ANDROID coord system in the Kalman library
//to the convention previously used in Chordata
// const Chordata::Kalman_filter::rotate_match_coord_sys.q0 = 0.707106828689575f;
// const Chordata::Kalman_filter::rotate_match_coord_sys.q1 = 0.0f;
// const Chordata::Kalman_filter::rotate_match_coord_sys.q2 = 0.0;
// const Chordata::Kalman_filter::rotate_match_coord_sys.q3 = -0.707106828689575f;

const FSsf::fquaternion Chordata::Kalman_filter::rotate_match_coord_sys = 
					{0.707106828689575f, 0.0f,
					0.0f, -0.707106828689575f};

Chordata::Kalman_filter::Kalman_filter(FSsf::int16 skip):
	iCounter(0),
	skip_mag_calibration(skip)
	{
		thisSV.resetflag = true;
		FSsf::fInit_9DOF_GBY_KALMAN(&thisSV, &thisAccel,  &thisMag, &thisMagCal );
		FSsf::fInitMagCalibration(&thisMagCal, &thisMagBuffer);

		//initiate sensitivities to -1 to flag them as invalid
		thisAccel.fgPerCount = -1;
		thisMag.fCountsPeruT = -1;
		thisMag.fuTPerCount = -1;
		thisGyro.fDegPerSecPerCount = -1;

		// initialize magnetometer data structure
		// zero the calibrated measurement since this is used for indexing the magnetic buffer even before first calibration
		for (int i = CHX; i <= CHZ; i++)
			thisMag.iBcAvg[i]= 0;

	}

void Chordata::Kalman_filter::set_sensitivity(float gRes, float aRes, float mRes){
	//Set the sensors scale resolutions
	thisAccel.fgPerCount = aRes;

	// convert GAUSS to uT
	mRes = 100*mRes;
	thisMag.fCountsPeruT = 1 / mRes;
	thisMag.fuTPerCount = mRes;

	thisGyro.fDegPerSecPerCount = gRes;
}

void Chordata::Kalman_filter::
set_mag_cal_from_EEPROM_v010( const float* _magOffset, const std::array<std::array<float, 3>, 3>& Me ){
	// This is the translation of the EEPROM mapping v 0.1.1

	thisMagCal.fV[0] = - (_magOffset[1] / thisMag.fCountsPeruT);
	thisMagCal.fV[1] = (_magOffset[0] / thisMag.fCountsPeruT); 
	thisMagCal.fV[2] = - (_magOffset[2] / thisMag.fCountsPeruT);

	auto& Mk = thisMagCal.finvW;

	// Reference rearrengment from OCTAVE tests
	//amod = 	[a(2,2), -a(2,1), a(2,3);
    // 			 a(1,2), -a(1,1), a(1,3);
    // 			 a(3,2), -a(3,1), a(3,3)];
    // NOTE: the minus sign in the second column is ignored here, already taken into accoun in the magOffset

	Mk[0][0] = Me[1][1]; Mk[0][1] = Me[1][0]; Mk[0][2] = Me[1][2];
	Mk[1][0] = Me[0][1]; Mk[1][1] = Me[0][0]; Mk[1][2] = Me[0][2]; 
	Mk[2][0] = Me[2][1]; Mk[2][1] = Me[2][0]; Mk[2][2] = Me[2][2]; 

}

void Chordata::Kalman_filter::
set_mag_cal_from_EEPROM( uint32_t version, const float* _magOffset, const std::array<std::array<float, 3>, 3>& _magMatrix ) {
	
	if (thisMag.fCountsPeruT == -1){
		throw Chordata::Idiot_Error("The sensor sensitivities should be initialized in the Kalman_Filter before setting the magnetometer calibration from the EEPROM");
	}

	if (version == _CHORDATA_EEPROM_VERSION_0_1_1){
		set_mag_cal_from_EEPROM_v010(_magOffset, _magMatrix);
		thisMagCal.iValidMagCal = true;
		return;
	}

	thisMagCal.fV[0] = - (_magOffset[0] / thisMag.fCountsPeruT);
	thisMagCal.fV[1] = - (_magOffset[1] / thisMag.fCountsPeruT); 
	thisMagCal.fV[2] = - (_magOffset[2] / thisMag.fCountsPeruT);

	for (int i=0;i<3;i++){
		for (int j=0;j<3;j++){
			thisMagCal.finvW[i][j] = _magMatrix[i][j];

		}
	}

	thisMagCal.iValidMagCal = true;
	return;
}	

void Chordata::Kalman_filter::
get_mag_offset_for_EEPROM(std::array<int16_t, 3>& ee, std::array<std::array<float, 3>, 3>& Me ){
	ee[0] = (thisMagCal.fV[1] * thisMag.fCountsPeruT);
	ee[1] = -(thisMagCal.fV[0] * thisMag.fCountsPeruT);
	ee[2] = -(thisMagCal.fV[2] * thisMag.fCountsPeruT);

	const auto& Mk = thisMagCal.finvW;

	Me[0][0] = Mk[1][1];	Me[0][1] = Mk[1][0]; 	Me[0][2] = Mk[1][2]; 
	Me[1][0] = Mk[0][1]; 	Me[1][1] = Mk[0][0];	Me[1][2] = Mk[0][2];
	Me[2][0] = Mk[2][1]; 	Me[2][1] = Mk[2][0]; 	Me[2][2] = Mk[2][2]; 
}


void Chordata::Kalman_filter::set_gyro( int16 x, int16 y, int16 z ){
	thisGyro.iYs[CHX] = x;
	thisGyro.iYs[CHY] = y;
	thisGyro.iYs[CHZ] = z;

}

void Chordata::Kalman_filter::set_acel( int16 x, int16 y, int16 z ){
	thisAccel.iGs[CHX] = x;
	thisAccel.iGs[CHY] = y;
	thisAccel.iGs[CHZ] = z;

}

void Chordata::Kalman_filter::set_mag( int16 x, int16 y, int16 z ){
	thisMag.iBs[CHX] = x;
	thisMag.iBs[CHY] = y;
	thisMag.iBs[CHZ] = z;

}

void Chordata::Kalman_filter::process_readings(){
	FSsf::int32 iSum[3];					// array of sums
	FSsf::int32 itmp;					// scratch
	FSsf::int8 i, j, k, l;				// counter

	/*----------  GYRO  ----------*/

	// store in a buffer for later gyro integration by sensor fusion algorithm
	for (i = CHX; i <= CHZ; i++)
			thisGyro.iYsBuffer[iCounter][i] = thisGyro.iYs[i];

	/*----------  ACEL  ----------*/
	
	// store measurement in a buffer for later end of block processing
	for (i = CHX; i <= CHZ; i++)
		thisAccel.iGsBuffer[iCounter][i] = thisAccel.iGs[i];

	// every OVERSAMPLE_RATIO passes calculate the block averaged measurement
	if (iCounter == (OVERSAMPLE_RATIO - 1))
	{	
		// calculate the block averaged measurement in counts and g
		for (i = CHX; i <= CHZ; i++)
		{
			iSum[i] = 0;
			for (j = 0; j < OVERSAMPLE_RATIO; j++)
				iSum[i] += (int32)thisAccel.iGsBuffer[j][i];
			// compute the average with nearest integer rounding
			if (iSum[i] >= 0)
				thisAccel.iGsAvg[i] = (int16)((iSum[i] + (OVERSAMPLE_RATIO >> 1)) / OVERSAMPLE_RATIO);
			else
				thisAccel.iGsAvg[i] = (int16)((iSum[i] - (OVERSAMPLE_RATIO >> 1)) / OVERSAMPLE_RATIO);
			// convert from integer counts to float g
			thisAccel.fGsAvg[i] = (float)thisAccel.iGsAvg[i] * thisAccel.fgPerCount;
		}
	} // end of test for end of OVERSAMPLE_RATIO block

	/*----------  MAG  ----------*/

	// store in a buffer for later end of block processing
	for (i = CHX; i <= CHZ; i++)
		thisMag.iBsBuffer[iCounter][i] = thisMag.iBs[i];

	// update magnetic buffer with iBs avoiding a write to the shared structure while a calibration is in progress.
	if (!thisMagCal.iCalInProgress)
		FSsf::iUpdateMagnetometerBuffer(&thisMagBuffer, &thisMag, loopcounter);

	// every OVERSAMPLE_RATIO passes calculate the block averaged and calibrated measurement using an anti-glitch filter
	// that rejects the measurement furthest from the mean. magnetometer sensors are sensitive
	// to occasional current pulses from power supply traces and so on and this is a simple method to remove these.
	if (iCounter == (OVERSAMPLE_RATIO - 1))
	{
		// calculate the channel means using all measurements
		for (i = CHX; i <= CHZ; i++)
		{
			// accumulate channel sums
			iSum[i] = 0;
			for (j = 0; j < OVERSAMPLE_RATIO; j++)
				iSum[i] += (int32)thisMag.iBsBuffer[j][i];
		}
		// store axis k in buffer measurement l furthest from its mean
		itmp = 0;
		for (i = CHX; i <= CHZ; i++)
		{
			for (j = 0; j < OVERSAMPLE_RATIO; j++)
			{
				if (abs((int32)thisMag.iBsBuffer[j][i] * OVERSAMPLE_RATIO - iSum[i]) >= itmp)
				{
					k = i;
					l = j;
					itmp = abs((int32)thisMag.iBsBuffer[j][i] * OVERSAMPLE_RATIO - iSum[i]);
				}
			}
		}

		// re-calculate the block averaged measurement ignoring channel k in measurement l
		if (OVERSAMPLE_RATIO == 1)
		{
			// use the one available measurement for averaging in this case
			for (i = CHX; i <= CHZ; i++)
			{
				thisMag.iBsAvg[i] = thisMag.iBsBuffer[0][i];
			}
		} // end of compute averages for OVERSAMPLE_RATIO = 1
		else
		{
			// sum all measurements ignoring channel k in measurement l
			for (i = CHX; i <= CHZ; i++)
			{
				iSum[i] = 0;
				for (j = 0; j < OVERSAMPLE_RATIO; j++)
				{
					if (!((i == k) && (j == l)))
						iSum[i] += (int32)thisMag.iBsBuffer[j][i];
				}
			}
			// compute the average with nearest integer rounding
			for (i = CHX; i <= CHZ; i++)
			{
				if (i != k)
				{
					// OVERSAMPLE_RATIO measurements were used
					if (iSum[i] >= 0)
						thisMag.iBsAvg[i] = (int16)((iSum[i] + (OVERSAMPLE_RATIO >> 1)) / OVERSAMPLE_RATIO);
					else
						thisMag.iBsAvg[i] = (int16)((iSum[i] - (OVERSAMPLE_RATIO >> 1)) / OVERSAMPLE_RATIO);
				}
				else
				{
					// OVERSAMPLE_RATIO - 1 measurements were used
					if (iSum[i] >= 0)
						thisMag.iBsAvg[i] = (int16)((iSum[i] + ((OVERSAMPLE_RATIO - 1) >> 1)) / (OVERSAMPLE_RATIO - 1));
					else
						thisMag.iBsAvg[i] = (int16)((iSum[i] - ((OVERSAMPLE_RATIO - 1) >> 1)) / (OVERSAMPLE_RATIO - 1));
				}
			}
		} // end of compute averages for OVERSAMPLE_RATIO = 1

		// convert the averages to float
		for (i = CHX; i <= CHZ; i++)
			thisMag.fBsAvg[i] = (float)thisMag.iBsAvg[i] * thisMag.fuTPerCount;

		// remove hard and soft iron terms from fBsAvg (uT) to get calibrated data fBcAvg (uT), iBc (counts)
		FSsf::fInvertMagCal(&thisMag, &thisMagCal);
		
	} // end of test for end of OVERSAMPLE_RATIO block

	// increment the loopcounter (used for time stamping magnetic data)
	loopcounter++;
}


bool Chordata::Kalman_filter::process_and_run(){
	process_readings();
	
	// every OVERSAMPLE_RATIO passes zero the decimation counter and run the sensor fusion
	if (iCounter++ == (OVERSAMPLE_RATIO - 1))
	{
		iCounter = 0;
		run_filter();
		run_mag_calib();
		return true;
	} 

	return false;
}

void Chordata::Kalman_filter::run_filter(){
	FSsf::fRun_9DOF_GBY_KALMAN(&thisSV, &thisAccel, &thisMag, &thisGyro,
		&thisMagCal);
}

bool Chordata::Kalman_filter::run_mag_calib(){
	static FSsf::int8 counter = 0;
	bool initiatemagcal;

	if ( counter++ >= skip_mag_calibration && !thisMagCal.iCalInProgress )
	{
		counter = 0;
		// do the first 4 element calibration immediately there are a minimum of MINMEASUREMENTS4CAL
		initiatemagcal = (!thisMagCal.iMagCalHasRun && (thisMagBuffer.iMagBufferCount >= MINMEASUREMENTS4CAL));

		// otherwise initiate a calibration at intervals depending on the number of measurements available
		initiatemagcal |= ((thisMagBuffer.iMagBufferCount >= MINMEASUREMENTS4CAL) && 
				(thisMagBuffer.iMagBufferCount < MINMEASUREMENTS7CAL) &&
				!(loopcounter % INTERVAL4CAL));
		initiatemagcal |= ((thisMagBuffer.iMagBufferCount >= MINMEASUREMENTS7CAL) &&
				(thisMagBuffer.iMagBufferCount < MINMEASUREMENTS10CAL) &&
				!(loopcounter % INTERVAL7CAL));
		initiatemagcal |= ((thisMagBuffer.iMagBufferCount >= MINMEASUREMENTS10CAL) &&
				!(loopcounter % INTERVAL10CAL));


		// initiate the magnetic calibration if any of the conditions are met
		if (initiatemagcal)
		{
			FSsf::fRunMagCalibration(&thisMagCal, &thisMagBuffer, &thisMag);
		}

	} // end of test that no calibration is already in progress

	return initiatemagcal;
}

