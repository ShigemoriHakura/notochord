/**
 * @file Chordata_utils.h
 * Utilities. 
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef __CHORDATA_UTILS__
#define __CHORDATA_UTILS__

#include <chrono>
#include <iostream>
#include <vector>
#include <cstdint>
#include <thread>
#include <ctime>
#include <cmath>
#include <unordered_map>
#include <deque>


#include "Chordata_def.h"
#include "osc/OscOutboundPacketStream.h"
#include "ip/UdpSocket.h"
#include "osc/OscTypes.h"
#include "fmt/core.h"


//////////////////////////////////
/// Quaternion container 
//////////////////////////////////

struct Quaternion {
	Quaternion(float _w, float _x, float _y, float _z):
	w(_w), x(_x), y(_y), z(_z)
	{};

	float w,x,y,z;
};

namespace Chordata{

	
	int create_dir(const char* path);

	std::string get_exe_path();
}
//////////////////////////////////
/// Check equality in some types (useful in xml parsing to not override cmd_line parsing) 
//////////////////////////////////
template <typename T>
bool are_the_same(const T& a, const T& b){
	return a == b;
}

  
//////////////////////////////////
/// SPLIT A STRING on whitespaces, and discard the excess 
//////////////////////////////////

std::string* splitMax(const std::string& s, std::string* buf, std::size_t max);

bool space(char c);
bool not_space(char c);

/////////////////////////////////
///TIME STUFF
/////////////////////////////////


//TODO: erase
// #define micros std::chrono::microseconds
#define thread_sleep std::this_thread::sleep_for

// inline unsigned long int ellapsedMicros(const time_point t){
// 	return std::chrono::duration_cast<std::chrono::microseconds>(
// 		std::chrono::high_resolution_clock::now() - t).count();
// }

namespace Chordata{
	typedef std::chrono::time_point<std::chrono::high_resolution_clock> time_point;
	typedef std::chrono::high_resolution_clock clock;
	typedef std::chrono::microseconds micros;
	typedef std::chrono::milliseconds millis;
	typedef std::chrono::seconds seconds;

	typedef std::chrono::duration<long, std::ratio<1, 1000000>> microDuration;

	class Timekeeper{
	public:
		typedef std::unordered_map<std::string, time_point> time_table;	
		
		explicit Timekeeper():
		start( clock::now() ), 
		lap ( clock::now() ), 
		counter_beta( clock::now() )
		{};

		time_point reset(){ 
			start = clock::now();
			return start; 
		}


		inline void resetLap() {lap = clock::now();}
		
		inline void initBetaCounter(uint16_t millis) {
			counter_beta = clock::now();
			beta_millis = millis;
		}

		float getBetaCounter() const{
			return std::chrono::duration_cast<millis>(clock::now() - counter_beta ).count() / 
					beta_millis;
		}


		inline uint64_t ellapsedLapMicros() const{
			return std::chrono::duration_cast<micros>(clock::now() - lap ).count();
		}

		inline uint64_t ellapsedMicros() const{
			return std::chrono::duration_cast<micros>(clock::now() - start ).count();
		}

		inline uint64_t ellapsedMillis() const{
			return std::chrono::duration_cast<millis>(clock::now() - start ).count();
		}

		inline double ellapsedSeconds() const{
			return std::chrono::duration_cast<seconds>(clock::now() - start ).count();
		}

		time_point getStart() const {return start;};

		inline const std::time_t initialTimestamp() const{
			return std::chrono::system_clock::to_time_t(start);
		}

		inline const char *initialTime() const{
			const std::time_t time = std::chrono::system_clock::to_time_t(start);
			return std::ctime(&time);
		}

		inline const char *now() const{
			const std::time_t time = std::chrono::system_clock::to_time_t(clock::now());
			return std::ctime(&time);
		}

		static const char* time_str(const std::time_t& time){
			return std::ctime(&time);	
		}

		auto get_transmision_delta(const std::string& s){
			time_point last;
			auto lti = last_transmisions.find(s);
			if ( lti == last_transmisions.end()){
				last_transmisions[s] = start;
				last = start;
			} else {
				last = lti->second; 
			}

			// auto last = last_transmisions.at(s);
			return std::chrono::duration_cast<micros>(clock::now() - last );
		}

		void set_last_transmision(const std::string& s){
			last_transmisions[s] = clock::now();
		}		

	private:
		time_table last_transmisions;
		time_point start;
		time_point lap;
		time_point counter_beta;
		float beta_millis;


	};
}

/////////////////////////////////
///Fit a line on scatter points.
/////////////////////////////////

namespace Chordata{
	// struct Point {
	//   Point(double _x, double _y):
	//   x(_x), y(_y){};

	//   double x, y;
	// };
	using scatter_line = std::deque<int16_t>;

	struct Line {
	  double _slope, _yInt;
	  double get_y(double x) {
	    return _slope*x + _yInt;
	  }

	  double slope() const{
	  	return _slope;
	  }

	  // Construct line from points
	  bool fit_points(const scatter_line& pts) {
	    int nPoints = pts.size();
	    if( nPoints < 2 ) {
	      // Fail: infinitely many lines passing through this single point
	      return false;
	    }
	    double sumX=0, sumY=0, sumXY=0, sumX2=0;
	    for(int i=0; i<nPoints; i++) {
	      sumX += i;
	      sumY += pts[i];
	      sumXY += pts[i] * i;
	      sumX2 += i * i;
	    }
	    double xMean = sumX / nPoints;
	    double yMean = sumY / nPoints;
	    double denominator = sumX2 - sumX * xMean;
	    // You can tune the eps (1e-7) below for your specific task
	    if( std::fabs(denominator) < 1e-7 ) {
	      // Fail: it seems a vertical line
	      return false;
	    }
	    _slope = (sumXY - sumX * yMean) / denominator;
	    _yInt = yMean - _slope * xMean;
	    return true;
	  }
	};


}


/////////////////////////////////
///HELPERS
/////////////////////////////////

// A helper function to output vectors.
template<class T>
std::ostream& operator<<(std::ostream&, const std::vector<T>&);

//Degrees to radians
inline float degToRads(float in){
	return in * M_PI / 180;
}

//Check if the string contains a valid IP address
bool verifyIP(const std::string& s);

#endif


