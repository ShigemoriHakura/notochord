/**
 * @file Chordata_node.h
 * In notochord the nodes represent hardware or virtual elements in the sensor tree. 
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef _CHORDATA_NODE_
#define _CHORDATA_NODE_

#include "Chordata_def.h"
#include "Chordata_error.h"
#include "Chordata_communicator.h"
#include "Chordata_imu.h"
#include "Chordata_fusion.h"

#include "MadgwickAHRS.h"

#include "EEPROM.h" //This can't be included anywhere else :P 
// belongs to some parts of this programs that were developed on the rush. 
// It shoudl be completely refactored one day 


#include <cstdint>
#include <cstddef>
#include <vector>
#include <string>
#include <memory>
#include <tuple>
#include <iostream>
#include <array>

namespace Chordata_test{
	struct Test_Helper;
}



namespace Chordata {

	/**
	 * A link represents the position in the hierarchy of the armature 
	 */
	class Link {
	
		Link *parent;
	
	public:
		// typedef std::unique_ptr<Link> uLink;
		typedef std::vector<Link*>::size_type size_type;

		/**
		 * Default constructor.
		 * Places the Link in the hierarchy tree by setting it's parent. 
		 * If no Link* is passed the parent is set to nullptr representing that it's at the base of the hierarchy
		 * @param _parent The parent Link* in the hierarchy (there can be only one).
		 */
		explicit Link(Link *_parent = nullptr):
		parent(_parent)
		{};

		/**
		 * @todo debug message
		 */
		virtual ~Link() {}

		/**
		 * Get the parent
		 * @return [description]
		 */
		virtual Link *getParent() const { return parent; }

		virtual Link *getChild( std::size_t nChild = 0 ) const = 0;

		/**
		 * Return the number of children
		 * @return         number of children
		 */
		virtual std::size_t getChildrenN() const = 0;

		virtual Link &addChild(Link *_child) = 0;

		virtual std::size_t getMax() const {return 1;}
		
	};
	
	/**
	 * A SimpleLink can only have one child
	 */
	class SimpleLink: public Link {
	
		Link *child;
	
	public:
		/**
		 * Default constructor.
		 * Places the SimpleLink in the hierarchy tree by setting it's parent. 
		 * If no Link* is passed the parent is set to nullptr representing that it's at the base of the hierarchy.
		 * @param _parent The parent Link* in the hierarchy (there can be only one).
		 */
		explicit SimpleLink(Link *_parent = nullptr):
		Link(_parent),
		child(nullptr)
		{};

		/**
		 * Default destructor
		 * Deletes the only child.
		 *
		 * @see Armature::~Armature()
		 * @todo add debug message
		 */
		~SimpleLink() { 
			delete child;
			 }


		/**
		 * Returns the only child
		 * @param  nChild Ask for the Nth child (just the 0 is valid)
		 * @return        the only child, or nullptr if asked for one different than 0
		 *
		 * @todo throw HierarchyError if nChild != 0
		 */
		virtual Link *getChild( std::size_t nChild = 0 ) const { 
			if (nChild > 0){
				throw Hierarchy_Error("Asked for a child different than 0 on a SimpleLink");
			}
			return child; 
			}

		
		std::size_t getChildrenN() const override
			{ return (child != nullptr)? 1 : 0 ; };

		/**
		 * Add the only possible child
		 * @param  _child pointer to the child to be added
		 * @throws HierarchyError STILL NOT IMPLEMENTED
		 * @return        *this
		 *
		 * @todo throw HierarchyError
		 */
		Link &addChild(Link *_child) override {
			if (child != nullptr){ 
				throw Hierarchy_Error("There's already a child in this SimpleLink node");
			}

			child = _child;
			return *this;
		}

	};

	/**
	 * A MultiLink can have multiple, finite children.
	 */
	class MultiLink: public Link {
		
		std::size_t maxChildren;
		std::vector<Link*> children;
	
	public:

		/**
		 * Default constructor.
		 * Places the Link in the hierarchy tree by setting it's parent. And also sets the maximum allowed children.
		 * If no Link* is passed the parent is set to nullptr representing that it's at the base of the hierarchy
		 * @param _parent 	The parent Link* in the hierarchy (there can be only one).
		 * @param max 		The maximum allowed children
		 */
		explicit MultiLink(Link *_parent = nullptr, std::size_t max = _CHORDATA_MUX_MAX):
		Link(_parent),
		maxChildren(max)
		{};

		/**
		 * Default destructor
		 * Deletes all the children.
		 *
		 * @see Armature::~Armature()
		 * @todo add debug message
		 */
		~MultiLink(){
			for (std::vector<Link*>::iterator i = children.begin(); i != children.end(); ++i){
				delete *i;
			}
		}

		/**
		 * Returns the child at the requested index
		 * @param  nChild Ask for the Nth child
		 * @return        the child at the requested index, or nullptr if there's none.
		 *
		 */
		Link *getChild( std::size_t nChild = 0 ) const override{
			if (nChild >= maxChildren)
				throw Hierarchy_Error("Asked for a child's index out of the bounds of this Multilink");

			if (nChild >= children.size())
				throw Hierarchy_Error("This Multilink has only " + std::to_string(children.size()) + ", asked for the #" +std::to_string(nChild));

			return children[nChild];
		}

		std::size_t getChildrenN() const override{ return children.size(); }

		/**
		 * Add a child to the MultiLink
		 * @param  _child pointer to the child to be added
		 * @throws HierarchyError STILL NOT IMPLEMENTED
		 * @return        *this
		 *
		 * @todo throw HierarchyError
		 */
		Link &addChild(Link *_child) override {
			if ( getChildrenN() >= maxChildren )
				throw Chordata::Hierarchy_Error(fmt::format("Maximum allowed number of children for this Multilink"));	
			
			children.push_back(_child);
			return *this;
		}

		std::size_t getMax() const override {return maxChildren;}
	};

	
	/**
	 * Types of preseted armatures. Still not implemented
	 * @TOIMPROVE Is this really necesary?
	 */
	enum Armature_type{
		SINGLE_NODE = 0,
		BIPED_BASIC,
		CUSTOM
	};

	class Armature_Parser;

	/**
	 * The armature contains the base Link* of the hierarchy. 
	 */
	class Armature{
		/**
		 * The Armature_Parser is the only user that can create an Armature
		 * @todo use just the getArmature() as a friend
		 */
		friend Armature_Parser;
	public:

		/**
		 * Default destructor
		 * Deletes the first Link, the base of the hierarchy. It spawns a chain reaction deleting all the other nodes.
		 *
		 * @see SimpleLink::~SimpleLink()
		 * @see MultiLink::~MultiLink()
		 * @todo add debug message
		 */
		~Armature(){
			delete firstNode;
		}

		Link *getFirstNode(){ return firstNode; }

		Armature_type getType(){ return type; }

	private:
		/**
		 * Private constructor. Can be used only by Armature_Parser
		 * @param	_firstNode	The first Node in the hierarchy
		 * @param	_type		The armature type preset
		 */
		Armature(	Link *_firstNode, 
					Armature_type _type = BIPED_BASIC ):
		firstNode(_firstNode),
		type(_type)
		{
			Chordata::Communicator::try_info("Armature Created.");
		};

		Link *firstNode;
		Armature_type type;
	};


	/**
	 * Node holds the basic information for creating representation of physical nodes.
	 * 
	 */
	class Node {
	public:
		/**
		 * Constructor
		 * @param _address The i2c 7 bit address of the node
		 * @param _label The label of the node. 
		 */
		Node(uint8_t _address, std::string _label):
		label(_label),
		address(_address)
		{};

		/**
		 * Return the address of the node
		 * @return the 7 bit address of the node
		 */
		uint8_t getAddress() const{
			return address;
		}

		/**
		 * Get the label of the node. 
		 * It matchs the corrisponding bone in the virtual representation of the armature 
		 * @return label of the bone
		 */
		std::string getLabel() const{
			return label;
		}


		///Shall this node be used to calculate the global ODR?
		virtual bool isSensor(){ return false; }

	protected:
		const std::string label;
		const uint8_t address;

	};
	


	/**
	 * Represents a Multiplexer
	 * @ingroup Node
	 */

	class Mux: public Node, public MultiLink {
	private:
		mux_channel state;
				
		Mux(	Link *_parent,
				tinyxml2::XMLElement *e,
				I2C_io *_i2c, 
				mux_channel mch = OFF
			);

		Mux(	Link *_parent,
				uint8_t _address, std::string _label,
				I2C_io *_i2c, 
				mux_channel mch = OFF
			);

	public:
		friend Armature_Parser;

		mux_channel getActiveChannel(){
			return state;
		}

		void setActiveChannel(mux_channel ch){
			state = ch;
		}

		// virtual i2c_result bang(){}; //TODO CHANGE THIS TO I2C_Node
	};

	/**
	 * Abstract base for the i2c active nodes.
	 * Presents a unified interface to be used on the readings or writing of the 
	 * physical nodes: bang()!. 
	 * Derivates should be used by a dispatcher on the time-dependent operations
	 * @ingroup Node
	 */

	class I2C_Node:  public Node, public SimpleLink {
	protected:
		bool setup;
		I2C_io *i2c; 

		I2C_Node(	I2C_io *_i2c,
					uint8_t _address, 
					std::string _label, 
					Link *_parent = nullptr):
		i2c(_i2c),
		Node(_address, _label),
		SimpleLink(_parent),
		setup(false)
		{};


	public:
		///Perform the appropiate read or write.
		virtual void bang() =0; 


	};


	/**
	 * Represents one of the gates of a multiplexer.
	 * @ingroup Node
	 */
	class Branch: public I2C_Node{
		Mux *thisMux;
		const mux_channel channel;

		void switchBranch();
	
		/**
		 * Constructor.
		 * @param _adapter The hardware i2c device on the HUB
		 * @param _mux 	A pointer to the Mux that owns this gate
		 * @param _label The name of the branch
		 * @param ch The channel on the Mux that is opened by this gate 
		 */
		Branch(	Mux *_mux,
				tinyxml2::XMLElement *e, 
				I2C_io *_i2c
				);

		Branch(	Mux *_mux,
				uint8_t _address, std::string _label, mux_channel _channel,
				I2C_io *_i2c
				);

	public:
		friend Armature_Parser;
		/**
		 * Open this branch.
		 * Banging a particular Branch grants access to all the offspring of children inside it,
		 * while hiding the children in other Branches.
		 * @return the result of the operation
		 */
		void bang() override;

		mux_channel getChannel() const {return channel;}
		
	};


	
	/**
	 * Represents a device with an IMU sensor.
	 * @ingroup NodeP
	 */
	class K_Ceptor: public I2C_Node, private sensorFusion{ 
		static std::string base_osc_addr;
		I_Imu<I2C_io> *imu;
		float magOffset[3];

		Chordata::Timekeeper timer;
		std::vector<uint64_t> last_laps;


		K_Ceptor(	Link *_parent,
					tinyxml2::XMLElement *e, 
					I2C_io *_i2c);

		K_Ceptor(	Link *_parent,
					uint8_t _address, std::string _label, 
					I2C_io *_i2c);

		bool mag_matrix_set, mag_offset_set, acel_offset_set, gyro_offset_set;

	public:
		Chordata::Kalman_filter kalman;
		const std::string osc_addr, raw_osc_addr, pointc_osc_addr;
		static const uint32_t EEPROM_CURRENT_VERSION;
		std::array<std::array<float, 3>, 3> magMatrix;

		void correct_mag_matrix();		
		
		void setBeta(float b) {sensorFusion::setBeta(b);};

		const Chordata::Kalman_filter& get_kalman() const{
			return kalman;
		}

		static const uint16_t who_am_i;
		friend Chordata_test::Test_Helper;
		friend Armature_Parser;
		template <typename Node>
		friend void Communicator::transmit (const Node *, Quaternion&& );
		template <typename Node>
		friend void Communicator::transmit (const Node *, int16_t gx, int16_t gy, int16_t gz,
		        					int16_t ax, int16_t ay, int16_t az,
		        					int16_t mx, int16_t my, int16_t mz );
		/**
		 * Read the sensor lectures on the device.
		 * Then pass them to the sensor-fusion
		 * @return the result of the operation
		 */
		void bang() override;

		///Only the K_Ceptor influence the global ODR
		bool isSensor(){ return true; }

		bool setupDone(){return setup;}

		void setMagMatrix(std::array<std::array<float, 3>, 3>& m){
			for (int col = 0; col < 3; ++col)	{
		 		for (int row = 0; row < 3; ++row){
		 			magMatrix[row][col] = m[row][col];
		 		}
		 	}
		 	mag_matrix_set = true;
		}

		void setMagMatrix(std::array<float, 9>& m, bool set = true){
			for (int col = 0; col < 3; ++col)	{
		 		for (int row = 0; row < 3; ++row){
		 			magMatrix[row][col] = m[row*3+col];
		 		}
		 	}
		 	mag_matrix_set = set;
		}

		void setMagOffset(std::tuple<float,float,float> o){ 
			magOffset[0] = std::get<0>(o);
			magOffset[1] = std::get<1>(o);
			magOffset[2] = std::get<2>(o);
			mag_offset_set = true;
		}

		void setAcelOffset(std::tuple<float,float,float> o){ 
			imu->aBiasRaw[0] = std::get<0>(o);
			imu->aBiasRaw[1] = std::get<1>(o);
			imu->aBiasRaw[2] = std::get<2>(o);
			acel_offset_set = true;
		}

		void setGyroOffset(std::tuple<float,float,float> o){ 
			imu->gBiasRaw[0] = std::get<0>(o);
			imu->gBiasRaw[1] = std::get<1>(o);
			imu->gBiasRaw[2] = std::get<2>(o);
			gyro_offset_set = true;
		}

		inline const float *getMagOffset() const {return magOffset;}

		static void set_osc_base_addr(const std::string& s){ base_osc_addr = s; }
		
		inline uint64_t ellapsedLapMicros(){
			long accumulate = 0;
			int n = last_laps.size();

			for(uint64_t lap : last_laps) {
			        accumulate += lap;
			    }

			last_laps.clear();

			return accumulate / n;
		}
		

	};

	/**
	 * A shorthand for the K_Ceptor class.
	 */
	typedef K_Ceptor KC;

}// namespace Chordata

#endif