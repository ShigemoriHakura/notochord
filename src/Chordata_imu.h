/**
 * @file Chordata_imu.h
 * Interface template class to handle the different imu implementations 
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef __CHORDATA_IMU_
#define __CHORDATA_IMU_

#include <array>

namespace Chordata{
	template <typename IO>
	class I_Imu
	{
	protected:
		IO* io;
	public:
		I_Imu(IO* _io): io(_io) {}

		int16_t gx, gy, gz; // x, y, and z axis readings of the gyroscope
		int16_t ax, ay, az; // x, y, and z axis readings of the accelerometer
		int16_t mx, my, mz; // x, y, and z axis readings of the magnetometer
	    int16_t temperature; // Chip temperature
	    std::array<int16_t, 3> gBiasRaw, aBiasRaw, mBiasRaw;

		IO* get_manager() const { return io; }

		virtual uint16_t begin() = 0;
		virtual double calibrateGyroAcel(float) = 0;

		virtual void readGyro() = 0;
		virtual void readAccel() = 0;
		virtual void readMag() = 0;
		virtual void readTemp() = 0;

		virtual float calcGyro(int16_t) = 0;
		virtual float calcAccel(int16_t) = 0;
		virtual float calcMag(int16_t) = 0;

		virtual void set_autoCalc() = 0;

	};
	
}//namespace Chordata

#define COMBINED_RESPONSE(a,b) ((a << 8) | b)

//If already included skip, in order to avoid name clashes
#ifdef __LSM9DS0_H__
# undef _CHORDATA_USE_LSM9DS1
#endif

#ifdef __LSM9DS1_H__
# undef _CHORDATA_USE_LSM9DS0
#endif


#ifndef __FORCE_LSM9DS0__

//Include the choosen IMU library
# ifdef _CHORDATA_USE_LSM9DS1

#  include "imu/LSM9DS1.h"
#  define EXPECTED_RESPONSE COMBINED_RESPONSE(WHO_AM_I_AG_RSP, WHO_AM_I_M_RSP)
#  define _CHORDATA_IMU_TYPE_ LSM9DS1<I2C_io>

# elif _CHORDATA_USE_LSM9DS0

#  include "imu/LSM9DS0.h"
#  define EXPECTED_RESPONSE COMBINED_RESPONSE(WHO_AM_I_XM_RESPONSE, WHO_AM_I_G_RESPONSE)
#  define _CHORDATA_IMU_TYPE_ LSM9DS0

# endif

#else //__FORCE_LSM9DS0__

# include "imu/LSM9DS0.h"
# define EXPECTED_RESPONSE COMBINED_RESPONSE(WHO_AM_I_XM_RESPONSE, WHO_AM_I_G_RESPONSE)
# define _CHORDATA_IMU_TYPE_ LSM9DS0(io)


#endif //__FORCE_LSM9DS0__


#endif //__CHORDATA_IMU__

