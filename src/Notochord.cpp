/**
 * @file Notochord.cpp
 * Entry point of the program
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <thread>
#include <fstream>
#include <cstdio>

#include "fmt/core.h"

#include "i2c_manager.h"
#include "Notochord.h"

#include "Chordata_gpio.h"
#include "Chordata_communicator.h"
#include "Chordata_Display.h"
// #include "EEPROM.h"

using namespace Chordata;
using namespace std;
using calibration_offset = std::array<int16_t, 3>;
using calibration_matrix = std::array<float, 9>;
namespace comm = Chordata::Communicator;

namespace Chordata
{
	std::vector<std::array<int16_t, 3>> mag_lectures;
	std::array<int16_t, 3> acel_offsets;
	std::array<int16_t, 3> gyro_offsets;
	bool first_calib_done = false;
	int K_Ceptor_addr;

	K_Ceptor *global_calibration_node;

	std::mutex calibration_lock;
	std::condition_variable calibration_go;

}

config_fn Chordata::getConf = []() -> const Chordata::Configuration_Data & {
	throw Chordata::Logic_Error("The Configuration_Data is not yet available,"
															" generaly this is set when creating an instance of Chordata::Notochord");
};

//////////////////////////////////
/// EEPROM
//////////////////////////////////
#define EEPROM_RW_SLEEP 4

void write_calib(I2C_io *io, uint16_t address, std::array<int16_t, 3> vals, int offset)
{
	// cout << "WRITING EEPROM OFFSET: " << offset << endl;
	for (int i = 0; i < 6; i += 2)
	{
		int16_t val = vals[i / 2];

		int8_t msb = (val & 0xff00) >> 8;
		io->writeByte(address, offset + i + 0, msb);
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));

		int8_t lsb = (val & 0x00ff);
		io->writeByte(address, offset + i + 1, lsb);
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));

		// printf("SLOT %d [ MSB:%2x LSB:%2x - VAL: %4x / %d]\n", i , msb, lsb, val, val);
	}
}

void write_calib(I2C_io *io, uint16_t address, calibration_matrix vals, int offset)
{
	static_assert((sizeof(float) == 4), "The sizeof float is not 4!\n\
	This might cause problems when storing floating point data on the EEPROM.\n\
	Please report this issue.");

	for (int i = 0; i < (9 * 4); i += 4)
	{

		float fval = vals[i / 4];
		uint32_t val = *((int *)&fval);

		int8_t byte = (val & 0xff000000) >> 24; //MSB
		io->writeByte(address, offset + i + 0, byte);
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));
		byte = (val & 0x00ff0000) >> 16;
		io->writeByte(address, offset + i + 1, byte);
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));
		byte = (val & 0x0000ff00) >> 8;
		io->writeByte(address, offset + i + 2, byte);
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));
		byte = (val & 0x000000ff); //LSB
		io->writeByte(address, offset + i + 3, byte);
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));
	}
}

void write_calib(I2C_io *io, uint16_t address, std::array<std::array<float, 3>, 3> vals, int offset)
{
	calibration_matrix M;
	for (int i = 0; i < 9; ++i)
	{
		int row = i / 3;
		int column = i % 3;
		M[i] = vals[row][column];
	}

	write_calib(io, address, M, offset);
}

std::array<float, 9> read_matrix(I2C_io *io, uint16_t address, int offset)
{
	std::array<float, 9> ret;

	for (int i = 0; i < (9 * 4); i += 4)
	{

		uint32_t val = 0x00000000;
		int8_t byte;
		byte = io->readByte(address, offset + i + 0);
		val |= (static_cast<uint32_t>(byte) << 24) & 0xff000000;
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));

		byte = io->readByte(address, offset + i + 1);
		val |= (static_cast<uint32_t>(byte) << 16) & 0x00ff0000;
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));

		byte = io->readByte(address, offset + i + 2);
		val |= (static_cast<uint32_t>(byte) << 8) & 0x0000ff00;
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));

		byte = io->readByte(address, offset + i + 3);
		val |= static_cast<uint32_t>(byte) & 0x000000ff;
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));

		ret[i / 4] = *((float *)&val);
	}

	return ret;
}

std::array<int16_t, 3> read_calib(I2C_io *io, uint16_t address, int offset)
{
	std::array<int16_t, 3> results;
	for (int i = 0; i < 6; i += 2)
	{
		int8_t msb = io->readByte(address, offset + i + 0);
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));

		int8_t lsb = io->readByte(address, offset + i + 1);
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));

		results[i / 2] = (msb << 8) | (lsb & 0x00ff);

		// printf("SLOT %d [ MSB:%2x LSB:%2x - RESULT:%4x / %d ]\n", i , msb, lsb, results[i/2], results[i/2]);
	}

	return results;
}

void write_info(I2C_io *io, uint16_t address, uint32_t val, const int offset)
{
	static const std::size_t N = 4;
	uint8_t chunks[N] = {
			(val & 0xff000000UL) >> 24,
			(val & 0x00ff0000UL) >> 16,
			(val & 0x0000ff00UL) >> 8,
			(val & 0x000000ffUL)};

	for (std::size_t i = 0; i < N; ++i)
	{
		io->writeByte(address, offset + i, chunks[i]);
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));
	}
}

uint32_t read_info(I2C_io *io, uint16_t address, const int offset)
{
	static const std::size_t N = 4;
	uint8_t chunks[N];

	for (std::size_t i = 0; i < N; ++i)
	{
		chunks[i] = io->readByte(address, offset + i);
		thread_sleep(Chordata::millis(EEPROM_RW_SLEEP));
	}

	uint32_t result = 0x00000000UL;
	result |= (chunks[0] << 24) & 0xff000000UL;
	result |= (chunks[1] << 16) & 0x00ff0000UL;
	result |= (chunks[2] << 8) & 0x0000ff00UL;
	result |= chunks[3] & 0x000000ffUL;

	return result;
}

//////////////////////////////////
/// NOTOCHORD
//////////////////////////////////

Notochord::Notochord(Chordata::Configuration_Data &&_conf) : aParser(make_unique<Armature_Parser>(&scheluder, _conf)),
																														 config(std::move(_conf)),
																														 timer_lock(),
																														 scheluder(&timer_lock),
																														 main_timer(&scheluder, &timer_lock),
																														 // imu(nullptr),
																														 i2c(nullptr)
{
	Chordata::getConf = [this]() -> const Chordata::Configuration_Data & { return this->config; };
};

Notochord::Notochord(int argc, const char **argv) : Notochord(parse_cmd_line(argc, argv)){};

int Notochord::inform_thread(int n)
{ //NOT USED AT THE MOMENT, TODO: REVIEW
	lock lk(this->workers_lock);
	objeto = n;
	whistle.notify_one();
}

int Notochord::spawn_threads()
{ //NOT USED AT THE MOMENT, TODO: REVIEW
	int i;
	for (i = 0; i < config.hardware_concurrency - 1; ++i)
	{
		workers.emplace_back([this]() {
			while (true)
			{
				std::unique_lock<std::mutex> lk(this->workers_lock);

				whistle.wait(lk, [this]() { return this->objeto != 0; });

				cout << "THREAD ~ " << std::this_thread::get_id() << " " << this->objeto << endl;

				objeto = 0;
			}
		});
	}
	return i;
}

int Notochord::join_threads()
{
	int i;
	for (i = 0; i < workers.size(); ++i)
	{
		workers[i].join();
	}
	return i;
}

void Notochord::init_communicator()
{
	comm::init_communicator(config);
}

bool Notochord::initI2C()
{
	try
	{
		i2c = new I2C_io(new I2C_Device(config.comm.adapter)); //ERROR!!

		// #ifdef _CHORDATA_USE_LSM9DS1
		// imu = make_LSM9DS1(i2c, 0x0f);
		// #endif

		// #ifdef _CHORDATA_USE_LSM9DS0
		// imu = new LSM9DS0(i2c);
		// #endif

		comm::try_info("I2C adapter initialied on {}", config.comm.adapter);
		return true;
	}
	catch (IO_Error &e)
	{
		comm::try_err("An error occured when trying to"
									" initialize the i2c adaptor on the system:\n{}",
									e.what());
		return false;
	}
}

bool Notochord::createArmature()
{
	if (!i2c)
		return false;

	if (config.calib || config.scan)
	{
		armature = aParser->scanArmature(i2c);
	}
	else
	{
		armature = aParser->getArmature(i2c);
	}

	return true;
}

#define OFFSET_TAG(target, v) \
	fmt::format("\t<calibration type=\"offset\" target=\"{}\">\n\t\t{} {} {}\n\t</calibration>", target, v[0], v[1], v[2])

#define MATRIX_TAG(target, v)                                 \
	fmt::format("\t<calibration type=\"matrix\" target=\"{}\">" \
							"\n\t\t{} {} {}\n\t\t{} {} {}\n\t\t{} {} {}"    \
							"\t</calibration>",                             \
							target, v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7], v[8])

#define KMATRIX_TAG(target, v)                                \
	fmt::format("\t<calibration type=\"matrix\" target=\"{}\">" \
							"\n\t\t{} {} {}\n\t\t{} {} {}\n\t\t{} {} {}"    \
							"\t</calibration>",                             \
							target, v[0][0], v[0][1], v[0][2], v[1][0], v[1][1], v[1][2], v[2][0], v[2][1], v[2][2])

#define GATE_WRITE_uS 45 //TOFIX: this is an empirical value
bool Notochord::run()
{
	if (!i2c || !i2c->ready())
	{
		comm::try_err("i2c device not available. Stopping execution.");
		return false;
	}

	if (!armature)
	{
		comm::err("Fatal error: No armature, aborting.");
		return 1;
	}

	int user_xor_val;
	string user_label;

	//It was observed on the oscilloscope that there were some extra delays
	//so the "config.empirical_add" value was introduced to virually change the ODRModifiers
	//on runtime.
	//Perhaps this is due to the nested multiplexer hack used on the R1 prototype.
	//TODO: investigate why is this happening?!

	long sleepMicros = (1000 * 1000) /
										 ((scheluder.getODRModifiers() + config.empirical_add) * Chordata::getConf().odr);

	const int notODRModifiers = scheluder.length() - scheluder.getODRModifiers();

	// This take in consideration the time consumed by the opening and closingof the gates
	// //TOFIX: change this to use a non empirical value!     //
	sleepMicros -= GATE_WRITE_uS * notODRModifiers / scheluder.getODRModifiers();

	comm::info("Starting timer. [NODES < {} , {} > = {} | ODR = {}  | Sleep = {}us]",
						 notODRModifiers, scheluder.getODRModifiers(), scheluder.length(),
						 Chordata::getConf().odr, sleepMicros);

	comm::info("Sending rate = {}Hz",
						 Chordata::getConf().comm.send_rate);

	try
	{
		std::thread t;

		if (config.calib)
		{

			comm::info_un("Initilizing calibration procedure,\n1.Put the KCEPTOR facing up in a flat surface.");
			comm::info_un("2.Press a key to begin calibration..\n" _CHORDATA_DEF_PROMPT);
			std::cin.ignore();

			t = std::thread([this]() {
				std::unique_lock<std::mutex> lk(calibration_lock);
				calibration_go.wait(lk, [this]() { return first_calib_done; });

				comm::info_un("Collecting mag samples, do the calibration procedure..\n"
											"press ENTER when you are done.\n" _CHORDATA_DEF_PROMPT);
				std::cin.ignore();
				this->main_timer.stop = true;

				comm::info_un("Samples collected: {}", Chordata::mag_lectures.size());
			}); //end thread
		}			//end try to start thread

		bool started = main_timer.startTimer(comm::timekeeper.getStart(), sleepMicros);

		calibration_offset mag_offsets;
		calibration_matrix mag_matrix;

		try
		{ //Save calibration to EEPROM

			std::array<int16_t, 3> mag_offsets;
			std::array<std::array<float, 3>, 3> mag_matrix;

			global_calibration_node->kalman.get_mag_offset_for_EEPROM(mag_offsets, mag_matrix);

			comm::info("\n\nCalibration results KALMAN:\n{}\n{}",
								 OFFSET_TAG("mag", mag_offsets),
								 KMATRIX_TAG("mag", mag_matrix));

			comm::info("Writting to EEPROM, xor: {}", Chordata::K_Ceptor_addr);

			uint8_t eeprom_addr = (Chordata::getConf().kc_ver == 1) ? R1_EEPROM_ADDR : R2_EEPROM_ADDR;
			eeprom_addr = eeprom_addr ^ Chordata::K_Ceptor_addr;

			// Reset EEPROM verification value
			write_info(i2c, eeprom_addr, 0, _CHORDATA_EEPROM_VAL_CHKSUM);

			write_info(i2c, eeprom_addr, _CHORDATA_EEPROM_VERSION_0_1_1, _CHORDATA_EEPROM_VERSION);
			write_info(i2c, eeprom_addr, comm::timekeeper.initialTimestamp(), _CHORDATA_EEPROM_TIMESTAMP);
			write_calib(i2c, eeprom_addr, gyro_offsets, _CHORDATA_EEPROM_GYRO_SPACE);
			write_calib(i2c, eeprom_addr, acel_offsets, _CHORDATA_EEPROM_ACEL_SPACE);
			write_calib(i2c, eeprom_addr, mag_offsets, _CHORDATA_EEPROM_MAG_SPACE);

			write_calib(i2c, eeprom_addr, mag_matrix, _CHORDATA_EEPROM_MMAG_SPACE);
			//If all writes went OK, write the correct verification value
			write_info(i2c, eeprom_addr, _CHORDATA_CRC32, _CHORDATA_EEPROM_VAL_CHKSUM);

			auto eeprom = read_calib(i2c, eeprom_addr, _CHORDATA_EEPROM_MAG_SPACE);

			if (eeprom != mag_offsets)
				comm::err("The readed values (MAG) from the eeprom are different than those written:\n\t{}\n\t{}\n\t{}",
									eeprom[0], eeprom[1], eeprom[2]);

			eeprom = read_calib(i2c, eeprom_addr, _CHORDATA_EEPROM_GYRO_SPACE);
			if (eeprom != gyro_offsets)
				comm::err("The readed values (GYRO) from the eeprom are different than those written:\n\t{}\n\t{}\n\t{}",
									eeprom[0], eeprom[1], eeprom[2]);

			eeprom = read_calib(i2c, eeprom_addr, _CHORDATA_EEPROM_ACEL_SPACE);
			if (eeprom != acel_offsets)
				comm::err("The readed values (ACEL) from the eeprom are different than those written:\n\t{}\n\t{}\n\t{}",
									eeprom[0], eeprom[1], eeprom[2]);

			comm::info("Calibration successfuly recorded to K-Ceptor EEPROM, exiting.");
			// clean_up_and_exit(0);
		}
		catch (const Chordata::IO_Error &e)
		{
			const char *filename = "K_Ceptor_offset_calib";
			comm::err("fail to write or read EEPROM memory, calib values dumped to file: {}", filename);
			ofstream myfile;
			myfile.open(filename, std::ios_base::app);
			myfile << "<!-- Value written on " << comm::timekeeper.now() << " -->\n";
			myfile << "<K_Ceptor Name=\"calibration_dump\" id=\"0\">\n\t" << Chordata::K_Ceptor_addr << "\n";
			myfile << OFFSET_TAG("mag", mag_offsets) << '\n';
			myfile << MATRIX_TAG("mag", mag_matrix) << '\n';
			myfile << OFFSET_TAG("acel", acel_offsets) << '\n';
			myfile << OFFSET_TAG("gyro", gyro_offsets) << '\n';
			myfile << "</K_Ceptor>";
			myfile.close();
			if (t.joinable())
				t.join();
			return false;
		}
		catch (...)
		{
			comm::err("An exception was throw while running octave and read/write to eeprom");
			if (t.joinable())
				t.join();
			return false;
		}

		if (t.joinable())
		{
			t.join();
		}

		if (!started)
			comm::try_err("Stopping execution.");
	}
	catch (const Chordata::Error &e)
	{
		Diaplay_show(e.what());
		comm::try_err("An error ocurred: {}", e.what());
	}
	return true;
}

void Notochord::stop()
{

	// delete armature;	//The armature was created on setup();
}

std::ostream &operator<<(std::ostream &stream, const Chordata::Output_Redirect &r)
{
	using namespace Chordata;
	// fmt::MemoryWriter w;
	for (char i = 0; i < _Output_Redirect_keywords_n; ++i)
	{
		if (static_cast<Output_Redirect>(1 << (i - 1)) & r)
		{
			stream << "[" << _Output_Redirect_keywords[i];
			stream << "] ";
		}
	}
	return stream;
}

#ifdef __CHORDATA_MAIN__

void print_banner(const Notochord &notochord)
{
	std::string separator(60, '=');
	std::cout << separator << '\n';
	std::cout << "Notochord start @ " << comm::timekeeper.initialTime();
	std::cout << "version: " + _CHORDATA_VERSION_STR;
	std::cout << "\n\nOutputs:"
						<< "\n";
	if (notochord.getConf().runner_mode)
	{
		std::cout << "=== RUNNER MODE ==="
							<< "\n";
		std::cout << "[ Log / Error ] -> localhost:50419"
							<< "\n";
		std::cout << "(this mode will also output to file if set in conf)"
							<< "\n";
	}
	else
	{
		std::cout << "Log -> " << notochord.getConf().comm.log << "\n";
		std::cout << "Error -> " << notochord.getConf().comm.error << "\n";
	}
	std::cout << "Transmit -> " << notochord.getConf().comm.transmit << "\n";
	std::cout << "\nTargets:"
						<< "\n";
	std::cout << "File -> " << notochord.getConf().comm.filename << "<DATE>\n";
	std::cout << "OSC ip/port -> " << notochord.getConf().comm.ip
						<< " / " << notochord.getConf().comm.port << "\n";
	std::cout << separator << '\n';
	std::cout << std::endl;

	if (notochord.getConf().wait)
	{
		std::cout << "Press a key to perform init...\n" _CHORDATA_DEF_PROMPT << std::endl;
		std::cin.ignore();
		comm::timekeeper.reset();
	}
}

#include "Chordata_fusion.h"

int main(int argc, char const *argv[])
{
	// Chordata::SensorResolution sr;
	// sr.aRes = 2;
	// sr.gRes = .20;
	// sr.mRes = 3;
	// Chordata::Kalman_filter k(sr);
	// return 0;

	try
	{
		bool status = true;

		Notochord notochord(argc, argv);

		notochord.init_communicator();

		//print_banner(notochord);

		comm::info("Notochord start @ {}", comm::timekeeper.initialTime());
		comm::info("Verbosity level {}", (int)notochord.getConf().comm.log_level);
		comm::info("K-Ceptor revision {}", (int)notochord.getConf().kc_ver);
		auto gpio = GPIO();
		gpio.reset_translators();

		status = notochord.initI2C();
		if (!status)
			return 1;

		Diaplay_begin(Diaplay_SWITCHCAPVCC, notochord.i2c);

		status = notochord.createArmature();
		if (!status)
		{
			throw Chordata::Error("Can't create armature, i2c manager not set.");
		}

		status = notochord.run();
		if (!status)
			return 1;
	}
	catch (const Chordata::CMDLine_Parse_Error &e)
	{
		std::string w = std::string(e.what());
		if (w.find("##credits##") != std::string::npos)
		{
			ifstream inFile;
			std::string banner = fmt::format("{}/../banner", Chordata::get_exe_path());
			inFile.open(banner);
			std::string str;
			if (inFile)
			{
				while (std::getline(inFile, str))
				{
					std::cout << str << std::endl;
				}
			}

			inFile.close();
			std::string credits = fmt::format("{}/../CREDITS", Chordata::get_exe_path());
			inFile.open(credits);

			if (!inFile)
			{
				std::cout << "Unable to open CREDITS file";
				return 1; // terminate with error
			}

			while (std::getline(inFile, str))
			{
				std::cout << str << std::endl;
			}

			// std::cout<< fmt::format("{}/../CREDITS", Chordata::get_exe_path()) << std::endl;
		}
		else
		{
			std::cout << e.what() << std::endl;
		}
		return 0;
	}
	catch (const Chordata::Error &e)
	{
		auto d = parse_cmd_line(argc, argv);

		if (d.comm.error & (Chordata::STDOUT | Chordata::STDERR))
		{
			Diaplay_show(e.what());
			spdlog::set_level(spdlog::level::err);
			comm::init_communicator(d);
			comm::err("Program aborted because of an error:\n{}", e.what());
		}
		else
		{
			std::cerr << e.what() << std::endl;
		}

		return 1;
	}
	catch (const std::bad_alloc &e)
	{
		auto d = parse_cmd_line(argc, argv);

		if (d.comm.error & (Chordata::STDOUT | Chordata::STDERR))
		{
			spdlog::set_level(spdlog::level::err);
			comm::init_communicator(d);
			comm::err("Program aborted because of a memory allocation error:\n{}", e.what());
		}
		else
		{
			std::cerr << e.what() << std::endl;
		}
		return 1;
	}

	return 0;
}

#endif
