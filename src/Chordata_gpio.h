/**
 * @file Chordata_gpio.h
 * class for controlling the hardware GPIO pins of the device
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "bcm2835-1.56/src/bcm2835.h"

#define PIN RPI_V2_GPIO_P1_07

namespace comm = Chordata::Communicator;

namespace Chordata{
    class GPIO{
        bool gpio_working;
    public: 
        GPIO ():
            gpio_working(false) {
            comm::try_debug( "Init GPIO ");
            if (!bcm2835_init()){
                // throw  System_Error( 100, "Can't init the GPIO");
                comm::try_warn("Can't init the GPIO, translators resetting disabled");
                return;
            }
            
            bcm2835_gpio_fsel(PIN, BCM2835_GPIO_FSEL_OUTP);
            gpio_working = true;
            
        }

        void reset_translators(){
            if (!gpio_working) return;

            comm::try_debug( " Resetting i2c addr translators ");
            disable_translators();
            // wait a bit
            bcm2835_delay(100);
            enable_translators();
   

        }

        void enable_translators(){
            if (!gpio_working) return;
            
            bcm2835_gpio_write(PIN, HIGH);
        }

        void disable_translators(){
            if (!gpio_working) return;
            
            bcm2835_gpio_write(PIN, LOW);
        }
    };
}
