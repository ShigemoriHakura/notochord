/**
 * @file Chordata_timer.h
 * Time-handlind classes: Scheduler to organize the reading of the nodes, and Timer to launch each read. 
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef __CHORDATA_TIMER_
#define __CHORDATA_TIMER_

#include <chrono>
#include <thread>
#include <mutex>
#include <vector>
#include <atomic>

#include "Chordata_circular_buffer.h"
#include "Chordata_communicator.h"
#include "Chordata_node.h"
#include "Chordata_utils.h"

#define T_WAIT_DEFAULT 500

namespace comm = Chordata::Communicator;

namespace Chordata {

	class Mutexable{
	protected:
		std::mutex *mp;
		bool deleteMutex;
	
	public:
		Mutexable(): mp( new std::mutex() ), deleteMutex(true) {};
		Mutexable(std::mutex* _m): mp(_m), deleteMutex(false){};
		~Mutexable(){ if ( deleteMutex ) delete mp; };

		std::mutex *getMutex(){
			return mp;
		};
	};


	class TimerActuator: public Mutexable{
	protected:

	public:
		TimerActuator(): Mutexable(){};
		TimerActuator(std::mutex* m): Mutexable(m){};
		virtual long int bang( long int ) = 0;
	};

	class Node_Scheluder: public TimerActuator{
	private:

		Circular_buffer<I2C_Node *> main_buffer;
		// int num;
		std::vector<I2C_Node*> nodelist;
		std::vector<I2C_Node*>::const_iterator current;
		int	ODRModifiers;


		Node_Scheluder& step(){ 
			//The scheluder gets loaded in a manner that the step has to be made backwards
			if (current-- == nodelist.begin())
			{
				current = nodelist.end()-1;
			}
			return *this;
		}

		void doSearch(); //fire the search

	public:
		Node_Scheluder():
			ODRModifiers(0),
			main_buffer(), 
			TimerActuator(), 
			current(nodelist.end()-1)
			{};

		Node_Scheluder(std::mutex *m): 
			ODRModifiers(0), 
			main_buffer(), 
			TimerActuator(m), 
			current(nodelist.end()-1)
			{};
		
		void addNode(I2C_Node *n);
		
		bool empty() const { return nodelist.empty(); }
		
		std::vector<I2C_Node*>::size_type length() const { return nodelist.size();};

		int getODRModifiers(){ return ODRModifiers; };

		long int bang(long int) override;	
	};


	template <class T>
	class Timer: public Mutexable{
	private:
		//Default constructor not acepted
		Timer();

		T *actuator;
		// const long int waitMicros;
	
	public:
		std::atomic_bool stop;

		Timer(T *);
		Timer(T *, std::mutex *);

		bool startTimer(const time_point& start, long int wait);
	};

	template <class T>
	Timer<T> ::Timer(T *act):
			// waitMicros(T_WAIT_DEFAULT),
			Mutexable(),
			actuator(act),
			stop(false)
			{ }

	template <class T>
	Timer<T> ::Timer(T *act, std::mutex *m):
			Mutexable(m),
			actuator(act),
			stop(false)
			{ }

	template <class T>
	bool Timer<T> ::startTimer(const time_point& start, long int wait){

		time_point next_bang = start;
		if (actuator->empty()){
			comm::try_err("Timer not started, scheduler was empty");
			return false;
		}

		comm::try_info("Timer started, {} nodes on scheduler", actuator->length());

		static long i2c_tick = (1000*1000)/400000;
		static long sensor_lecture = i2c_tick * 8 + i2c_tick * 16 * 3;

		if (sensor_lecture > wait)	
			comm::try_warn("With this amount of sensors the sleep between lectures is bigger than the "
				"necessary: {:6d} > {:6d}.\n", wait, sensor_lecture);

		while(!stop){
			next_bang += std::chrono::duration_cast<microDuration>(micros(wait)); //TODO:implement correctly
			wait = actuator->bang(wait);
			std::this_thread::sleep_until(next_bang);
		}
		return true;
	}

}


#endif


