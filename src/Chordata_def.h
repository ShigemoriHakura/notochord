/**
 * @file Chordata_def.h
 * Global constants, enums and strucs declarations
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef __CHORDATA_DEF__
#define __CHORDATA_DEF__

#define _CHORDATA_CBUFF_SIZE 10

#include <cstdint>
#include <string>
#include <functional>

/**
 * @name Globals
 * Global attributes of the program
 */
///@{

/////////////////////////////////
///The version of the program
/////////////////////////////////
#define _CHORDATA_VERSION_MAJOR 0
#define _CHORDATA_VERSION_MINOR 2
#define _CHORDATA_VERSION_PATCH 0
#define _CHORDATA_VERSION_POST "b"

// #define _CHORDATA_GIT_HASH "ccddff"

#define _GET_VERSION_INT(MAJ, MIN, PATCH) (uint32_t)(( MAJ * 10000 ) +\
													 ( MIN * 100 ) +\
													 ( PATCH ))

#define _CHORDATA_VERSION _GET_VERSION_INT( _CHORDATA_VERSION_MAJOR,\
									 		_CHORDATA_VERSION_MINOR,\
									  		_CHORDATA_VERSION_PATCH )

#define	_CHORDATA_VERSION_STR fmt::format("{}.{}.{}{} #{}",\
							_CHORDATA_VERSION_MAJOR,\
							_CHORDATA_VERSION_MINOR,\
							_CHORDATA_VERSION_PATCH,\
							_CHORDATA_VERSION_POST,\
							_CHORDATA_GIT_HASH)

//this is the string "chordata", used for checking presense of data in EEPROM 
#define _CHORDATA_CRC32 0xCBF43926 

//////////////////////////////////
/// The name of the client (where the render is performed and the animation recorded) 
//////////////////////////////////
#define _CHORDATA_CLIENT "Chordata Client"

//////////////////////////////////
/// The name of the hub (where all the sensor information is collected and takes place the sensor-fusion) 
//////////////////////////////////
#define _CHORDATA_HUB "Notochord"

//////////////////////////////////
/// The description of the program, to be displayed on the top of the help (-h option) 
//////////////////////////////////
#define _NOTOCHORD_DESCRIPTION "Hub software of the Chordata motion capture system"


//////////////////////////////////
/// The notes of the program, to be displayed on the bottom of the help (-h option) 
//////////////////////////////////
#define _NOTOCHORD_NOTES "\n*_*"


///@}

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/**
 * @name Tweaks
 */
///@{

/////////////////////////////////
///The padding of the millis on print
/////////////////////////////////
#define _CHORDATA_PRINT_MILLIS_PADDING "7"


#define _CHORDATA_USE_LSM9DS1
// #define _CHORDATA_USE_LSM9DS0 



///@}

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/**
 * @name Configuration
 */
///@{

/////////////////////////////////
///The default path to the configuration file
/////////////////////////////////
#define _CHORDATA_CONF "../Chordata.xml"

/////////////////////////////////
///The default path to the configuration file
/////////////////////////////////
#define _CHORDATA_CONF_SCHEMA "../Chordata.xsd"

//////////////////////////////////
/// The name of the base node of the configuration xml
//////////////////////////////////
#define _CHORDATA_XML_BASE "Chordata"


//////////////////////////////////
/// Communication defaults 
//////////////////////////////////
#define _CHORDATA_FILEOUT_NAME "chord_log"

#define _CHORDATA_FILEOUT_DIR "logs"

#define _CHORDATA_LOG_QUEUE_SIZE 4096 //queue size must be power of 2

#define _CHORDATA_VERBOSITY 0

#define _CHORDATA_SEND_RATE 25

#define _CHORDATA_TRANSMIT_ADDR "127.0.0.1"
#define _CHORDATA_TRANSMIT_PORT (uint16_t) 7000
#define _CHORDATA_RUNNER_PORT (uint16_t) 50419
#define _CHORDATA_TRANSMIT_BUFFER_SIZE 1024
#define _CHORDATA_LOG_FLUSH_INTERVAL 5

#define _CHORDATA_I2C_DEFAULT_ADAPTER_ "/dev/i2c-1"

#define ___CAST_REDIR_ static_cast<Chordata::Output_Redirect>

#define _CHORDATA_DEF_LOG_REDIR ___CAST_REDIR_ (Chordata::STDOUT|Chordata::FILE)

#define _CHORDATA_DEF_ERR_REDIR ___CAST_REDIR_ (Chordata::STDERR|Chordata::FILE)

#define _CHORDATA_DEF_TRA_REDIR ___CAST_REDIR_ (Chordata::OSC)

#define _CHORDATA_DEF_PROMPT "~>"
#define _CHORDATA_DEF_EOM "~#"

///@}

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/**
 * @name OSC
 */
///@{

/////////////////////////////////
///The base OSC address (all the subaddresses will follow this one)
/////////////////////////////////
#define _CHORDATA_DEF_OSC_ADDR "/Chordata"

//////////////////////////////////
/// The OSC error subaddress
//////////////////////////////////
#define _CHORDATA_DEF_ERROR_SADDR "/error"

//////////////////////////////////
/// The OSC subaddress dedicated to secondary communications
//////////////////////////////////
#define _CHORDATA_DEF_COM_SADDR "/comm"
///@}

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/**
 * @name IMU
 */
///@{

//////////////////////////////////
///The default output data rate (for each sensor)
//////////////////////////////////
#define _CHORDATA_ODR 50


//////////////////////////////////
///The default version of K-Ceptor used
//////////////////////////////////
#define _KC_REV 2

//////////////////////////////////
/// The defaule Beta parameter of the Madgwick sensor fusion algorithm
//////////////////////////////////
#define _MADGWICK_BETA 0.2f
#define _MADGWICK_BETA_MANGLING_TIME 5000

//////////////////////////////////
/// The max abs(SLOPE) tolerated when doing acel and gyro calibration
//////////////////////////////////
#define _MAX_AG_CALIB_SLOPE 0.12f

///@}


/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/**
 * @name NODES
 */
///@{
///
//////////////////////////////////
/// The default address of the Hub's Multiplexer
//////////////////////////////////
#define _CHORDATA_MUX_ADDR 0x73

//////////////////////////////////
/// The maximun number of ports allowed by default on a Multiplexer
//////////////////////////////////
#define _CHORDATA_MUX_MAX 6

//////////////////////////////////
/// The starting addrs of the chunks of calib data stored on the EEPROM
//////////////////////////////////
#define _CHORDATA_EEPROM_VAL_CHKSUM		0
#define _CHORDATA_EEPROM_VERSION		8
#define _CHORDATA_EEPROM_TIMESTAMP		12
#define _CHORDATA_EEPROM_GYRO_SPACE 	16
#define _CHORDATA_EEPROM_ACEL_SPACE 	24
#define _CHORDATA_EEPROM_MAG_SPACE 		32
#define _CHORDATA_EEPROM_MMAG_SPACE		40

//////////////////////////////////
/// The last update on the EEPROM structure was on version..
/// (Below this value the calibration data is not useful)
//////////////////////////////////
#define _CHORDATA_EEPROM_VERSION_0_1_1 _GET_VERSION_INT(0,1,1)
#define _CHORDATA_EEPROM_CURRENT_VERSION _CHORDATA_EEPROM_VERSION_0_1_1

///@}

/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

/**
 * @namespace Chordata
 * The namespace under which are grouped all the Chordata objects
 */
namespace Chordata {
	/**
	 * @defgroup Node i2c active objects
	 * All the objects that are somehow involved in the i2c communication
	 */
	
	/**
	 * The register values for opening each of the channels on the Mux
	 * @see Armature_Parser::getBranchCH
	 * @ingroup Node
	 */
	enum mux_channel{
		OFF = 0, ///< 0
		CH_1 = 0x1, ///< 0x1
		CH_2 = 0x2, ///< 0x2
		CH_3 = 0x4, ///< 0x4
		CH_4 = 0x8, ///< 0x8
		CH_5 = 0x10, ///< 0x10
		CH_6 = 0x20, ///< 0x20
	};

	/**
	 * Possible results of an i2c opperation
	 * @ingroup Node
	 */
	enum i2c_result{
		SUCCESS = 0, 
		ADAPTER_NOT_AVAIBLE, ///< The requested i2c adapter is not avaible on this machine. 
		READ_ERROR, ///< Can't read from the slave device. Are you asking for the right register?
		WRITE_ERROR,	///< Can't write to the slave device. Are you asking for the right register?
		DEVICE_NOT_FOUND, ///< Can't find the device. 
		///< Can be due to an error on the configuration files, \n 
		///< a physical connectivity problem (some lousy wires), \n
		///< or a logic problem on the open/closing of Branches (trying to access 
		///< to a non reachable I2C_Node)
		SETUP_ERROR ///< Error while performing a sensor setup
	};

	/**
	 * Utility template for getting size of array on compile time.
	 * From Efficient Modern c++ by S. Meyers
	 */
	template<typename T, std::size_t N>                 
	constexpr std::size_t arraySize(T (&)[N]) noexcept {                                                  
	  return N;                                         
	}
	
	/**
	 * Possible outputs fot data and logging.
	 */
	enum Output_Redirect{
		NONE = 0,
		STDOUT = 1,
		STDERR = 1<<1,
		FILE = 1<<2,
		OSC= 1<<3
	};

	inline Output_Redirect& operator |=(Output_Redirect& a, Output_Redirect b)
	{
	    return a= static_cast<Output_Redirect>(static_cast<char>(a) | static_cast<char>(b));
	}

	inline Output_Redirect& operator |(Output_Redirect& a, Output_Redirect &b)
	{
	    return a= static_cast<Output_Redirect>(static_cast<char>(a) | static_cast<char>(b));
	}

	inline Output_Redirect& operator &=(Output_Redirect& a, Output_Redirect b)
	{
	    return a= static_cast<Output_Redirect>(static_cast<char>(a) & static_cast<char>(b));
	}

	inline Output_Redirect& operator &(Output_Redirect& a, Output_Redirect &b)
	{
	    return a= static_cast<Output_Redirect>(static_cast<char>(a) & static_cast<char>(b));
	}

	inline Output_Redirect operator ~(Output_Redirect& a)
	{
	    return static_cast<Output_Redirect>(~ static_cast<char>(a));
	}


    static const char* _Output_Redirect_keywords[] = 
    								{"none","stdout", "stderr", "file", "osc"};
	static const char _Output_Redirect_keywords_n = arraySize(_Output_Redirect_keywords);

	static const char* _Output_Redirect_REGEX = "none|stdout|stderr|file|osc";

	/**
	 * The reserved i2c 7bits addr according to the specifcation
	 */
	static uint8_t invalid_i2c_addr[] =
			{
				0b0000000,
				0b0000000,
				0b0000001,
				0b0000010,
				0b0000011,
				0b0000100,
				0b0000101,
				0b0000110,
				0b0000111,
				0b1111100,
				0b1111101,
				0b1111110,
				0b1111111,
				0b1111000,
				0b1111001,
				0b1111010,
				0b1111011
			};

	static const char invalid_i2c_addr_n = arraySize(invalid_i2c_addr);

	/**
	 * A Struct that contains all the configuration data.
	 */
	struct Configuration_Data
	{	
		Configuration_Data(); //defined in Chordata_utils.cpp

		std::string exe_path;
		int hardware_concurrency;
		bool wait;
		bool raw;
		bool calib;
		bool scan;
		bool runner_mode;

		int kc_ver;
		int empirical_add;
		int odr;

		struct Program_State{
			mutable char beta_timer_init;
			mutable bool imu_setup_complete;

		} state;

		struct Fusion_info{
			float beta_s, beta_f;
			uint16_t time;
			bool madgwick;
		} fusion;


		void append_path();
		
		struct Conf_info{
			std::string description;
			std::string notes;
			uint32_t version;
		} info;

		struct Conf_XML{
			std::string filename;
			std::string schema;
		} xml;

		struct Conf_comm{
			std::string adapter;
			std::string ip;
			uint16_t port;
			Output_Redirect log;
			Output_Redirect error;
			Output_Redirect transmit;
			std::string filename;
			char log_level;
			float send_rate;
			bool use_bundles;
		} comm;

		struct Conf_osc{
			std::string base_address;
			std::string error_address;
			std::string comm_address;
		} osc;
	};

	/////////////////////////////////
	///TYPEDEFS AND CHORDATA NAMESPACE OBJECTS
	/////////////////////////////////

	typedef std::function<const Chordata::Configuration_Data&(void)> config_fn;
	extern config_fn getConf;
	/**
	 * The possible parsing stages.
	 */
	enum Parsing_stage{
		CMD_LINE_PARSING,///< When parsing the general configuration.
		CONFIG_PARSING,///< When parsing the general configuration.
		ARMATURE_PARSING///< When parsing the xml hierarchy, extracting nodes like Mux, Branch and K_Ceptor.
		///< In this stage the armature object is been created, and the Node_Scheluder is been loaded.
	};

	namespace msgs{
		static const char* version_dump = "Chordata's Notochord v{}"; 

		/**
		 * A String array used to return the name of the stage on error message.
		 */
		static const char *stages[] = {
			"CMD_LINE_PARSING",
			"CONFIG_PARSING",
			"ARMATURE_PARSING"
		};

		static const char *help = "Display this help menu";

		static const char *version = "Display program's version and exit";

		static const char *wait = "Disable confirmation to start the program. By default Notochord will wait for a confirmation from the user.";

		static const char *raw = "Transmit the raw lectures fron the K-Ceptors";

		static const char *scan = "Scan the i2c port to create the Armature, instead of reading the configuration file";

		static const char *calib = "Sensor calibration procedure. Notochord will perform a --scan, and calibrate the found K-Ceptor. It is adviced to plug just one KC to the Hub with this flag!!";

		static const char *kc_ver = "The revision of the K-Ceptor hardware used.";

		static const char *xml_g = "\nCONFIGURATION FILES:";

		static const char *xml_filename = "The path to the configuration file. Default [./Chordata.xml]";

		static const char *xml_schema = "The path to the xsd schema for the configuration file. Default [./Chordata.xsd]";

		static const char *ip = "The client's ip.\n";

		static const char *port = "The client's port.\n";

		static const char * comm_g = "\nOUTPUT OPTIONS:";

		static const char* log = "Where to redirect the information messages";

		static const char* error = "Where to redirect the errors";

		static const char* transmit = "Where to redirect the actual payload (mostly Quaternion data)";

		static const char* runner = "Used when the notochord is initialized by a daemon who expects communications by OSC in localhost:50419";

		static const char* i2c = "The user space i2c adapter, default: "
									_CHORDATA_I2C_DEFAULT_ADAPTER_;

		static const char* filename = "The file where to put the log.";

		static const char* verbose = "Verbose output";

		static const char* send_rate = "Maximum send rate for transmision";

		static const char* osc_g = "\nOSC ADDRESSES";

		static const char* osc_base = "Base OSC address, default:\n"
										_CHORDATA_DEF_OSC_ADDR;

		static const char* osc_err = "Errors OSC subaddress, default:\n"
										_CHORDATA_DEF_ERROR_SADDR;

		static const char* osc_comm = "Info OSC subaddress, default:\n"
										_CHORDATA_DEF_COM_SADDR;

		static const char* note = 
		"OUTPUT OPTIONS can be repetead assigning more than one output.\n" 
		"Valid options are `stdout`, `stderr`, `file`, `osc`"
		_NOTOCHORD_NOTES
		"\n";
	}

}



#endif
