/**
 * @file Chordata_node.cpp
 * In notochord the nodes represent hardware or virtual elements in the sensor tree. 
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>. *
 */

#include <iostream>
#include <algorithm>
#include <condition_variable>
#include <mutex>

#include "Chordata_def.h"
#include "Chordata_node.h"
#include "Chordata_imu.h"
#include "Chordata_parser.h"
#include "MadgwickAHRS.h"
#include "Chordata_fusion.h"

namespace Chordata
{
	// typedef std::function<const Chordata::Configuration_Data&(void)> config_fn;

	// extern config_fn getConf;
	extern std::vector<std::array<int16_t, 3>> mag_lectures;
	extern std::array<int16_t, 3> acel_offsets;
	extern std::array<int16_t, 3> gyro_offsets;
	extern bool first_calib_done;
	extern std::mutex calibration_lock;
	extern std::condition_variable calibration_go;

	extern K_Ceptor *global_calibration_node;

	extern int K_Ceptor_addr;
}

using namespace Chordata;
using namespace std;

using AP = Chordata::Armature_Parser;
namespace comm = Chordata::Communicator;

Mux::Mux(Link *_parent,
				 tinyxml2::XMLElement *e,
				 I2C_io *_i2c,
				 mux_channel mch) : Node(AP::getAddress(e), AP::getName(e)),
														MultiLink(_parent),
														state(mch){};

Mux::Mux(Link *_parent,
				 uint8_t _address, std::string _label,
				 I2C_io *_i2c,
				 mux_channel mch) : Node(_address, _label),
														MultiLink(_parent),
														state(mch){};

Branch::Branch(Mux *_mux,
							 tinyxml2::XMLElement *e,
							 I2C_io *_i2c) : I2C_Node(_i2c, _mux->getAddress(), AP::getName(e), _mux),
															 thisMux(_mux),
															 channel(AP::getBranchCH(e)){};

Branch::Branch(Mux *_mux,
							 uint8_t _address, std::string _label, mux_channel _channel,
							 I2C_io *_i2c) : I2C_Node(_i2c, _address, _label, _mux),
															 thisMux(_mux),
															 channel(_channel){};

// K_Ceptor::K_Ceptor(	Link *_parent,
// 					tinyxml2::XMLElement *e,
// 					I2C_io *_i2c):
// 	I2C_Node(_i2c, AP::getAddress(e), AP::getName(e), _parent),
// 	imu( new _CHORDATA_IMU_TYPE_(_i2c, AP::getAddress(e) )),
// 	osc_addr(fmt::format("{}/{}", base_osc_addr, AP::getName(e))),
// 	sensorFusion(),
// 	magOffset()
// 	{
// 		Chordata::K_Ceptor_addr = address;
// 	};

K_Ceptor::K_Ceptor(Link *_parent,
									 tinyxml2::XMLElement *e,
									 I2C_io *_i2c) : K_Ceptor(_parent, AP::getAddress(e), AP::getName(e), _i2c){};

std::string get_q_addr(const std::string &base_osc_addr, const std::string &_label)
{
	if (!Chordata::getConf().comm.use_bundles)
		return fmt::format("{}/q/{}", base_osc_addr, _label);

	return fmt::format("/%/{}", _label);
}

std::string get_raw_addr(const std::string &base_osc_addr, const std::string &_label)
{
	if (!Chordata::getConf().comm.use_bundles)
		return fmt::format("{}/raw/{}", base_osc_addr, _label);

	return fmt::format("/%/{}", _label);
}

std::string get_pointc_addr(const std::string &base_osc_addr, const std::string &_label)
{
	if (!Chordata::getConf().comm.use_bundles)
		return fmt::format("{}/extra/{}/pointc", base_osc_addr, _label);

	return fmt::format("/%/{}/pointc", _label);
}

K_Ceptor::K_Ceptor(Link *_parent,
									 uint8_t _address, std::string _label,
									 I2C_io *_i2c) : I2C_Node(_i2c, _address, _label, _parent),
																	 imu(new _CHORDATA_IMU_TYPE_(_i2c, _address, Chordata::getConf().odr)),
																	 osc_addr(get_q_addr(base_osc_addr, _label)),
																	 raw_osc_addr(get_raw_addr(base_osc_addr, _label)),
																	 pointc_osc_addr(get_pointc_addr(base_osc_addr, _label)),
																	 sensorFusion(Chordata::getConf().odr),
																	 magOffset(),
																	 mag_matrix_set(false), mag_offset_set(false),
																	 acel_offset_set(false), gyro_offset_set(false),
																	 kalman(Chordata::getConf().odr / OVERSAMPLE_RATIO)
{
	kalman.set_sensitivity(SENSITIVITY_GYROSCOPE_2000,
												 SENSITIVITY_ACCELEROMETER_8,
												 SENSITIVITY_MAGNETOMETER_4);

	//Set global address for calibration, this works when only one KC is attached!s
	Chordata::K_Ceptor_addr = address;

	//Set calib values to 0
	for (int i = 0; i < 3; ++i)
	{
		magOffset[i] = 0;
		imu->aBiasRaw[i] = 0;
		imu->gBiasRaw[i] = 0;
	}

	magMatrix = {{{1, 0, 0}, {0, 1, 0}, {0, 0, 1}}};
};

//static K_Ceptor data members:
const uint16_t K_Ceptor::who_am_i = EXPECTED_RESPONSE;
string K_Ceptor::base_osc_addr = "";

inline void printNineFloats(const float &rx, const float &ry, const float &rz,
														const float &ax, const float &ay, const float &az,
														const float &mx, const float &my, const float &mz)
{
	std::streamsize prec = cout.width(10);
	cout << rx << " " << ry << " " << rz << " "
			 << ax << " " << ay << " " << az << " "
			 << mx << " " << my << " " << mz << " " << endl;

	cout.width(prec);
}

#define OFFSET_DEBUG_MSG(W, V, S) \
	comm::try_debug("{:>10} correction vector =[{:^8} {:^8} {:^8}] {}", W, V[0], V[1], V[2], S)

#define MATRIX_DEBUG_MSG(M, S)                        \
	comm::try_debug("{10:>10} Matrix = {9}\n"           \
									"\t\t\t|{0:^12} {1:^12} {2:^12}|\n" \
									"\t\t\t|{3:^12} {4:^12} {5:^12}|\n" \
									"\t\t\t|{6:^12} {7:^12} {8:^12}|",  \
									M[0][0], M[0][1], M[0][2],          \
									M[1][0], M[1][1], M[1][2],          \
									M[2][0], M[2][1], M[2][2], S, "Mag")

void KC::correct_mag_matrix()
{
	std::array<float, 3> b = {imu->mx, imu->my, imu->mz};
	std::array<float, 3> c = {0, 0, 0};
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			c[i] += (magMatrix[i][j] * b[j]);
		}
	}
	imu->mx = c[0];
	imu->my = c[1];
	imu->mz = c[2];
}

const uint32_t KC::EEPROM_CURRENT_VERSION = _CHORDATA_EEPROM_CURRENT_VERSION;

void KC::bang()
{
	if (!setup)
	{
		uint16_t setup_result = 0;

		try
		{
			setup_result = imu->begin();
		}
		catch (const Chordata::IO_Error &e)
		{
			comm::try_err("Can't find K_Ceptor {}", getLabel());
			return;
		}

		if (setup_result == who_am_i)
		{
			setup = true;
			comm::try_info("K_Ceptor {:.<12}: setup OK.", getLabel());
			if (Chordata::getConf().calib)
			{
				using lock = std::lock_guard<std::mutex>;
				comm::info_un("Collecting Acel and Gyro samples, don't move the KCEPTOR...");
				thread_sleep(Chordata::millis(250));
				while (1)
				{
					double slope = imu->calibrateGyroAcel(_MAX_AG_CALIB_SLOPE);
					if (slope == 0.0)
						break;
					comm::try_info_un("Too much variation (slope = {}) Is the sensor steady?\nRetrying..", slope);
				}
				std::copy(std::begin(imu->gBiasRaw), std::end(imu->gBiasRaw), std::begin(gyro_offsets));
				std::copy(std::begin(imu->aBiasRaw), std::end(imu->aBiasRaw), std::begin(acel_offsets));
				comm::info_un("Acel and Gyro calibration done");
				comm::info_un("3.Press a key when you are ready to do the Mag calibration procedure\n" _CHORDATA_DEF_PROMPT);
				std::cin.ignore();
				lock lk(Chordata::calibration_lock);
				Chordata::first_calib_done = true;
				Chordata::calibration_go.notify_one();
				return;
			}

			imu->set_autoCalc();

			try
			{
				uint8_t eeprom_addr = (Chordata::getConf().kc_ver == 1) ? R1_EEPROM_ADDR : R2_EEPROM_ADDR;
				eeprom_addr = this->getAddress() ^ eeprom_addr;

				auto validation = read_info(i2c, eeprom_addr, _CHORDATA_EEPROM_VAL_CHKSUM);

				if (validation != _CHORDATA_CRC32)
				{
					comm::try_warn("K_Ceptor {}: No EEPROM calib data found.", getLabel());
					OFFSET_DEBUG_MSG("Gyro", imu->gBiasRaw, (gyro_offset_set) ? "XML" : "NONE");
					OFFSET_DEBUG_MSG("Acel", imu->aBiasRaw, (acel_offset_set) ? "XML" : "NONE");
					OFFSET_DEBUG_MSG("Mag", magOffset, (mag_offset_set) ? "XML" : "NONE");
					MATRIX_DEBUG_MSG(magMatrix, (mag_matrix_set) ? "XML" : "NONE");
					return;
				}

				comm::try_debug("K_Ceptor {}: EEPROM calib data found.", getLabel());
				auto eeprom_info = read_info(i2c, eeprom_addr, _CHORDATA_EEPROM_TIMESTAMP);
				auto eeprom_v = read_info(i2c, eeprom_addr, _CHORDATA_EEPROM_VERSION);
				comm::try_debug("~~ V_int: {1} | Last calibration: {0} ",
												comm::timekeeper.time_str(eeprom_info),
												eeprom_v);

				if (eeprom_v < EEPROM_CURRENT_VERSION)
				{
					comm::err("Calibration version outdated, please re-calibrate this K-Ceptor --> {} <---",
										getLabel());
				}

				std::array<int16_t, 3> eeprom;
				//get gyro offsets
				if (!gyro_offset_set)
				{
					eeprom = read_calib(i2c, eeprom_addr, _CHORDATA_EEPROM_GYRO_SPACE);
					std::copy(std::begin(eeprom), std::end(eeprom), std::begin(imu->gBiasRaw));
				}
				OFFSET_DEBUG_MSG("Gyro", imu->gBiasRaw, (gyro_offset_set) ? "XML" : "EEPROM");
				gyro_offset_set = true;

				//get acel offsets
				if (!acel_offset_set)
				{
					eeprom = read_calib(i2c, eeprom_addr, _CHORDATA_EEPROM_ACEL_SPACE);
					std::copy(std::begin(eeprom), std::end(eeprom), std::begin(imu->aBiasRaw));
				}
				OFFSET_DEBUG_MSG("Acel", imu->aBiasRaw, (acel_offset_set) ? "XML" : "EEPROM");
				acel_offset_set = true;

				//Get mag offsets
				if (!mag_offset_set)
				{
					eeprom = read_calib(i2c, eeprom_addr, _CHORDATA_EEPROM_MAG_SPACE);
					std::copy(std::begin(eeprom), std::end(eeprom), std::begin(magOffset));
				}
				OFFSET_DEBUG_MSG("Mag", magOffset, (mag_offset_set) ? "XML" : "EEPROM");
				mag_offset_set = true;

				//Get mag matrix
				if (!mag_matrix_set)
				{
					auto eeprom_m = read_matrix(i2c, eeprom_addr, _CHORDATA_EEPROM_MMAG_SPACE);
					setMagMatrix(eeprom_m, false);
				}
				MATRIX_DEBUG_MSG(magMatrix, (mag_matrix_set) ? "XML" : "EEPROM");
				mag_matrix_set = true;

				//set mag calibration to Kalman_filter:

				kalman.set_mag_cal_from_EEPROM(eeprom_v, magOffset, magMatrix);
			}
			catch (const Chordata::IO_Error &e)
			{
				setup = false;
				comm::try_err("EEPROM not found on {}", getLabel());
				OFFSET_DEBUG_MSG("Gyro", imu->gBiasRaw, (gyro_offset_set) ? "XML" : "NONE");
				OFFSET_DEBUG_MSG("Acel", imu->aBiasRaw, (acel_offset_set) ? "XML" : "NONE");
				OFFSET_DEBUG_MSG("Mag", magOffset, (mag_offset_set) ? "XML" : "NONE");
				MATRIX_DEBUG_MSG(magMatrix, (mag_matrix_set) ? "XML" : "NONE");
			} //end try to read EEPROM

			return; //setup and EEPROM read done
		}
		else
		{
			comm::try_err("K_Ceptor {}: setup ERROR!. ({:x})", getLabel(), setup_result);
			return;
			// throw IO_Error( "Error while performing the setup of the node");
		}
	}

	static bool beta_mangling = Chordata::getConf().fusion.beta_s != Chordata::getConf().fusion.beta_f;

	if (beta_mangling && Chordata::getConf().fusion.madgwick)
	{
		if (!Chordata::getConf().state.beta_timer_init)
		{
			Chordata::getConf().state.beta_timer_init = 1;
			comm::timekeeper.initBetaCounter(Chordata::getConf().fusion.time);
			comm::debug("Init Beta Mangling {} to {} in {} secs",
									Chordata::getConf().fusion.beta_s,
									Chordata::getConf().fusion.beta_f,
									Chordata::getConf().fusion.time / 1000.0f);
		}
		//get
		float mix = comm::timekeeper.getBetaCounter();

		setBeta(Chordata::getConf().fusion.beta_f * std::min(mix, 1.0f) +
						Chordata::getConf().fusion.beta_s * (1 - std::min(mix, 1.0f)));

		if (mix > 1)
		{
			beta_mangling = false;
			comm::info_once(this, "End Beta Mangling in {}", Chordata::getConf().fusion.beta_f);
		}
	}

	try
	{
		imu->readGyro();
		imu->readAccel();
		imu->readMag();

		last_laps.push_back(timer.ellapsedLapMicros());
		timer.resetLap();

		static bool assert_once = true;

		if (Chordata::getConf().calib)
		{
			kalman.set_mag(imu->my, -imu->mx, imu->mz);
			kalman.process_readings();

			if (kalman.iCounter++ == (OVERSAMPLE_RATIO - 1))
			{
				kalman.iCounter = 0;

				if (kalman.run_mag_calib())
				{
					kalman.transmit_mag_pointcloud(this);
				}
			}

			Chordata::global_calibration_node = this;

			return;
		}

		//NOTE: Converting Mixed axis convention on LSM9DS1 to ENU coordinate system
		//
		// gy	gx 	gz
		// ay 	ax	az
		// my  -mx 	mz
		//

		if (Chordata::getConf().fusion.madgwick)
		{
			/*
			// Correct magnetometer reads from callibration data
			imu->mx += magOffset[0];
			imu->my += magOffset[1];
			imu->mz += magOffset[2];

			correct_mag_matrix();

			Quaternion q = MadgwickAHRSupdate(
					degToRads(imu->calcGyro(imu->gy)), degToRads(imu->calcGyro(imu->gx)), degToRads(imu->calcGyro(imu->gz)),
					imu->calcAccel(imu->ay), imu->calcAccel(imu->ax), imu->calcAccel(imu->az),
					imu->calcMag(imu->my), -imu->calcMag(imu->mx), imu->calcMag(imu->mz));

			comm::transmit(this, move(q));

			if (Chordata::getConf().raw)
			{
				comm::transmit(this,
											 imu->gy, imu->gx, imu->gz,
											 imu->ay, imu->ax, -imu->az,
											 imu->my, -imu->mx, imu->mz);
			}
			*/
		}
		else
		{
			kalman.set_gyro(imu->gy, imu->gx, imu->gz);
			kalman.set_acel(imu->ay, imu->ax, imu->az);
			kalman.set_mag(imu->my, -imu->mx, imu->mz);

			bool filter_has_run = kalman.process_and_run();
			if (!filter_has_run)
			{
				return;
			}

			const FSsf::fquaternion &the_quat = kalman.get_quat();

			Quaternion q(the_quat.q0, the_quat.q2,
									 -the_quat.q1, the_quat.q3);

			comm::transmit(this, move(q));
			/*
			imu->mx += magOffset[0];
			imu->my += magOffset[1];
			imu->mz += magOffset[2];
*/
			//correct_mag_matrix();
			/*
			if (Chordata::getConf().raw)
			{
				comm::transmit(this,
											 imu->gx, -imu->gy, imu->gz,
											 imu->ax, -imu->ay, -imu->az,
											 // kalman.thisMag.iBcAvg[0], kalman.thisMag.iBcAvg[1], kalman.thisMag.iBcAvg[2]);
											 imu->mx, imu->my, imu->mz);
			}
			*/
		}
	}
	catch (const Chordata::IO_Error &e)
	{
		comm::err("Error reading {}", getLabel());
		setup = false;
	}
}

void Branch::bang()
{

	for (int i = 0; i < 5; ++i)
	{
		try
		{
			switchBranch();
			thisMux->setActiveChannel(channel);
			return;
		}
		catch (const Chordata::IO_Error &e)
		{
			comm::err("error switching branch {}. Retrying.. ({})", getLabel(), i);
			thread_sleep(Chordata::millis(20));
		}
	}

	comm::err("Too many errors when switching branch {}.", getLabel());
	throw IO_Error("Can't write to mux");
}

void Branch::switchBranch()
{
	i2c->I2CwriteByte(address, channel);
}

//////////////////////////////////
/// For testing individual utils
//////////////////////////////////

#ifdef __NODE_TEST_

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
	I2C_Node baseNode(0, string("BASE"));

	Mux multiplexer(0xfe, string("MUX"), &baseNode);

	for (int i = 0; i < 5; i++)
	{
		multiplexer.addChild(new Branch(&multiplexer, string("BRANCH")));
	}

	Link *abranch = (multiplexer.getChild(2));
	if (abranch == nullptr)
		cout << "NO TABA EL BRANCHO" << endl;

	abranch->addChild(new K_Ceptor(0xc1, string("first ceptor"), abranch))
			.getChild()
			->addChild(new K_Ceptor(0xc2, string("second ceptor"), (*abranch).getChild()));

	return 0;
}

#endif
