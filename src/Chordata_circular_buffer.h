/**
 * @file Chordata_circular_buffer.h
 * Definition of the Circular_buffer template class 
 *
 * @author Bruno Laurencich
 * @version 0.2.0 
 *
 * Notochord  
 * -- Hub program for the Chordata Open Source motion capture system
 *
 * http://chordata.cc
 * contact@chordata.cc
 *
 *
 * Copyright 2018 - 2020 Bruno Laurencich
 *
 * This file is part of Notochord.
 *
 * Notochord is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Notochord is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Notochord. If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef __CHORDATA_CBUFFER_
#define __CHORDATA_CBUFFER_

#include <boost/circular_buffer.hpp>
#include <type_traits>
// #include <iostream>
#include <mutex>
#include "Chordata_def.h"
#include "Chordata_utils.h"
#include "Chordata_error.h"
#include "Chordata_communicator.h"


namespace Chordata {

	// class I2C_Node;

	template<typename>
	struct PM_traits {};
	 
	template<class T, class U>
	struct PM_traits<U T::*> {
	    using member_type = U;
	};

	template <typename T>
	class Circular_buffer{

	private:
		using rT = typename std::remove_pointer<T>::type;
		using FUNC = typename PM_traits<decltype(&rT::bang)>::member_type;

		boost::circular_buffer<T> buffer;
		bool sleeping;
		std::mutex m;

	public:
		Circular_buffer(int size = _CHORDATA_CBUFF_SIZE):
		buffer(size),
		sleeping(true)
		{
			static_assert( std::is_function<FUNC>::value, 
				"The type parameter doesn't expose a `bang()` member function" );
		};

		void put(T node){
			// std::lock_guard<std::mutex> lk(m);
			if (buffer.full()){
				throw Chordata::Error( "Circular buffer overflow",
					Chordata::Communicator::timekeeper.ellapsedMillis());
			}

			buffer.push_back(node);
		}

		bool isSleeping() const { return sleeping; };

		bool empty() const { return buffer.empty(); }

		void eat(){
			// std::unique_lock<std::mutex> lk(m);
			while (!buffer.empty()){
				auto el = buffer.front();
				buffer.pop_front();
				// lk.unlock();
				el->bang();
			}
		}
	};
}



#endif